# FFHS BSc INF 2015, ZH1, HS18/19
# 'Krunchy' der Nussknacker
# 
Seminararbeit im Fach Embedded Systems und Hardware Hacking

### Toolchain
* [GitLab](https://docs.gitlab.com/ee/README.html)                     - Concurrent Versioning
* [Enterprise Architect](https://www.sparxsystems.de/uml/ea-function/) - Architecture/Modeller
* [IntelliJ](https://www.jetbrains.com/idea/)                          - IDE for Java
* [gfm-plugin](https://plugins.jetbrains.com/plugin/7701-gfm)          - Markdown File Preview für IntelliJ
* [JavaDoc](http://www.oracle.com/technetwork/java/javase/documentation/javadoc-137458.html) - Code Documentation
* [JUnit](http://junit.org/junit4/) - Unit Testing für Java
* [Mockito](http://site.mockito.org/) - Mocking Support for Unit Testing
* [SLF4J](https://www.slf4j.org/) - Logging Support für Java
* [QAplug](https://qaplug.com/) - Static Code Checker für Java/IntelliJ
* [diozero](http://www.diozero.com/en/latest/#diozero) - Java I/O Library for the Raspberry Pi
* [Pi4J](https://pi4j.com/) - Java I/O Library for the Raspberry Pi


### Conventions
Angepasste [Style- und Naming-Convention](doc/appendix/Convention_Java_FFHS.docx) für Java Projekte an der FFHS 

### Authors
* **Andreas Frommel**
* **Mirko Helbling**

### License
Licensed under the [Educational Community License](https://opensource.org/licenses/ECL-2.0) Version 2.0 (ECL-2.0)


### Installation and Test
- Krunchy Raspian Image auf SD etchen (gem. [Anleitung](doc/KrunchyInstall.docx))

#### [Terminplan Krunchy als Excel](doc/appendix/Terminplan.xls)

#### [Abgabe 1  Projekt-Initialisierung](doc/Frommel_Helbling_Initialisierung.pdf)
#### [Abgabe 2  Analyse](doc/Frommel_Helbling_Analyse.docx)
#### [Abgabe 3  Design](doc/Frommel_Helbling_Design.docx)
#### [Abgabe 4  Implementation](doc/Frommel_Helbling_Implementierung.docx)
#### [Abgabe 5  Projekt Dokumentation](doc/Frommel_Helbling_Projekt.docx)


### Mehr Informationen:
- [Nussknacker Ideen](doc/appendix/Nussknacker.docx)
- [andere Projekt Ideen](doc/appendix/Projekt_Ideen.docx)
- [Unterrichtsmaterial, Ebooks und allerlei](http://ffhs.frommel.ch/eshh/)

