package AD_Converter_HX711;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test class to check and debug the function of a HX711 converter board with a weight cell
 *
 */
public class Calibrate_Load_cell
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Test_AD_Converter_HX711.class);

    private static final int HX711_CLOCK_IN_PIN_CHANNEL1 = 19;
    private static final int HX711_DATA_OUT_PIN_CHANNEL1 = 20;
    private static final int HX711_CLOCK_IN_PIN_CHANNEL2 = 21;
    private static final int HX711_DATA_OUT_PIN_CHANNEL2 = 22;


    /**
     * main to start the calibration
     * @param args
     */
    public static void main(String[] args)
    {
        LOGGER.info("Calibrate_Load_cell main invoked");

        calibrate2LoadCells();

        LOGGER.info("Calibrate_Load_cell main exit");
    }

    /**
     * Test method for the HX711 AD-converter.
     * creates a converter object with the given pin numbers initialises the hardware
     * and runs severeal conversion on it. the values are logged for reference
     */
    private static void calibrate2LoadCells()
    {
        ConverterHX711 calibrationCannel1 = new ConverterHX711(HX711_DATA_OUT_PIN_CHANNEL1, HX711_CLOCK_IN_PIN_CHANNEL1);
        ConverterHX711 calibrationCannel2 = new ConverterHX711(HX711_DATA_OUT_PIN_CHANNEL2, HX711_CLOCK_IN_PIN_CHANNEL2);

        calibrationCannel1.setCalibrationFactor(1); // during calibration no correction
        calibrationCannel2.setCalibrationFactor(1); // during calibration no correction

        calibrationCannel1.resetConverter();
        calibrationCannel2.resetConverter();

        // the HX711 shows a strange behavior after startup. some FFFFFF readings are skipped by these dummy reads
        LOGGER.info("dummy read: " + (int)calibrationCannel1.getForceInGram(10));
        LOGGER.info("dummy read: " + (int)calibrationCannel2.getForceInGram(10));

        System.out.println("Calibration Start");
        System.out.println("=================");

        int iRemainingTests = 10; // make this -1 for endless loop

        while (iRemainingTests-- != 0)
        {
            double dbOuterSum1 = 0.0;
            double dbOuterSum2 = 0.0;
            int iOuterAverageCount = 0;

            // outer average loop
            int iOuterLoops = 1;
            while (iOuterLoops-- > 0)
            {
                double dbInnerSum1 = 0.0;
                double dbInnerSum2 = 0.0;
                int iInnerAverageCount = 0;

                // inner average loop
                int iInnerLoops = 150;
                while (iInnerLoops-- > 0)
                {
                    iInnerAverageCount++;
                    dbInnerSum1 = dbInnerSum1 + calibrationCannel1.getForceInGram(1);
                    dbInnerSum2 = dbInnerSum2 + calibrationCannel2.getForceInGram(1);
                    //LOGGER.info("getForceInGram: " + (int)dbInnerSum1 + " units  |  "  + (int)dbInnerSum2 + " units");

                }

                double dbInnerVal1 = dbInnerSum1 / iInnerAverageCount;
                double dbInnerVal2 = dbInnerSum2 / iInnerAverageCount;
                //System.out.print("\rInner Loop  Loadcell 1 : " + (int)dbInnerVal1 + " units  | Loadcell 2 : " + (int)dbInnerVal2 + " units");

                iOuterAverageCount++;
                dbOuterSum1 = dbOuterSum1 + dbInnerVal1;
                dbOuterSum2 = dbOuterSum2 + dbInnerVal2;
            }

            double dbOuterVal1 = dbOuterSum1 / iOuterAverageCount;
            double dbOuterVal2 = dbOuterSum2 / iOuterAverageCount;
            System.out.println("");

            System.out.println("Outer Loop  Loadcell 1 : " + (int)dbOuterVal1 + " units  | Loadcell 2 : " + (int)dbOuterVal2 + " units");
        }

        System.out.println("");
        System.out.println("Calibration End");
        System.out.println("===============");

    }



    }
