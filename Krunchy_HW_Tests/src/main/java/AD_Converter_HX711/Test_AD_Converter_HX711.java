package AD_Converter_HX711;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test class to check and debug the function of a HX711 converter board with a weight cell
 *
 */
public class Test_AD_Converter_HX711
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Test_AD_Converter_HX711.class);

    private static final int HX711_CLOCK_IN_PIN   = 19;
    private static final int HX711_DATA_OUT_PIN   = 20;


    /**
     * main to start the test
     * @param args
     */
    public static void main(String[] args)
    {
        LOGGER.info("Test_AD_Converter_HX711 main invoked");
        LOGGER.info(getJavaInfo());

        LOGGER.info("call testHX711");
        testHX711();

        LOGGER.info("Test_AD_Converter_HX711 main exit");
    }


    /**
     * Test method for the HX711 AD-converter.
     * creates a converter object with the given pin numbers initialises the hardware
     * and runs severeal conversion on it. the values are logged for reference
     */
    private static void testHX711()
    {
        ConverterHX711 testComponent = new ConverterHX711(HX711_DATA_OUT_PIN, HX711_CLOCK_IN_PIN);

        testComponent.setCalibrationFactor(-22); // empirically found value...

        testComponent.resetConverter();

        // the HX711 shows a strange behavior after startup. some FFFFFF readings are skipped by these dummy reads
        LOGGER.info("dummy read: " + (int)testComponent.getForceInGram(10));

        int iRemainingTests = 10;
        while (iRemainingTests-- > 0)
        {
            // now the zero weight can be measured and set
            testComponent.tare(5);
            int iRemainingLoops = 50;
            while (iRemainingLoops-- > 0)
            {
                double dbVal = testComponent.getForceInGram(2);

                System.out.print("\rgetForceInGram: " + (int)dbVal + " gram");
                //LOGGER.info("getForceInGram: " + (int)dbVal + " gram\r");

                //SleepUtil.sleepMicros(100);
            }
        }
    }


    /**
     * Return the java path for faster identification of file location problems
     * @return
     */
    private static String getJavaInfo()
    {
        return System.getProperty("java.library.path");
    }



}
