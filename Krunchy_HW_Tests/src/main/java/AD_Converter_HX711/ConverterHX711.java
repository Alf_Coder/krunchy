package AD_Converter_HX711;

import com.diozero.api.DigitalInputDevice;
import com.diozero.api.DigitalOutputDevice;
import com.diozero.api.GpioEventTrigger;
import com.diozero.api.GpioPullUpDown;
import com.diozero.util.SleepUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 *  HX711 Java Library based on ideas from a python project in
 *  https://github.com/tatobari/hx711py
 *
 *  still room for improvments...
 */
public class ConverterHX711
{
    private final Logger LOGGER = LoggerFactory.getLogger(ConverterHX711.class);

    private static final int HX711_RESOLUTION_NR_OF_BITS = 24;

    private DigitalInputDevice  m_pinHx711Data;  // a output pin of the converter is a input pin on the Raspi
    private DigitalOutputDevice m_pinHx711Clock; // a input pin of the converter is a output pin on the Raspi

    private int m_iCalibrationFactor = 1; // what weight unit is calculated (gram vs. newton)
    private int m_iOffset = 0;            // offset to 0 weight (tare)

    /**
     * Init constructor for a converter object
     * @param p_Hx711DataPinNr     pin number where the Raspi can read the data of the HX711
     * @param p_iHx711ClockPinNr   pin number where the Raspi can clock the bits out of the HX711
     */
    public ConverterHX711(int p_Hx711DataPinNr, int p_iHx711ClockPinNr)
    {
        m_pinHx711Clock = new DigitalOutputDevice(p_iHx711ClockPinNr, true, false);
        m_pinHx711Data = new DigitalInputDevice(p_Hx711DataPinNr, GpioPullUpDown.PULL_DOWN, GpioEventTrigger.NONE);

        LOGGER.info("DataPin is {}", m_pinHx711Data.getPullUpDown().toString());
    }


    /**
     * Return true if the converter is ready for data transfer
     *   ref. HX711 datasheet:
     *   When output data is not ready for retrieval, digital output pin DOUT is high.
     *   Serial clock input PD_SCK should be low (keep it power-up).
     *   When DOUT goes to low, it indicates data is ready for retrieval.
     *
     * @return boolean
     */
    private boolean isReady()
    {
        return !m_pinHx711Data.isActive();
    }


    /**
     * Set the HX711 into power safe mode
     *   ref. HX711 datasheet:
     *   Pin PD_SCK input is used to power down the HX711.
     *   When PD_SCK Input is low, chip is in normal working mode.
     *   When PD_SCK pin changes from low to high and stays at high for longer than 60μs,
     *   HX711 enters power down mode.
     *   When PD_SCK returns to low, chip will reset and enter normal operation mode.
     */
    private void powerDown()
    {
        m_pinHx711Clock.off();
        m_pinHx711Clock.on();
        SleepUtil.sleepMicros(100); // longer than 60
    }


    /**
     * Wake-up the HX711 from power safe mode
     *   ref. HX711 datasheet:
     *   When PD_SCK returns to low, chip will reset and enter normal operation mode.
     */
    private void powerUp()
    {
        m_pinHx711Clock.off();
    }


    /**
     * Resetting the HX711 is performed by cycle the power mode
     */
    public void resetConverter()
    {
        powerDown();
        powerUp();
    }


    /**
     * Setter for the value offset (tare)
     * @param p_iOffset  the new tare value
     */
    private void setOffset(int p_iOffset)
    {
        LOGGER.info("tare new offset: " + p_iOffset);

        m_iOffset = p_iOffset;
    }


    /**
     * Getter for the value offset (tare)
     * @return int
     */
    private int getOffset()
    {
        return m_iOffset;
    }


    /**
     * Set the multiplication Factor for Units-per-Gram calibration
     * @param p_iReferenceUnit  int
     */
    public void setCalibrationFactor(int p_iReferenceUnit)
    {
        if (p_iReferenceUnit != 0)
        {
            m_iCalibrationFactor = p_iReferenceUnit;
        }
        else
        {
            // do not allow div by zero
            m_iCalibrationFactor = 1;
        }
    }


    /**
     * Perform a tare function by reading the average of N conversion with 0 tare and
     * storing that number as the new offset
     * @param  p_iTimesToAverage the number of convertions to be averaged
     */
    public void tare(int p_iTimesToAverage)
    {
        LOGGER.info("tare old offset: " + getOffset());

        setOffset(0);
        int iSum = readAverage(p_iTimesToAverage);
        setOffset(iSum);
    }


    /**
     * Return the current force applied to the weight cell using the average value of
     * N conversions corrected by the calibration value
     * @param  p_iTimesToAverage the number of convertions to be averaged
     * @return double
     */
    public double getForceInGram(int p_iTimesToAverage)
    {
        double dbValue = getValue(p_iTimesToAverage); // is already tare corrected

        return dbValue / m_iCalibrationFactor;
    }


    /**
     * Returns the value of N reads from the converter adjusted with the tare value
     *
     * @param  p_iTimesToAverage the number of convertions to be averaged
     * @return double
     */
    private double getValue(int p_iTimesToAverage)
    {
        double dbAvgValue = readAverage(p_iTimesToAverage);
        double dbCorrectedValue = dbAvgValue - m_iOffset;

        //LOGGER.info(String.format("getValue " + p_iTimesToAverage + " times / avg: " + dbAvgValue + " corrected: " + dbCorrectedValue));

        return dbCorrectedValue;
    }


    /**
     * Returns the average readout value of N conversions fom the HX711
     *
     * @param  p_iTimesToAverage the number of converter values to be averaged
     * @return int
     */
    private int readAverage(int p_iTimesToAverage)
    {
        long lSumValues = 0; // java long is 8 bytes, should be large enough for a sum of int
        for (int iLoop = 0; iLoop < p_iTimesToAverage; iLoop++)
        {
            lSumValues = lSumValues + readRawBits();
        }
        return (int) (lSumValues / p_iTimesToAverage);
    }


    /**
     * Read the current conversion result from the HX711 and return it as a
     * int number: 4 bytes i.e. signed 32 bits long
     *
     * @return int the raw data read from the converter
     */
    private int readRawBits()
    {

        int iRawBits = 0;

        // waiting for the converter to signal it has data ready for transmission
        while (!isReady()) ; // how to avoid an endless loop in case of defect?!

        // fill the bits into the int var, MSBit comes in first
        for (int iBitNumber = HX711_RESOLUTION_NR_OF_BITS - 1; iBitNumber >= 0; iBitNumber--)
        {
            // get the n-th bit at the data output. use the same command as an additional delay
            // of some nano more seconds due to the signal conditioning propagation delay
            for (int iDelayLoop = 0 ; iDelayLoop < 10; iDelayLoop++)
            {
                m_pinHx711Clock.on();
            }
            //m_pinHx711Clock.off();

            iRawBits = iRawBits << 1; // spend the pulse duration time to shift the bits

            //m_ledReadIndicator.on();

            // by now the data output of the converter should have the bit presented, read it
            if (m_pinHx711Data.isActive())
            {
                iRawBits = iRawBits + 1; // add a '1' bit as the current LSB
            }

            //m_ledReadIndicator.off();

            for (int iDelayLoop = 0 ; iDelayLoop < 4; iDelayLoop++)
            {
                m_pinHx711Clock.off();
            }

        }

        // the current read cycle must also configure the mode for the next conversion
        // add one clock pulse without read, the data pin should be 1
        for (int iDelayLoop = 0 ; iDelayLoop < 1; iDelayLoop++)
        {
            m_pinHx711Clock.on();
        }

        // the HX711 returns 2s complement numbers i.e. fill the remaining bits for sign correctness
        int iSignBit = 1 << (HX711_RESOLUTION_NR_OF_BITS - 1); // with 24bits this would be 00000000 10000000 00000000 00000000

        boolean bIsNegativeNumber = (iRawBits & iSignBit) > 0;
        if (bIsNegativeNumber)
        {
            // the captured value is negative, fill the leading bits with 1's
            int iNegativeSignMask = -2 << (HX711_RESOLUTION_NR_OF_BITS - 1);
                                                     // -2 is e.g. 11111111 11111111 11111111 11111110
                                                     // shift to   11111111 00000000 00000000 00000000

            iRawBits = iRawBits | iNegativeSignMask; // e.g. from  00000000 1xxxxxxx xxxxxxxx xxxxxxxx
                                                     //      to    11111111 1xxxxxxx xxxxxxxx xxxxxxxx
        }

        for (int iDelayLoop = 0 ; iDelayLoop < 1; iDelayLoop++)
        {
            m_pinHx711Clock.off();
        }

        //LOGGER.info(String.format("%10X\t", iRawBits));

        return iRawBits;
    }
}