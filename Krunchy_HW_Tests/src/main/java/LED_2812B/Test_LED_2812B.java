package LED_2812B;

import com.diozero.ws281xj.LedDriverInterface;
import com.diozero.ws281xj.PixelAnimations;
import com.diozero.ws281xj.rpiws281x.WS281x;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
        * @author Mirko J. Helbling
        * @date 10.11.2018
        * The class Test_LED_2812B is used to check the functionality of the WS2812B LEDs. Please consider the scheme
        * for checking the wiring of your Raspberry pi. Please be shure, that you installed the dependencies on your Raspberry Pi.
        * We would hard recommend you to deploy this program with IntelliJ and the Linux embedded Plugin. In this case you should configure
        * the correct ip and password of your raspberry pi within this plug in.
        */
public class Test_LED_2812B {

    private static final Logger LOGGER = LoggerFactory.getLogger(Test_LED_2812B.class);

    private static final int WS2812B_DATA_LINE_PIN = 18;

    private static LedDriverInterface m_LedDriver;

    private static  int iBrightness = 64; // 0..255

    private static  int iNrOfLeds   = 8; // number of LEDs

    private static  int iDelayMs = 150; // delay in ms


    /**
     * @author Mirko J. Helbling
     * The main class calls two test funkctionality for checking the LED hardware. The sense of these two functions is
     * to check the full functionality of all elements within the diods.
     * @param args
     */
    public static void main(String[] args) {
        LOGGER.info("Test_LED_2812B main invoked");

        LOGGER.info(getJavaInfo());

        m_LedDriver = new WS281x(WS2812B_DATA_LINE_PIN, iBrightness, iNrOfLeds);

        LOGGER.info("call testLedColors");
        testLedColors(iDelayMs);

        LOGGER.info("call testLedWhite");
        testLedWhite(iDelayMs);


        LOGGER.info("Test_LED_2812B main exit");
    }


    /**
     * The function testLedWhite checks the interplay between the three color elements within the diods. The result should be
     * a mix of white colors by all light emitting diods.
     * @param iDelayMs
     */
    private static void testLedWhite(int iDelayMs){
        LOGGER.info("testLedWhite invoked");

        for (int iWhiteLevel = 0; iWhiteLevel < 256; iWhiteLevel++)
        {
            for (int iLed = 0; iLed < iNrOfLeds; iLed++)
            {
                m_LedDriver.setRedComponent(iLed,   iWhiteLevel);
                m_LedDriver.setGreenComponent(iLed, iWhiteLevel);
                m_LedDriver.setBlueComponent(iLed,  iWhiteLevel);
            }
            m_LedDriver.render();
            PixelAnimations.delay(iDelayMs);
        }
        m_LedDriver.allOff();
        LOGGER.info("testLedWhite exit");

    }

    /**
     * The function testLedColors checks the colour components individually. So you should see firstly RED light then green
     * light and at last blue light.
     * @param iDelayMs
     */
    private static void testLedColors(int iDelayMs){
        LOGGER.info("testLedColors invoked");

        final int I_RED_RGB_NR = 0;
        final int I_GREEN_RGB_NR = 1;
        final int I_BLUE_RGB_NR = 2;


        int iGreenComponent = 255;
        int iRedComponent = 255;
        int iBlueComponent = 255;
        int[] icolors = {iGreenComponent, iRedComponent, iBlueComponent};

        for(int iBaseColor = 0; iBaseColor < icolors.length; iBaseColor++ ){
            for(int iLed = 0; iLed < iNrOfLeds; iLed++ ){
                switch (iBaseColor){
                    case I_RED_RGB_NR :
                        System.out.println("RED");
                        m_LedDriver.setPixelColourRGB(iLed,iRedComponent, 0,0 );
                        break;

                    case I_GREEN_RGB_NR :
                        System.out.println("GREEN");
                        m_LedDriver.setPixelColourRGB(iLed,0,iGreenComponent,0 );
                        break;

                    case I_BLUE_RGB_NR :
                        System.out.println("BLUE");
                        m_LedDriver.setPixelColourRGB(iLed,0,0,iBlueComponent );
                        break;
                }
                m_LedDriver.render();
                PixelAnimations.delay(iDelayMs);
                m_LedDriver.allOff();

            }
            m_LedDriver.allOff();
            PixelAnimations.delay(iDelayMs);
        }

        LOGGER.info("testLedColors exit");


    }


    /**
     *This function returns the java PATH
     */
    private static String getJavaInfo(){
        return System.getProperty("java.library.path");

    }



}


