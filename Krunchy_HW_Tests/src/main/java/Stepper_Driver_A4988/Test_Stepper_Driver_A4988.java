package Stepper_Driver_A4988;

import Experimental.FirstExperiments;
import com.diozero.api.DigitalOutputDevice;
import com.diozero.api.GpioPullUpDown;
import com.diozero.devices.Button;
import com.diozero.devices.LED;
import com.diozero.util.SleepUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mirko J. Helbling
 * @date 22.11.2018
 * The class Test_Stepper_Driver_A4988 is used to check the functionality of the A4988 stepper driver and the particular motor. Please consider the scheme
 * for checking the wiring of your Raspberry pi. Please be shure, that you installed the dependencies on your Raspberry Pi.
 * We would hard recommend you to deploy this program with IntelliJ and the Linux embedded Plugin. In this case you should configure
 * the correct ip and password of your raspberry pi within this plug in.
 */
public class Test_Stepper_Driver_A4988 {


    private static final Logger LOGGER = LoggerFactory.getLogger(FirstExperiments.class);


    private static final int A4988_STEP_PIN   = 27;
    private static final int A4988_DIR_PIN    = 17;
    private static final int A4988_ENABLE_PIN = 22;



    private static final DigitalOutputDevice m_pinEnable = new DigitalOutputDevice(A4988_ENABLE_PIN, false, false);
    private static final DigitalOutputDevice m_pinStep   = new DigitalOutputDevice(A4988_STEP_PIN,   true,  false);
    private static final DigitalOutputDevice m_pinDir    = new DigitalOutputDevice(A4988_DIR_PIN,    true,  false);


    /**
     * @author Mirko J. Helbling
     * The main class calls two test funkctionality for checking the stepper driver and motor hardware. The sense of these two functions is
     * to check the full functionality of all elements within the motor control elements.
     * @param args
     */
    public static void main(String[] args) {
        LOGGER.info("Test_Stepper_Driver_A4988 main invoked");

        LOGGER.info(getJavaInfo());

        LOGGER.info("call stepperTest");
        testStepper();
        LOGGER.info("call rampTest");
        testRamp();

        LOGGER.info("Test_Stepper_Driver_A4988 main close hardware");
        m_pinEnable.close();
        m_pinStep.close();
        m_pinDir.close();
        LOGGER.info("Test_Stepper_Driver_A4988 main exit");

    }


    /**
     * The function stepperTest checks the motor stepps with diffrent pulse durations. During the tests should be the sleep time
     * for each 800 steps 200 US longer. The whole test time is approximently  8 minutes.
     */
    private static void testStepper()
    {
        m_pinEnable.on(); // Enables the motor to move in a particular direction

        int iPulseDurationUs = 20000;
        int iTestLoops = 800;
        int iPulseDurationUsSteps = 200;

        m_pinDir.on();

        while (iTestLoops > 0)
        {
            m_pinStep.on();
            SleepUtil.sleepMicros(iPulseDurationUs);
            m_pinStep.off();
            SleepUtil.sleepMicros(iPulseDurationUs);
            iPulseDurationUs += iPulseDurationUsSteps;
            iTestLoops--;
        }

        m_pinEnable.off(); // Disable the motor power

    }



    /**
     * The function rampTest checks the acceleration and deceleration phase of the motor and the particular driver
     */
    private static void testRamp()
    {
        double dbStartFrequencyHz =  100.0;
        double dbMaxFrequencyHz   = 1400.0;
        double dbAccelerationHz2  = 5000.0;
        double dbDecelerationHz2  = 5000.0;


        m_pinEnable.on(); // Enables the motor to move in a particular direction
        m_pinDir.on(); // move clockwise ???

        //--------------------------------------------
        // acceleration phase
        double dbTimeToAccelerate  = (dbMaxFrequencyHz - dbStartFrequencyHz) / dbAccelerationHz2;
        int iStepsToAccelerate = (int)((dbMaxFrequencyHz - dbStartFrequencyHz) / 2 * dbTimeToAccelerate);
        int iPulseDurationUs = (int)(1000000.0 / dbStartFrequencyHz);
        int iDurationDecrementUs = 1;
        while (iStepsToAccelerate > 0)
        {
            m_pinStep.on();
            SleepUtil.sleepMicros(iPulseDurationUs);
            m_pinStep.off();
            SleepUtil.sleepMicros(iPulseDurationUs);
            iStepsToAccelerate--;
            iPulseDurationUs -= iDurationDecrementUs;
            LOGGER.info("iPulseDurationUs: {} / Frequency: {}", iPulseDurationUs, 1000000/iPulseDurationUs);

        }

        //--------------------------------------------
        // movement phase
        int iStepsForDistance = 1600; // 8 rotations
        iPulseDurationUs = (int)(1000000.0 / dbMaxFrequencyHz);
        while (iStepsForDistance > 0)
        {
            m_pinStep.on();
            SleepUtil.sleepMicros(iPulseDurationUs);
            m_pinStep.off();
            SleepUtil.sleepMicros(iPulseDurationUs);
            iStepsForDistance--;
        }

        //--------------------------------------------
        // deceleration phase
        double dbTimeToDecelerate  = (dbMaxFrequencyHz - dbStartFrequencyHz) / dbDecelerationHz2; // in seconds !
        int iStepsToDeccelerate = (int)((dbMaxFrequencyHz - dbStartFrequencyHz) / 2 * dbTimeToDecelerate);

        iPulseDurationUs = (int)(1000000.0 / dbMaxFrequencyHz);
        int iDurationIncrementUs = 1;
        while (iStepsToDeccelerate > 0)
        {
            m_pinStep.on();
            SleepUtil.sleepMicros(iPulseDurationUs);
            m_pinStep.off();
            SleepUtil.sleepMicros(iPulseDurationUs);
            iStepsToDeccelerate--;
            iPulseDurationUs += iDurationIncrementUs;
            LOGGER.info("iPulseDurationUs: {} / Frequency: {}", iPulseDurationUs, 1000000/iPulseDurationUs);
        }

        m_pinEnable.off(); // Disable the motor power
    }

    /**
     *This function returns the java PATH
     */
    private static String getJavaInfo(){
        return System.getProperty("java.library.path");

    }


}
