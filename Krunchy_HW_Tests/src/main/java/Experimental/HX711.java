package Experimental;

import com.diozero.api.DigitalInputDevice;
import com.diozero.api.DigitalOutputDevice;
import com.diozero.api.GpioEventTrigger;
import com.diozero.api.GpioPullUpDown;
import com.diozero.devices.LED;
import com.diozero.util.SleepUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//===============================================================================
//  HX711 Java Library based on ideas from  https://github.com/tatobari/hx711py
//   purely experimental, needs lots of improvments
//===============================================================================
public class HX711
{
    private static final int HX711_RESOLUTION_NR_OF_BITS = 24;

    private static final int LED_WAITING_FOR_READY = 16;

    private final Logger LOGGER = LoggerFactory.getLogger(FirstExperiments.class);

    private DigitalInputDevice m_pinHx711Data;  // a output pin of the converter is a input pin on the Raspi
    private DigitalOutputDevice m_pinHx711Clock; // a input pin of the converter is a output pin on the Raspi

    private LED m_ledWaitingForReady  ;

    private EconversionMode m_eMode = EconversionMode.CHANNEL_A_128;
    private int m_iReferenceUnit = 1; // what weight unit is calculated

    private int m_iOffset = 0;
    private double m_dbScale = 1.0;


    // init-ctor
    public HX711(int p_Hx711DataPinNr, int p_iHx711ClockPinNr, EconversionMode p_eMode)
    {
        m_eMode = p_eMode;
        m_pinHx711Clock = new DigitalOutputDevice(p_iHx711ClockPinNr, true, false);
        m_pinHx711Data = new DigitalInputDevice(p_Hx711DataPinNr, GpioPullUpDown.PULL_DOWN, GpioEventTrigger.NONE);

        m_ledWaitingForReady = new LED(LED_WAITING_FOR_READY);

        LOGGER.info("DataPin is {}", m_pinHx711Data.getPullUpDown().toString());
    }

    private boolean isReady()
    {
        // ref. HX711 datasheet:
        // When output data is not ready for retrieval, digital output pin DOUT is high.
        // Serial clock input PD_SCK should be low (keep it power-up).
        // When DOUT goes to low, it indicates data is ready for retrieval.

        return !m_pinHx711Data.isActive();
    }

    private void powerDown()
    {
        // ref. HX711 datasheet:
        // Pin PD_SCK input is used to power down the HX711.
        // When PD_SCK Input is low, chip is in normal working mode.
        // When PD_SCK pin changes from low to high and stays at high for longer than 60μs,
        // HX711 enters power down mode.
        // When PD_SCK returns to low, chip will reset and enter normal operation mode.
        m_pinHx711Clock.off();
        m_pinHx711Clock.on();
        SleepUtil.sleepMicros(100);
    }

    private void powerUp()
    {
        // ref. HX711 datasheet:
        // When PD_SCK returns to low, chip will reset and enter normal operation mode.
        m_pinHx711Clock.off();
    }


    public void resetConverter()
    {
        // resetting is performed by cycle the power mode of the HX711
        powerDown();
        powerUp();
    }


    private void setOffset(int p_iOffset)
    {
        System.out.println("tare new offset: " + p_iOffset);

        m_iOffset = p_iOffset;
    }

    private int getOffset()
    {
        return m_iOffset;
    }

    private void setScale(double p_dbScale)
    {
        m_dbScale = p_dbScale;
    }

    private double getScale()
    {
        return m_dbScale;
    }


    public void setReferenceUnit(int p_iReferenceUnit)
    {
        if (p_iReferenceUnit != 0)
        {
            m_iReferenceUnit = p_iReferenceUnit;
        }
        else
        {
            // do not allow dev by zero
            m_iReferenceUnit = 1;
        }
    }

    public int getReferenceUnit()
    {
        return m_iReferenceUnit;
    }


    // set the OFFSET value for tare weight; times = how many times to read the tare value
    public void tare(int p_iTimesToAverage)
    {
        System.out.println("tare old offset: " + getOffset());
        setOffset(0);

        int iSum = readAverage(p_iTimesToAverage);

        setOffset(iSum);
    }


    public double getUnits(int p_iTimesToAverage)
    {
        double dbValue =  getValue(p_iTimesToAverage);
        //System.out.println(("dbValue: " + dbValue));
        //System.out.println(("m_dbScale: " + m_dbScale));

        return dbValue / m_iReferenceUnit;// m_dbScale;
    }


    private double getValue(int p_iTimesToAverage)
    {
        double dbAvgValue = readAverage(p_iTimesToAverage);
        System.out.println("getValue " + p_iTimesToAverage +" times / avg: " + dbAvgValue);

        return dbAvgValue - m_iOffset;
    }


    private int readAverage(int p_iTimesToAverage)
    {
        long lSumValues = 0;
        for (int iLoop = 0; iLoop < p_iTimesToAverage; iLoop++)
        {
            // values += self.read_long()
            lSumValues = lSumValues + readRaw(m_eMode);
        }
        return (int) (lSumValues / p_iTimesToAverage);

    }

    private int readRaw(EconversionMode p_eModeForNextConversion)
    {
        int iRawBits = 0; // 32 bits long with sign

        m_ledWaitingForReady  .on();

        while (!isReady()) ; // could be an endless loop?!

        m_ledWaitingForReady  .off();

        // fill the bits into the long var, MSBit comes in first
        for (int iBitNumber = HX711_RESOLUTION_NR_OF_BITS - 1; iBitNumber >= 0; iBitNumber--)
        {
            m_pinHx711Clock.on();  // get the n-th bit at the data output
            m_pinHx711Clock.on();  // used as an additional delay
            m_pinHx711Clock.on();
            m_pinHx711Clock.on();
            m_pinHx711Clock.on();
            m_pinHx711Clock.on();
            m_pinHx711Clock.on();

            iRawBits = iRawBits << 1; // spend the pulse duration time to shift the bits

            if (m_pinHx711Data.isActive())
            {
                iRawBits = iRawBits + 1; // add a '1' bit as the current LSB
            }

            m_pinHx711Clock.off();
            m_pinHx711Clock.off();
            m_pinHx711Clock.off();
            m_pinHx711Clock.off();
            m_pinHx711Clock.off();
            m_pinHx711Clock.off();

            //String.format("%24X", lRawBits);
            // int iTest = (int) lRawBits;
            //Integer.toString(iTest,2);

            //System.out.println(Integer.toString(iTest,2));

        }

        // the current read must also configure the mode for the next conversion
        switch (p_eModeForNextConversion)
        {
            case CHANNEL_A_64:
            {
                // add three clock pulses
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();

                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                // break; -> no break to issue the additional pulses
            }
            case CHANNEL_B:
            {
                // add two clock pulses
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();

                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                // break; -> no break to issue the additional pulses
            }
            case CHANNEL_A_128:
            {
                // add one clock pulse
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();
                m_pinHx711Clock.on();

                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                m_pinHx711Clock.off();
                break;
            }
            default:
            {
                LOGGER.debug("invalid mode for conversion: readRaw({})", p_eModeForNextConversion);
                break;
            }
        }

        // fill the remaining bits for sign correctness
        int iSignBit = 1 << (HX711_RESOLUTION_NR_OF_BITS - 1);// e.g. 00000000 1000000 00000000 00000000

        //System.out.print(Integer.toString((int) iRawBits,2));

        if ((iRawBits & iSignBit) > 0)
        {
            // the captured value is negative, fill the leading bits with 1's
            int iSignMask = iSignBit - 1;        // e.g.  00000000 01111111 11111111 11111111
            //System.out.println(Integer.toString((int) lSignMask,2));
            iSignMask = iSignMask | iSignBit;     // e.g.  00000000 11111111 11111111 11111111
            //System.out.println(Integer.toString((int) lSignMask,2));
            iSignMask = ~iSignMask;               // e.g.  11111111 00000000 00000000 00000000
            //System.out.println(Integer.toString((int) lSignMask,2));
            iRawBits = iRawBits | iSignMask;      // e.g.  11111111 1xxxxxxx xxxxxxxx xxxxxxxx
        }

        System.out.print(String.format("%10X\t", iRawBits));

        //System.out.println(Integer.toString((int) lRawBits,2));
        //System.out.println(lRawBits);

        return iRawBits;
    }
}