package Experimental;

import com.diozero.api.DigitalOutputDevice;
import com.diozero.api.SpiClockMode;
import com.diozero.api.SpiDevice;
import com.diozero.util.RuntimeIOException;
import com.diozero.util.SleepUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Andy on 21.12.2018.
 */
public class SpiStepperTest
{
    private static final Logger LOGGER = LoggerFactory.getLogger(FirstExperiments.class);

    private static final byte L6470_CMD_NOP          = (byte) 0x00;

    private static final byte L6470_CMD_GET_PARAM    = (byte) 0x20; // + register 01..1B
    private static final byte L6470_CMD_SET_PARAM    = (byte) 0x00; // + register 01..1B

    private static final byte L6470_CMD_GETSTATUS    = (byte) 0xD0;

    private static final byte L6470_CMD_RESET_DEVICE = (byte) 0xC0;
    private static final byte L6470_CMD_RESET_POS    = (byte) 0xD8;

    private static final byte L6470_CMD_GOTO         = (byte) 0x60;
    private static final byte L6470_CMD_RUN          = (byte) 0x50;
    private static final byte L6470_CMD_MOVE         = (byte) 0x40;
    private static final byte L6470_CMD_GOTO_DIR     = (byte) 0x68;
    private static final byte L6470_CMD_GO_UNTIL     = (byte) 0x82;
    private static final byte L6470_CMD_GO_HOME      = (byte) 0x70;
    private static final byte L6470_CMD_GO_MARK      = (byte) 0x78;

    private static final byte L6470_CMD_STEP_CLOCK   = (byte) 0x58;
    private static final byte L6470_CMD_RELEASE_SW   = (byte) 0x92;
    private static final byte L6470_CMD_SOFT_STOP    = (byte) 0xB0;
    private static final byte L6470_CMD_HARD_STOP    = (byte) 0xB8;
    private static final byte L6470_CMD_SOFT_HIZ     = (byte) 0xA0;
    private static final byte L6470_CMD_HARD_HIZ     = (byte) 0xA8;



    private static final byte L6470_PARAM_ABS_POS = 0x01;		///< len=22
    private static final byte L6470_PARAM_EL_POS = 0x02;		///< len=9
    private static final byte L6470_PARAM_MARK = 0x03;		///< len=22
    private static final byte L6470_PARAM_SPEED = 0x04;		///< len=20
    private static final byte L6470_PARAM_ACC = 0x05;			///< len=12
    private static final byte L6470_PARAM_DECEL = 0x06;		///< len=12
    private static final byte L6470_PARAM_MAX_SPEED  = 0x07;
    private static final byte L6470_PARAM_MIN_SPEED  = 0x08;
    private static final byte L6470_PARAM_KVAL_HOLD  = 0x09;
    private static final byte L6470_PARAM_KVAL_RUN   = 0x0A;
    private static final byte L6470_PARAM_KVAL_ACC   = 0x0B;
    private static final byte L6470_PARAM_KVAL_DEC   = 0x0C;
    private static final byte L6470_PARAM_INT_SPD    = 0x0D;
    private static final byte L6470_PARAM_ST_SLP     = 0x0E;
    private static final byte L6470_PARAM_FN_SLP_ACC = 0x0F;
    private static final byte L6470_PARAM_FN_SLP_DEC = 0x10;
    private static final byte L6470_PARAM_K_THERM    = 0x11;
    private static final byte L6470_PARAM_ADC_OUT    = 0x12;
    private static final byte L6470_PARAM_OCD_TH     = 0x13;
    private static final byte L6470_PARAM_STALL_TH   = 0x14;
    private static final byte L6470_PARAM_FS_SPD     = 0x15;
    private static final byte L6470_PARAM_STEP_MODE  = 0x16;
    private static final byte L6470_PARAM_ALARM_EN   = 0x17;
    private static final byte L6470_PARAM_CONFIG     = 0x18;
    private static final byte L6470_PARAM_STATUS     = 0x19;



    private static double m_dbTick = 0.00000025; // depends on config
    private static long m_lMicrostepping = 1;

    private static boolean m_bLoggingNeeded = true;
    private static long m_lLastStatus = 0;

    private static DigitalOutputDevice m_pinBoardReset;
    private static DigitalOutputDevice m_pinMotorPowerOn;
    private static DigitalOutputDevice m_pinCoolingFanOn;



    private static DigitalOutputDevice                 m_ledStart;
    private static DigitalOutputDevice                 m_ledStop;


    private static SpiDevice m_StepperBoard;

    //===============================================================================
    //  MAIN
    //===============================================================================
    public static void main(String[] args)
    {
        LOGGER.info("SpiStepperTest main invoked");

        int iController = 0;
        int iChipSelect = 0;
        int iFrequency  = 500000;
        SpiClockMode eMode = SpiClockMode.MODE_3; // L6470 needs this mode
        boolean bLsbFirst = false;                // -> LSB is last byte
        try
        {
            m_StepperBoard = new SpiDevice(iController, iChipSelect, iFrequency, eMode, bLsbFirst);
        }
        catch(RuntimeIOException exception)
        {
            LOGGER.error("SpiDevice not created:\n{}", exception.getMessage());
        }

        LOGGER.info("SpiDevice has Chipselect: {}", m_StepperBoard.getChipSelect());
        LOGGER.info("SpiDevice has Controller: {}", m_StepperBoard.getController());

        m_pinBoardReset   = new DigitalOutputDevice(6, false, true);
        m_pinMotorPowerOn = new DigitalOutputDevice(26, true, false);
        m_pinCoolingFanOn = new DigitalOutputDevice(27, true, false);

//        m_ledStart.close();
//        m_ledStop.close();

//        m_pinCoolingFanOn.off();
//        SleepUtil.sleepMillis(3000);
        m_pinCoolingFanOn.on();
//        SleepUtil.sleepMillis(3000);

        // L6470 needs motor power ON, even for communication
        m_pinMotorPowerOn.on();
        SleepUtil.sleepMillis(300);

        m_pinBoardReset.off(); // release the board from hard reset, start with default parameters
        SleepUtil.sleepMillis(250);

        cmdHardHiZ(); // to allow the following configuration settings
        enableSwitchMode(true); // do not hard stop on switch event

        listConfiguration();

        listStatus();


        testGetConfigRegister();
        //testResetDevice();

        //testGetStatus();
//        checkStatus();   // get status cmd resets the latched flags

        //testParamSetup();

        //testSetAbsPos();



        LOGGER.info("SpiStepperTest start test movements");
        testMovements();
        //testGetAbsolutePosition();





        SleepUtil.sleepMillis(1000);
        m_pinMotorPowerOn.off();
        SleepUtil.sleepMillis(800);

        m_pinCoolingFanOn.off();

        LOGGER.info("SpiStepperTest main close hardware");
        m_StepperBoard.close();

        LOGGER.info("SpiStepperTest main exit");

    }

    private static void cmdHardHiZ()
    {
        // send a reset command byte to the only SPI slave
        byte[] commandByte  = {L6470_CMD_HARD_HIZ};
        m_StepperBoard.write(commandByte);
    }


    private static void listConfiguration()
    {
        // get the configuration register
        int iConfig = getConfigRegister();

        String sConfig = String.format("0x%04X", iConfig);

        // log the configuration in the registers
        LOGGER.info("Configuration Register ---------- {} ----------", sConfig);


        LOGGER.info("Configuration Register --------------------------");

    }


    private static void listStatus()
    {
        int iStatus = (int)getStatusRegister();
        String sStatus = String.format(" 0x%04X", iStatus);

        // log the states in the registers
        LOGGER.info("Status Register ----------{} ----------", sStatus);

        // When HiZ flag is high it indicates that the bridges are in high impedance state.
        // Whichever motion command makes the device to exit from High Z state (HardStop
        // and SoftStop included), unless error flags forcing a High Z state are active.
        if ((iStatus & 0b0000000000000001) == 0)
        {
            LOGGER.info("High-Z:     \tH-bridges are active");
        }
        else
        {
            LOGGER.info("High-Z:     \tH-bridges are inactive");
        }

        // The BUSY bit reflects the /BUSY pin status. The BUSY flag is low when a constant
        // speed, positioning or motion command is under execution and is released (high) after
        // the command have been completed.
        if ((iStatus & 0b0000000000000010) == 0)
        {
             LOGGER.info("Busy:      \tBoard is busy");
        }
        else
        {
            LOGGER.info("Busy:       \tBoard is not busy");
        }

        // The SW_F report the SW input status (low for open and high for closed).
        if ((iStatus & 0b0000000000000100) == 0)
        {
            LOGGER.info("SwitchState:\tSwitch is inactive");
        }
        else
        {
            LOGGER.info("SwitchState:\tSwitch is active");
        }

        // The SW_EVN flag is active high and indicates a switch turn-on event (SW input falling edge).
        // The flag is latched: when the respective conditions make it high it remain in that
        // state until a GetStatus command is sent to the board.
        if ((iStatus & 0b0000000000001000) == 0)
        {
            LOGGER.info("SwitchEvent:\tSwitch has not turned ON");
        }
        else
        {
            LOGGER.info("SwitchEvent:\tSwitch has turned ON");
        }

        // The DIR bit indicates the current motor direction:
        if ((iStatus & 0b0000000000010000) == 0)
        {
            LOGGER.info("Direction:  \tDirection is UP");
        }
        else
        {
            LOGGER.info("Direction:  \tDirection is DOWN");
        }

        // MOT_STATUS indicates the current motor status:
        int iMotorState = (iStatus & 0b0000000001100000) >> 5;
        switch (iMotorState)
        {
        case 0b00:
        {
            LOGGER.info("MotorState: \tMotor is in state Stopped");
            break;
        }
        case 0b01:
        {
            LOGGER.info("MotorState: \tMotor is in state Acceleration");
            break;
        }
        case 0b10:
        {
            LOGGER.info("MotorState: \tMotor is in state Deceleration");
            break;
        }
        case 0b11:
        {
            LOGGER.info("MotorState: \tMotor is in state Constant Speed");
            break;
        }
        }

        // The NOTPERF_CMD flag is active high and indicates that the command received by SPI can't be
        // performed. The flag is latched: when the respective conditions make it high it remain in that
        // state until a GetStatus command is sent to the board.
        if ((iStatus & 0b0000000010000000) == 0)
        {
            LOGGER.info("NotPerform: \tLast command could be performed");
        }
        else
        {
            LOGGER.info("NotPerform: \tLast command could NOT be performed");
        }

        // The WRONG_CMD flag is active high and indicates that the command received by SPI does not
        // exist. The flag is latched: when the respective conditions make it high it remain in that
        // state until a GetStatus command is sent to the board.
        if ((iStatus & 0b0000000100000000) == 0)
        {
            LOGGER.info("WrongCmd:   \tLast command is valid");
        }
        else
        {
            LOGGER.info("WrongCmd:   \tLast command is UNKNOWN");
        }

        // The UVLO flag is active low and is set by an under-voltage lock out or reset events (powerup
        // included). The flag is latched: when the respective conditions make it low it remain in that state until
        // a GetStatus command is sent to the board
        if ((iStatus & 0b0000001000000000) == 0)
        {
            LOGGER.info("UnderVoltage:\tBus voltage is too low");
        }
        else
        {
            LOGGER.info("UnderVoltage:\tBus voltage is ok");
        }


        // The TH_WRN flag is active low and indicates temperature warning level reached. The flag is latched:
        // when the respective conditions make it low it remain in that state until a GetStatus command is
        // sent to the board.
        if ((iStatus & 0b0000010000000000) == 0)
        {
            LOGGER.info("WarnLevel: \tTemperature warning level reached");
        }
        else
        {
            LOGGER.info("WarnLevel: \tTemperature below warning level");
        }

        // The TH_SD flag is active low and indicates a thermal shutdown. The flag is latched:
        // when the respective conditions make it low it remain in that state until a GetStatus
        // command is sent to the board.

        if ((iStatus & 0b0000100000000000) == 0)
        {
            LOGGER.info("StopLevel: \tTemperature shut down level reached");
        }
        else
        {
            LOGGER.info("StopLevel: \tTemperature below shut down level");
        }

        // The OCD flag is active low and indicates a over-current detection event. The flag is latched:
        // when the respective conditions make it low it remains in that state until a GetStatus
        // command is sent to the board.
        if ((iStatus & 0b0001000000000000) == 0)
        {
            LOGGER.info("OverCurrent:\tOvercurrent level reached");
        }
        else
        {
            LOGGER.info("OverCurrent:\tOvercurrent level NOT reached");
        }

        // STEP_LOSS_A flag is forced low when a stall is detected on bridge A. The flag is latched:
        // when the respective conditions make it low it remains in that state until a GetStatus
        // command is sent to the board.
        if ((iStatus & 0b0010000000000000) == 0)
        {
            LOGGER.info("StepLoss A:\tSteploss current reached on phase A");
        }
        else
        {
            LOGGER.info("StepLoss A:\tSteploss current ok on phase A");
        }

        // STEP_LOSS_B flag is forced low when a stall is detected on bridge B. The flag is latched:
        // when the respective conditions make it low it remains in that state until a GetStatus
        // command is sent to the board.
        if ((iStatus & 0b0100000000000000) == 0)
        {
            LOGGER.info("StepLoss B:\tSteploss current reached on phase B");
        }
        else
        {
            LOGGER.info("StepLoss B:\tSteploss current ok on phase B");
        }

        // The SCK_MOD bit is an active high flag indicating that the device is working in step clock mode.
        // In this case the step clock signal should be provided through STCK input pin.
        if ((iStatus & 0b1000000000000000) == 0)
        {
            LOGGER.info("StepClockMode:\tBoard is working in constant speed mode");
        }
        else
        {
            LOGGER.info("StepClockMode:\tBoard is working in step-clock mode");
        }
        LOGGER.info("Status Register --------------------------");
    }


    private static void testParamSetup()
    {
        testGetConfigRegister();
        testGetStatusRegister();
        checkStatus();

        testGetStatus();
        checkStatus();

        // Step Mode Settings
        getStepMode();
        testSetFullStepMode();
        getStepMode();

        checkStatus();

        // Speed  Settings



        // Phase Current  Settings
        testGetSetKVal_Hold();

//        getKVal_Run();
//        getKVal_Acc();
//        getKVal_Dec();

        // Absolute Position
        testGetSetAbsPos();
        checkStatus();

        testResetPos();
        checkStatus();

    }


    private static void testMovements()
    {
        resetAndConfigureDevice();
        testGetConfigRegister();
        LOGGER.info("testMovements configuration finished");

        // Max Speed
        testSetMaxSpeed(); //todo
        //checkStatus();

        //testSetMinSpeed();
        SleepUtil.sleepMillis(100);
        //checkStatus();

        //testGoTo(7000);
        //checkStatus();
        //SleepUtil.sleepMillis(7000);

        testGetStatus();
        run(700, 1);
        checkStatus();
        SleepUtil.sleepMillis(1000);
        checkStatus();
        run(800,1);
        SleepUtil.sleepMillis(3000);
        run(900, 0);
        SleepUtil.sleepMillis(1000);
        run(700, 1);
        SleepUtil.sleepMillis(1000);
        run(600, 0);
        SleepUtil.sleepMillis(3000);
        run(500, 0);
        SleepUtil.sleepMillis(1000);
        run(400, 0);
        SleepUtil.sleepMillis(1000);
        run(300, 0);
        SleepUtil.sleepMillis(1000);
        run(350, 0);
        SleepUtil.sleepMillis(50);
        run(400, 0);
        SleepUtil.sleepMillis(50);
        run(450, 0);
        SleepUtil.sleepMillis(50);
        run(500, 0);
        SleepUtil.sleepMillis(50);
        run(350, 0);
        SleepUtil.sleepMillis(250);
        testSoftStop();

        checkStatus();

        goUntil(350, 1);
        SleepUtil.sleepMillis(10000);

        LOGGER.info("testMovements movements finished");

        checkStatus();

        testSoftStop();
        checkStatus();

    }


    private static void resetAndConfigureDevice()
    {
        //softResetDevice();

        checkStatus();
        enableSwitchMode(true); // do not hard stop on switch event
        checkStatus();
        testGetConfigRegister();

        // set alarm flag pin when thermal warning is on in order to activate the cooling fan
        byte byAlarmFlagSource = 0b00000100;
        //byte byAlarmFlagSource = 0b00000101; // incl. overcurrent
        setAlarmCondition(byAlarmFlagSource);



        // set the motor holding-current
        setMotorCurrentHold(350);

        // set the motor acceleration-current
        setMotorCurrentAcc(3500);

        // set the motor deceleration-current
        setMotorCurrentDec(350);

        // set the motor running-current
        setMotorCurrentRun(1500);

        // disable supply voltage compensation
        enableVoltageCompensation(false);

        // set back emf compensation
        setStartSlope(0);
        setFinalSlopeAcc(0);
        setFinalSlopeDec(0);

        // disable thermal winding compensation
        setThermalWindingCompensation(0);

        // set overcurrent threshold
        setOverCurrentThreshold(2100);

        // set stall current threshold
        setStallCurrentThreshold(1900);

        // set to 1/4 microstep
        setStepMode(6);

        setMinSpeed(false, 100);
        setMaxSpeed(1000);
        setAcceleration(10); // in step/s^2
        setDeceleration(100);

        setFullStepFrequency(2000);

    }

    private static void setFullStepFrequency(long p_lFrequency)
    {
        byte[] commandBytes  = new byte[3];
        commandBytes[0]  = L6470_CMD_SET_PARAM + L6470_PARAM_FS_SPD;

        long lStepsPerSecond = p_lFrequency;
        long lStepsPerTick   = (long)((double)lStepsPerSecond * m_dbTick / Math.pow(2, -24) - 0.5);

        commandBytes[1]  = (byte) ((lStepsPerTick & 0b0000001100000000) >>  8);
        commandBytes[2]  = (byte) ((lStepsPerTick & 0b0000000011111111) >>  0);
    }

    private static void setMinSpeed(boolean p_bOptimizationEnabled, long p_lMinSpeed)
    {
        byte[] commandBytes  = new byte[3];
        commandBytes[0]  = L6470_CMD_SET_PARAM + L6470_PARAM_MIN_SPEED;

        if (p_bOptimizationEnabled)
        {
            // L6470 is using speed 0 when optimization is enabled
            commandBytes[1]  = (byte) 0b0001000000000000;
            commandBytes[2]  = (byte) 0b0000000000000000;
        }
        else
        {
            long lStepsPerTick = calcMinSpeed(p_lMinSpeed);
            commandBytes[1]  = (byte) ((lStepsPerTick & 0b0000111100000000) >>  8);
            commandBytes[2]  = (byte) ((lStepsPerTick & 0b0000000011111111) >>  0);
        }

        m_StepperBoard.write(new byte[]{commandBytes[0]});
        m_StepperBoard.write(new byte[]{commandBytes[1]});
        m_StepperBoard.write(new byte[]{commandBytes[2]});
    }

    private static long calcMinSpeed(long p_lMinSpeed)
    {
        long lStepsPerSecond = p_lMinSpeed;
        long lStepsPerTick   = (long)((double)lStepsPerSecond * m_dbTick / Math.pow(2, -24));
        // can be reduced to (double)lStepsPerSecond * 4.194304

        // parameter correction:
        // min speed is positive and not larger than 0x0FFF
        if (lStepsPerTick > 0x0FFF)
        {
            lStepsPerTick = 0x0FFF;
        }
        else if (lStepsPerTick < 0)
        {
            lStepsPerTick = 0;
        }

        return lStepsPerTick;
    }


    private static void setMaxSpeed(long p_lMaxSpeed)
    {
        byte[] commandBytes  = new byte[3];
        commandBytes[0]  = L6470_CMD_SET_PARAM + L6470_PARAM_MAX_SPEED;

        long lStepsPerTick = calcMaxSpeed(p_lMaxSpeed);
        commandBytes[1]  = (byte) ((lStepsPerTick & 0b0000001100000000) >>  8);
        commandBytes[2]  = (byte) ((lStepsPerTick & 0b0000000011111111) >>  0);
    }


    private static long calcMaxSpeed(long p_lMaxSpeed)
    {
        long lStepsPerSecond = p_lMaxSpeed;
        long lStepsPerTick   = (long)((double)lStepsPerSecond * m_dbTick / Math.pow(2, -18));
        // can be reduced to (double)lStepsPerSecond * 0.065536

        // parameter correction:
        // max speed is positive and not larger than 0x03FF (10 bits)
        if (lStepsPerTick > 0x03FF)
        {
            lStepsPerTick = 0x03FF;
        }
        else if (lStepsPerTick < 0)
        {
            lStepsPerTick = 0;
        }

        return lStepsPerTick;
    }


    private static void setAcceleration(long p_lSteps)
    {
        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_KVAL_ACC};
        byte[] dataByte  = {getAccDecSpeed(p_lSteps)};

        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);
    }

    private static void setDeceleration(long p_lSteps)
    {
        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_KVAL_DEC};
        byte[] dataByte  = {getAccDecSpeed(p_lSteps)};

        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);
    }

    private static byte getAccDecSpeed(long p_lSteps)
    {
        double dbStepPerTickSquare = ((double)p_lSteps * m_dbTick * m_dbTick)/Math.pow(2, -40);
        int i = (int)Math.round(dbStepPerTickSquare);
        LOGGER.info("Acc/Dec steps({}) = {} round {}", p_lSteps, dbStepPerTickSquare, i);
        return (byte) (int)Math.round(dbStepPerTickSquare);
    }




    private static void setMotorCurrentHold(long p_lCurrentMilliAmp)
    {
        // calculate the KVal for this current and set the register accordingly
        //TODO
        // calculate the KVal value out of the given current by using the motor
        // specs and the supply voltage
        long lKValNominal = 25;

        long lKvalHold = 0x37;

        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_KVAL_HOLD};
        byte[] dataByte  = {(byte) lKvalHold};

        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);

    }

    private static void setMotorCurrentAcc(long p_lCurrentMilliAmp)
    {
        // calculate the KVal for this current and set the register accordingly
        //TODO: calculate the KVal value out of the given current by using the motor specs and the supply voltage
        long lKValNominal = 25;

        long lKvalAcceleration = 0x37;

        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_KVAL_ACC};
        byte[] dataByte  = {(byte) lKvalAcceleration};

        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);
    }


    private static void setMotorCurrentDec(long p_lCurrentMilliAmp)
    {
        // calculate the KVal for this current and set the register accordingly
        //TODO: calculate the KVal value out of the given current by using the motor specs and the supply voltage
        long lKValNominal = 25;

        long lKvalDeceleration = 0x37;

        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_KVAL_DEC};
        byte[] dataByte  = {(byte) lKvalDeceleration};

        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);
    }

    private static void setMotorCurrentRun(long p_lCurrentMilliAmp)
    {
        // calculate the KVal for this current and set the register accordingly
        //TODO: calculate the KVal value out of the given current by using the motor specs and the supply voltage
        long lKValNominal = 25;

        long lKvalRun = 0x37;

        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_KVAL_RUN};
        byte[] dataByte  = {(byte) lKvalRun};

        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);
    }

    private static int getConfigRegister()
    {
        // read the configuration bytes in single byte arrays
        byte[] commandByte  = {L6470_CMD_GET_PARAM + L6470_PARAM_CONFIG}; // 0x20 + 0x18 = 0x38
        byte[] configBytes = new byte[2];

        m_StepperBoard.write(commandByte);
        configBytes[0] = m_StepperBoard.writeAndRead(new byte[]{L6470_CMD_NOP})[0];
        configBytes[1] = m_StepperBoard.writeAndRead(new byte[]{L6470_CMD_NOP})[0];

        int iConfigRegister = ((configBytes[0] << 8) & 0x0000FF00) + (configBytes[1] & 0x000000FF);
        return iConfigRegister;
    }

    private static void setConfigRegister(byte[] p_byParameter)
    {
        // write the new configuration bytes in single byte arrays
        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_CONFIG};
        byte[] configByte1 = {p_byParameter[0]};
        byte[] configByte2 = {p_byParameter[1]};

        String sCommand = String.format("0x%02X 0x%02X 0x%02X", commandByte[0], configByte1[0], configByte2[0]);
        LOGGER.info("setConfigRegister({})", sCommand);

        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(configByte1);
        m_StepperBoard.write(configByte2);
    }


    private static void enableSwitchMode(boolean p_bEnableState)
    {
        // inefficient version of the config register setting, since the current
        // state must be read before write in order to modify only one flag

        // read the current configuration bytes
        byte[] commandByte  = {L6470_CMD_GET_PARAM + L6470_PARAM_CONFIG}; // 0x20 + 0x18 = 0x38
        byte[] oneByteRead  = {L6470_CMD_NOP};
        byte[] configBytes = new byte[2];

        m_StepperBoard.write(commandByte);
        configBytes[0] = m_StepperBoard.writeAndRead(oneByteRead)[0];
        configBytes[1] = m_StepperBoard.writeAndRead(oneByteRead)[0];

        String sCommand = String.format("0x%02X 0x%02X 0x%02X", commandByte[0], configBytes[0], configBytes[1]);
        LOGGER.info("Config register read {}", sCommand);

        // write the new configuration bytes in single byte arrays
        commandByte[0] = L6470_CMD_SET_PARAM + L6470_PARAM_CONFIG;
        byte[] configByte1 = {configBytes[0]};
        byte[] configByte2 = {configBytes[1]};

        if (p_bEnableState)
        {
            LOGGER.info("enableSwitchMode(1)");
            configBytes[1] = (byte) (configBytes[1] | 0b00010000);
            configByte2[0] = (byte) (configByte2[0] | 0b00010000);
        }
        else
        {
            LOGGER.info("enableSwitchMode(0)");
            configBytes[1] = (byte) (configBytes[1] & 0b11101111);
            configByte2[0] = (byte) (configByte2[0] & 0b11101111);
        }

        setConfigRegister(configBytes);

        sCommand = String.format("0x%02X 0x%02X 0x%02X", commandByte[0], configByte1[0], configByte2[0]);
        LOGGER.info("Config register write {}", sCommand);

//        m_StepperBoard.write(commandByte);
//        m_StepperBoard.write(configByte1);
//        m_StepperBoard.write(configByte2);
    }

    private static void enableVoltageCompensation(boolean p_bEnableState)
    {
        // inefficient version of the config register setting, since the current
        // state must be read before write in order to modify only one flag

        byte[] commandByte  = {L6470_CMD_GET_PARAM + L6470_PARAM_CONFIG};
        byte[] oneByteRead  = {L6470_CMD_NOP};
        byte[] configReturn = new byte[2];

        m_StepperBoard.write(commandByte);
        configReturn[0] = m_StepperBoard.writeAndRead(oneByteRead)[0];
        configReturn[1] = m_StepperBoard.writeAndRead(oneByteRead)[0];

        commandByte[0] = L6470_CMD_SET_PARAM + L6470_PARAM_CONFIG;
        byte[] configByte1 = {configReturn[0]};
        byte[] configByte2 = {configReturn[1]};

        if (p_bEnableState)
        {

        }
        else
        {

        }

        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(configByte1);
        m_StepperBoard.write(configByte2);
    }

    private static void setStartSlope(long p_lStartSlope)
    {
        // When ST_SLP, FN_SLP_ACC and FN_SLP_DEC parameters are
        // set to zero no BEMF compensation is performed.
        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_ST_SLP};
        byte[] dataByte  = {(byte)p_lStartSlope};
        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);
    }

    private static void setFinalSlopeAcc(long p_lFinalSlope)
    {
        // When ST_SLP, FN_SLP_ACC and FN_SLP_DEC parameters are
        // set to zero no BEMF compensation is performed.
        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_FN_SLP_ACC};
        byte[] dataByte  = {(byte)p_lFinalSlope};
        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);
    }

    private static void setFinalSlopeDec(long p_lFinalSlope)
    {
        // When ST_SLP, FN_SLP_ACC and FN_SLP_DEC parameters are
        // set to zero no BEMF compensation is performed.
        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_FN_SLP_DEC};
        byte[] dataByte  = {(byte)p_lFinalSlope};
        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);
    }


    private static void setThermalWindingCompensation(long p_lCoefficient)
    {
        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_K_THERM};
        byte[] dataByte  = {(byte)p_lCoefficient};
        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);
    }

    private static void setOverCurrentThreshold(long p_lCurrentMilliAmp)
    {
        // set the OCD_TH register to the value corresponding to the nearest value of the
        // 4-bit raster. The available range is from 375 mA to 6 A, steps of 375 mA
        // i.e.  375 -->  0
        //       750 -->  1
        //      6000 --> 15
        long lCurrentStep = 375;

        //logOCD_Thresh(p_lCurrentMilliAmp);

        // use integer-truncation for rounding by adding half step value
        long lValue = (p_lCurrentMilliAmp + (lCurrentStep/2)) / lCurrentStep;

        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_OCD_TH};
        byte[] dataByte  = {(byte)lValue};
        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);
    }

    private static void setStallCurrentThreshold(long p_lCurrentMilliAmp)
    {
        // set the STALL_TH register to the value corresponding to the nearest value of the
        // 7-bit raster. The available range is from 31.25 mA to 4 A with a resolution of 31.25 mA.
        // i.e.  31.25 -->   0
        //       62.50 -->   1
        //     4000.00 --> 127
        double dbCurrentStep = 31.25;

        //logStall_Thresh(p_lCurrentMilliAmp);

        long lValue = (int) Math.round((double)p_lCurrentMilliAmp / dbCurrentStep);

        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_STALL_TH};
        byte[] dataByte  = {(byte)lValue};
        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);
    }



    private static void setAlarmCondition(byte p_byAlarmFlagSource)
    {
        //logAlarmMode(p_byAlarmFlagSource);

        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_ALARM_EN};
        byte[] dataByte  = {p_byAlarmFlagSource};
        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);
    }


    private static void softResetDevice()
    {
        // send a reset command byte to the only SPI slave
        byte[] commandByte  = {L6470_CMD_RESET_DEVICE};
        m_StepperBoard.write(commandByte);
        SleepUtil.sleepMillis(500);  // how long is necessary?
    }



    // ======================================================================
    //  Step Mode handling
    // ======================================================================
    private static void setStepMode(long p_lStepMode)
    {
        logStepMode(p_lStepMode);

        // m_lMicrostepping = 1  Full  step
        //                    2  Half  step
        //                    4  1/4   microstep
        //                    8  1/8   microstep
        //                   16  1/16  microstep
        //                   32  1/32  microstep
        //                   64  1/64  microstep
        //                  128  1/128 microstep
        m_lMicrostepping = 1 << p_lStepMode;

        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_STEP_MODE};
        byte[] dataByte  = {(byte) p_lStepMode};

        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);
    }

    private static long getStepMode()
    {
        byte[] commandByte = {L6470_CMD_GET_PARAM + L6470_PARAM_STEP_MODE};
        byte[] oneByteRead = {L6470_CMD_NOP};

        m_StepperBoard.write(commandByte);
        long lDataReceived = m_StepperBoard.writeAndRead(oneByteRead)[0];

        logStepMode(lDataReceived);

        return lDataReceived;
    }



    private static void logStepMode(long p_lStepMode)
    {
        // -----------------------------------------------------
        // |    7    |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
        // -----------------------------------------------------
        // | SYNC_EN |     SYNC_SEL    |  0  |    STEP_SEL     |
        // -----------------------------------------------------

        // when SYNC_EN bit is set low BUSY/SYNC output is forced low during commands execution
        // when SYNC_EN bit is set high, BUSY/SYNC output provides a clock signal according to
        // SYNC_SEL parameter.

        // The SYNC_SEL parameter selects the divider of the step frequency. this depends on
        // the selected STEP_SEL mode.

        // The STEP_SEL parameter selects one of eight possible stepping modes:
        // 0 Full  step
        // 1 Half  step
        // 2 1/4   microstep
        // 3 1/8   microstep
        // 4 1/16  microstep
        // 5 1/32  microstep
        // 6 1/64  microstep
        // 7 1/128 microstep
        if (m_bLoggingNeeded)
        {
            long lSyncEnabled = ((p_lStepMode & 0b10000000) >> 7);
            long lSyncSelect  = ((p_lStepMode & 0b01110000) >> 4);
            long lStepSelect  = ((p_lStepMode & 0b00000111) >> 0);

            LOGGER.info("StepMode({}) SYNC_EN:{} SYNC_SEL:{} STEP_SEL:{}", p_lStepMode, lSyncEnabled, lSyncSelect, lStepSelect);
        }
    }

    private static void testSetFullStepMode()
    {
        // write 0 into the STEP_MODE register:
        // - no syn mode
        // - no sync select
        // - step mode = full
        setStepMode(0);
    }



    // ======================================================================
    //  KVal Hold handling
    // ======================================================================
    private static void setKVal_Hold(long p_lNewKValue)
    {
        logKVal_Hold(p_lNewKValue);

        byte[] commandByte = {L6470_CMD_SET_PARAM + L6470_PARAM_KVAL_HOLD};
        byte[] dataByte  = {(byte) p_lNewKValue};

        m_StepperBoard.write(commandByte);
        m_StepperBoard.write(dataByte);
    }

    private static long getKVal_Hold()
    {
        byte[] commandByte = {L6470_CMD_GET_PARAM + L6470_PARAM_KVAL_HOLD};
        byte[] oneByteSent = {L6470_CMD_NOP};

        m_StepperBoard.write(commandByte);
        byte[] oneByteRead = m_StepperBoard.writeAndRead(oneByteSent);

        long lDataReceived = ((long) oneByteRead[0]) & 0x000000FF; // keep the lowest 8 bits without sign

        if (lDataReceived < 0)
        {

            //lDataReceived = 256 + lDataReceived;

        }
        logKVal_Hold(lDataReceived);

        return lDataReceived;
    }

    private static void logKVal_Hold(long p_lValue)
    {
        if (m_bLoggingNeeded)
        {
            LOGGER.info("KVal_Hold: {} {}", String.format("0x%02X",p_lValue), p_lValue);
        }
    }

    private static void testGetSetKVal_Hold()
    {
        // prepare test condition
        boolean bOldLoggingSetting = m_bLoggingNeeded;
        m_bLoggingNeeded = false;
        long lOldKval_Hold = getKVal_Hold();
        boolean bSuccess = true;

        long lKVal = 0;
        for (lKVal = 0; lKVal < 256; lKVal++)
        {
            setKVal_Hold(lKVal);
            if (getKVal_Hold() != lKVal)
            {
                bSuccess = false;
                break;
            }
        }

        // restore condition
        setKVal_Hold(lOldKval_Hold);
        m_bLoggingNeeded = bOldLoggingSetting;
        if (!bSuccess)
        {
            LOGGER.error("testGetSetKVal_Hold\t==> failed at value: {}", lKVal );
            checkStatus();
        }
        else
        {
            LOGGER.info("testGetSetKVal_Hold\t==> passed");
        }
    }

    // ======================================================================
    //  AbsPos handling
    // ======================================================================

    public static void setAbsPos(long p_lNewPosition)
    {
        logAbsPos(p_lNewPosition);

        byte[] commandBytes  = new byte[4];
        commandBytes[0]  = L6470_CMD_SET_PARAM + L6470_PARAM_ABS_POS;
        commandBytes[1]  = (byte) ((p_lNewPosition & 0b001111110000000000000000) >> 16);
        commandBytes[2]  = (byte) ((p_lNewPosition & 0b000000001111111100000000) >>  8);
        commandBytes[3]  = (byte) ((p_lNewPosition & 0b000000000000000011111111) >>  0);

        m_StepperBoard.write(new byte[]{commandBytes[0]});
        m_StepperBoard.write(new byte[]{commandBytes[1]});
        m_StepperBoard.write(new byte[]{commandBytes[2]});
        m_StepperBoard.write(new byte[]{commandBytes[3]});
    }

    private static long getAbsPos()
    {
        byte[] commandByte    = {L6470_CMD_GET_PARAM + L6470_PARAM_ABS_POS};
        byte[] oneByteRead    = {L6470_CMD_NOP};
        byte[] registerReturn = new byte[3];

        m_StepperBoard.write(commandByte);
        registerReturn[0] = m_StepperBoard.writeAndRead(oneByteRead)[0];
        registerReturn[1] = m_StepperBoard.writeAndRead(oneByteRead)[0];
        registerReturn[2] = m_StepperBoard.writeAndRead(oneByteRead)[0];

        // convert incoming bytes to int/long, considering the sign
        boolean bIsNegative = (registerReturn[0] & 0b00100000) > 0;
        int iByte1 = (registerReturn[0] * 0x010000) & 0x00FF0000;
        int iByte2 = (registerReturn[1] * 0x000100) & 0x0000FF00;
        int iByte3 = (registerReturn[2] * 0x000001) & 0x000000FF;
        int iDataReceived = iByte1 + iByte2 + iByte3;
        if (bIsNegative)
        {
            // fill the remaining bits with 1's
            iDataReceived = iDataReceived | 0xFFC00000;
        }
        logAbsPos((long)iDataReceived);

        return (long)iDataReceived;
    }

    private static void logAbsPos(long p_lPosition)
    {
        if (m_bLoggingNeeded)
        {
            long lByte1 = ((p_lPosition & 0b001111110000000000000000) >> 16);
            long lByte2 = ((p_lPosition & 0b000000001111111100000000) >>  8);
            long lByte3 = ((p_lPosition & 0b000000000000000011111111) >>  0);
            String sPosition = String.format("0x%02X 0x%02X 0x%02X", lByte1, lByte2, lByte3);
            LOGGER.info("AbsPos {} {}", sPosition, p_lPosition);
        }
    }

    private static void testGetSetAbsPos()
    {
        // prepare test condition
        boolean bOldLoggingSetting = m_bLoggingNeeded;
        m_bLoggingNeeded = false;
        long lOldPos = getAbsPos();
        boolean bSuccess = true;

        int[] lTestPositions = {1000, -1000, 0xFFFFFFFF, 0xAAAAAAAA, 0x55555555, 0x00000000};
        long lPos = 0;
        for (int iIndex = 0; iIndex < lTestPositions.length; iIndex++)
        {
            lPos = lTestPositions[iIndex];
            setAbsPos(lPos);
            long lResult = getAbsPos();

            // shorten the integer values to 22 bits in order to compare comparable values
            long lPosShort    = lPos    & 0x003FFFFF;
            long lResultShort = lResult & 0x003FFFFF;
            if (lResultShort != lPosShort)
            {
                bSuccess = false;
                break;
            }
        }

        // restore condition
        setAbsPos(lOldPos);
        m_bLoggingNeeded = bOldLoggingSetting;
        if (!bSuccess)
        {
            LOGGER.error("testGetSetAbsPos     \t==> failed at value: {}", String.format("0x%04X",lPos) );
            checkStatus();
        }
        else
        {
            LOGGER.info("testGetSetAbsPos     \t==> passed");
        }

    }


    private static long getSpeed()
    {
        // The SPEED register contains the current motor speed, expressed in step/tick
        // (format unsigned fixed point 0.28). In order to convert the SPEED value in step/s
        // the following formula can be used:

        // [step/s] = SPEED * 2^-28 / tick

        // where SPEED is the integer number stored into the register and tick is 250 ns.

        // The available range is from 0 to 15625 step/s with a resolution of 0.015 step/s.
        // Note: The range effectively available to the user is limited by the MAX_SPEED parameter.

        return 0; // TODO
    }

    private static long getMinSpeed()
    {
        // -------------------------------
        // |    12    |       11...0     |
        // -------------------------------
        // | LSDP_OPT |     MIN_SPEED    |
        // -------------------------------

        // The MIN_SPEED parameter contains the speed profile minimum speed. Its value is expressed
        // in step/tick and to convert it in step/s the following formula can be used:

        // [step/s] = MIN_SPEED * 2^-24 / tick

        // where MIN_SPEED is the integer number stored into the register and tick is the ramp 250 ns.
        // The available range is from 0 to 976.3 step/s with a resolution of 0.238 step/s.

        // When LSPD_OPT bit is set high, low speed optimization feature is enabled and MIN_SPEED value
        // indicates the speed threshold below which the compensation works. In this case the minimum
        // speed of speed profile is set to zero.

        return 0; // TODO
    }

    private static long getMaxSpeed()
    {
        // The MAX_SPEED register contains the speed profile maximum speed expressed in
        // step/tick (format unsigned fixed point 0.18).
        // In order to convert it in step/s the following formula can be used:

        // [step/s] = MAX_SPEED * 2^-18 / tick

        // where MAX_SPEED is the integer number stored into the register and tick is 250 ns.
        // The available range is from 15.25 to 15610 step/s with a resolution of 15.25 step/s.

        return 0; // TODO
    }


    private static void testSoftStop()
    {
        byte[] commandBytes  = new byte[1];

        commandBytes[0]  = L6470_CMD_SOFT_STOP;
        m_StepperBoard.write(new byte[]{commandBytes[0]});

    }

    private static void testRun(long p_lSpeedStepPerSec)
    {
        //long lSpeedStepPerSec = 600;
        long lDirection = 0;

        // When issuing RUN command, the 20-bit speed is [(steps/s)*(tick)]/(2^-28)
        // where tick is 250ns (datasheet value).
        // Multiply desired steps/s by 67.106 to get an appropriate value for this register
        // This is a 20-bit value, so we need to make sure the value is at or below 0xFFFFF.
        long lSpeedStepsPerTick = (long) ((double) p_lSpeedStepPerSec * 67.106);
        if (lSpeedStepsPerTick > 0x000FFFFF)
        {
            lSpeedStepsPerTick = 0x000FFFFF;
        }

        byte[] commandBytes  = new byte[4];

        commandBytes[0]  = L6470_CMD_RUN + 0; // direction 0 is reverse

        commandBytes[1]  = (byte) ((lSpeedStepsPerTick & 0b000011110000000000000000) >> 16);
        commandBytes[2]  = (byte) ((lSpeedStepsPerTick & 0b000000001111111100000000) >> 8);
        commandBytes[3]  = (byte)  (lSpeedStepsPerTick & 0b000000000000000011111111);


        m_StepperBoard.write(new byte[]{commandBytes[0]});
        m_StepperBoard.write(new byte[]{commandBytes[1]});
        m_StepperBoard.write(new byte[]{commandBytes[2]});
        m_StepperBoard.write(new byte[]{commandBytes[3]});

        String sCommand = String.format("0x%02X 0x%02X 0x%02X 0x%02X",commandBytes[0], commandBytes[1], commandBytes[2], commandBytes[3]);

        LOGGER.info("--> SpiDevice testRun({} {}) issued as {} {}", lDirection, lSpeedStepsPerTick, String.format("0x%08X", lSpeedStepsPerTick), sCommand);
    }

    private static void goUntil(long p_lSpeedStepPerSec, long p_lDirection)
    {
        long lSpeedStepsPerTick = calcRunSpeed(p_lSpeedStepPerSec);

        byte[] commandBytes  = new byte[4];
        commandBytes[0]  = (byte) (L6470_CMD_GO_UNTIL + p_lDirection);
        commandBytes[1]  = (byte) ((lSpeedStepsPerTick & 0b000011110000000000000000) >> 16);
        commandBytes[2]  = (byte) ((lSpeedStepsPerTick & 0b000000001111111100000000) >> 8);
        commandBytes[3]  = (byte)  (lSpeedStepsPerTick & 0b000000000000000011111111);

        m_StepperBoard.write(new byte[]{commandBytes[0]});
        m_StepperBoard.write(new byte[]{commandBytes[1]});
        m_StepperBoard.write(new byte[]{commandBytes[2]});
        m_StepperBoard.write(new byte[]{commandBytes[3]});
    }


    private static void run(long p_lSpeedStepPerSec, long p_lDirection)
    {
        long lSpeedStepsPerTick = calcRunSpeed(p_lSpeedStepPerSec);

        byte[] commandBytes  = new byte[4];
        commandBytes[0]  = (byte) (L6470_CMD_RUN + p_lDirection);
        commandBytes[1]  = (byte) ((lSpeedStepsPerTick & 0b000011110000000000000000) >> 16);
        commandBytes[2]  = (byte) ((lSpeedStepsPerTick & 0b000000001111111100000000) >> 8);
        commandBytes[3]  = (byte)  (lSpeedStepsPerTick & 0b000000000000000011111111);

        String sCommand = String.format("0x%02X 0x%02X 0x%02X 0x%02X",commandBytes[0], commandBytes[1], commandBytes[2], commandBytes[3]);
        LOGGER.info("--> SpiDevice run({} {}) issued as {} {}", p_lDirection, lSpeedStepsPerTick, String.format("0x%08X", lSpeedStepsPerTick), sCommand);

        m_StepperBoard.write(new byte[]{commandBytes[0]});
        m_StepperBoard.write(new byte[]{commandBytes[1]});
        m_StepperBoard.write(new byte[]{commandBytes[2]});
        m_StepperBoard.write(new byte[]{commandBytes[3]});
    }

    private static long calcRunSpeed(long p_lSpeedStepPerSec)
    {
        // When issuing RUN command, the 20-bit speed is [(steps/s)*(tick)]/(2^-28)
        // where tick is 250ns (datasheet value).
        // Multiply desired steps/s by 67.106 to get an appropriate value for this register
        // This is a 20-bit value, so we need to make sure the value is at or below 0xFFFFF.
        long lStepsPerSecond = p_lSpeedStepPerSec;
        long lStepsPerTick   = (long)((double)lStepsPerSecond * m_dbTick / Math.pow(2, -28));
        // can be reduced to (double)lStepsPerSecond * 67.106

        // parameter correction:
        // run speed is positive and not larger than 0x000FFFFF
        if (lStepsPerTick > 0x000FFFFF)
        {
            lStepsPerTick = 0x000FFFFF;
        }
        else if (lStepsPerTick < 0)
        {
            lStepsPerTick = 0;
        }

        return lStepsPerTick;
    }


    private static void testSetMaxSpeed()
    {
        byte[] commandBytes  = new byte[4];
        commandBytes[0]  = L6470_CMD_SET_PARAM;
    }


    private static void testGoTo(int p_iPosition)
    {
        byte[] commandBytes  = new byte[4];
        commandBytes[0]  = L6470_CMD_GOTO;
        commandBytes[1]  = (byte) ((p_iPosition & 0b001111110000000000000000) >> 16);
        commandBytes[2]  = (byte) ((p_iPosition & 0b000000001111111100000000) >> 8);
        commandBytes[3]  = (byte)  (p_iPosition & 0b000000000000000011111111);
        //m_StepperBoard.write(commandBytes);

        m_StepperBoard.write(new byte[]{commandBytes[0]});
        m_StepperBoard.write(new byte[]{commandBytes[1]});
        m_StepperBoard.write(new byte[]{commandBytes[2]});
        m_StepperBoard.write(new byte[]{commandBytes[3]});

        String sCommand = String.format("0x%02X 0x%02X 0x%02X 0x%02X",commandBytes[0], commandBytes[1], commandBytes[2], commandBytes[3]);

        LOGGER.info("--> SpiDevice testGoTo({} {}) issued as {} ", p_iPosition, String.format("0x%08X", p_iPosition), sCommand);

    }



    public static void testResetPos()
    {
        // send a reset command byte to the only SPI slave
        byte[] commandByte  = {L6470_CMD_RESET_POS};
        byte[] oneByteRead  = {L6470_CMD_NOP};
        byte[] resetReturn = new byte[5];

        m_StepperBoard.write(commandByte);
        resetReturn[0] = m_StepperBoard.writeAndRead(oneByteRead)[0];
        resetReturn[1] = m_StepperBoard.writeAndRead(oneByteRead)[0];
        resetReturn[2] = m_StepperBoard.writeAndRead(oneByteRead)[0];
        resetReturn[3] = m_StepperBoard.writeAndRead(oneByteRead)[0];
        resetReturn[4] = m_StepperBoard.writeAndRead(oneByteRead)[0];

        SleepUtil.sleepMillis(800);

        String sCommand = String.format("0x%X",commandByte[0]);

        int iBytesReceived = resetReturn.length;
        String sResult = "";
        for (int iByte = 0; iByte < iBytesReceived; iByte++)
        {
            sResult = sResult + String.format(" 0x%X",resetReturn[iByte]);
        }

        LOGGER.info("SpiDevice testResetPos ({}) returned ({} bytes):{}",sCommand ,iBytesReceived, sResult);
    }

    public static void checkStatus()
    {
        long lCurrentStatus = getStatusRegister();
        long lStateDifference = lCurrentStatus^m_lLastStatus;

        if (lStateDifference != 0)
        {
            // log the differences of the state
            LOGGER.info("Status: -------------------------");
            if ((lStateDifference & 0b0000000000000001) > 0)
            {
                long lHiZ = lCurrentStatus & 0b0000000000000001;
                LOGGER.info("High-Z:     \t{}", lHiZ);
            }

            if ((lStateDifference & 0b0000000000000010) > 0)
            {
                long lBusy = (lCurrentStatus & 0b0000000000000010) >> 1;
                LOGGER.info("Busy:      \t{}", lBusy);
            }

            if ((lStateDifference & 0b0000000000000100) > 0)
            {
                long lSwitch = (lCurrentStatus & 0b0000000000000100) >> 2;
                LOGGER.info("SwitchState: \t{}", lSwitch);
            }

            if ((lStateDifference & 0b0000000000001000) > 0)
            {
                long lSwitch = (lCurrentStatus & 0b0000000000001000) >> 3;
                LOGGER.info("SwitchEvent: \t{}", lSwitch);
            }

            if ((lStateDifference & 0b0000000000010000) > 0)
            {
                long lDir = (lCurrentStatus & 0b0000000000010000) >> 4;
                LOGGER.info("Direction: \t{}", lDir);
            }

            if ((lStateDifference & 0b0000000001100000) > 0)
            {
                long lMotorState = (lCurrentStatus & 0b0000000001100000) >> 5;
                LOGGER.info("MotorState: \t{}", lMotorState);
            }

            if ((lStateDifference & 0b0000000010000000) > 0)
            {
                long lNotPerform = (lCurrentStatus & 0b0000000010000000) >> 7;
                LOGGER.info("NotPerform: \t{}", lNotPerform);
            }

            if ((lStateDifference & 0b0000000100000000) > 0)
            {
                long lWrongCmd = (lCurrentStatus & 0b0000000100000000) >> 8;
                LOGGER.info("WrongCmd: \t{}", lWrongCmd);
            }

            if ((lStateDifference & 0b0000001000000000) > 0)
            {
                long lUvLock = (lCurrentStatus & 0b0000001000000000) >> 9;
                LOGGER.info("UnderVoltage: \t{}", lUvLock);
            }

            if ((lStateDifference & 0b0000010000000000) > 0)
            {
                long lWarnLevel = (lCurrentStatus & 0b0000010000000000) >> 10;
                LOGGER.info("WarnLevel: \t{}", lWarnLevel);
            }

            if ((lStateDifference & 0b0000100000000000) > 0)
            {
                long lStopLevel = (lCurrentStatus & 0b0000100000000000) >> 11;
                LOGGER.info("StopLevel: \t{}", lStopLevel);
            }

            if ((lStateDifference & 0b0001000000000000) > 0)
            {
                long lOverCurrent = (lCurrentStatus & 0b0001000000000000) >> 12;
                LOGGER.info("OverCurrent: \t{}", lOverCurrent);
            }

            if ((lStateDifference & 0b0010000000000000) > 0)
            {
                long lStepLoss = (lCurrentStatus & 0b0010000000000000) >> 13;
                LOGGER.info("StepLoss A: \t{}", lStepLoss);
            }

            if ((lStateDifference & 0b0100000000000000) > 0)
            {
                long lStepLoss = (lCurrentStatus & 0b0100000000000000) >> 14;
                LOGGER.info("StepLoss B: \t{}", lStepLoss);
            }

            if ((lStateDifference & 0b1000000000000000) > 0)
            {
                long lStepClockMode = (lCurrentStatus & 0b1000000000000000) >> 15;
                LOGGER.info("StepClockMode: \t{}", lStepClockMode);
            }

        }

        // keep the state for the next check
        m_lLastStatus = lCurrentStatus;
    }

    public static void testGetStatus()
    {
        // GetStatus
        // Command Structure:
        // from Host:   1 byte  0xD0
        // to host:     2 bytes MSB:LSB
        // The GetStatus command returns the Status register value.
        // GetStatus command resets the STATUS register warning flags. The command forces the
        // system to exit from any error state. The GetStatus command DO NOT reset HiZ flag.

        // Returns the current contents of the STATUS register on the L6470 chip.
        // This is a good communications sanity check because on boot, the value will be 0x2E88.
        byte[] commandByte  = {L6470_CMD_GETSTATUS};
        byte[] oneByteRead  = {L6470_CMD_NOP};
        byte[] statusReturn = new byte[2];

        m_StepperBoard.write(commandByte);
        statusReturn[0] = m_StepperBoard.writeAndRead(oneByteRead)[0];
        statusReturn[1] = m_StepperBoard.writeAndRead(oneByteRead)[0];

        String sCommand = String.format("0x%02X",commandByte[0]);

        int iBytesReceived = statusReturn.length;
        String sResult = "";
        for (int iByte = 0; iByte < iBytesReceived; iByte++)
        {
            sResult = sResult + String.format(" 0x%02X",statusReturn[iByte]);
        }

        LOGGER.info("SpiDevice getStatus ({}) returned Status ({} bytes):{}",sCommand ,iBytesReceived, sResult);

    }

    public static long getStatusRegister()
    {
        byte[] commandByte    = {L6470_CMD_GET_PARAM + L6470_PARAM_STATUS};
        byte[] oneByteRead    = {L6470_CMD_NOP};
        byte[] statusRegister = new byte[2];

        m_StepperBoard.write(commandByte);
        statusRegister[0] = m_StepperBoard.writeAndRead(oneByteRead)[0];
        statusRegister[1] = m_StepperBoard.writeAndRead(oneByteRead)[0];

        int iStatusRegister = ((statusRegister[0] << 8) & 0x0000FF00) + (statusRegister[1] & 0x000000FF);

        m_lLastStatus = iStatusRegister;

        return iStatusRegister;
    }

    public static void testGetStatusRegister()
    {
        String sCommand = String.format("0x%02X",(L6470_CMD_GET_PARAM + L6470_PARAM_STATUS));

        long lStatusRegister = getStatusRegister();

        String sResult = String.format(" 0x%04X",lStatusRegister);

        LOGGER.info("SpiDevice getStatusRegister ({}) returned Status ({} bytes):{}",sCommand , 2, sResult);
    }


    public static void testGetConfigRegister()
    {
        // h18 CONFIG IC configuration
        // 2 bytes, reset at 2E88 (2E08)
        // Internal oscillator,
        // 2MHz OSCOUT clock,
        // supply voltage compensation disabled,
        // overcurrent shutdown enabled,          --> 2E88 vs 2E08
        // slew-rate = 290 V/µs
        // PWM frequency = 15.6kH
        byte[] commandByte  = {L6470_CMD_GET_PARAM + L6470_PARAM_CONFIG};
        byte[] oneByteRead  = {L6470_CMD_NOP};
        byte[] configReturn = new byte[2];

        m_StepperBoard.write(commandByte);
        configReturn[0] = m_StepperBoard.writeAndRead(oneByteRead)[0];
        configReturn[1] = m_StepperBoard.writeAndRead(oneByteRead)[0];

        String sCommand = String.format("0x%02X",commandByte[0]);

        int iBytesReceived = configReturn.length;
        String sResult = "";
        for (int iByte = 0; iByte < iBytesReceived; iByte++)
        {
            sResult = sResult + String.format(" 0x%02X",configReturn[iByte]);
        }

        LOGGER.info("SpiDevice testGetConfigRegister ({}) returned Status ({} bytes):{}",sCommand ,iBytesReceived, sResult);

    }



}
