package Experimental;

public enum EconversionMode
{
    CHANNEL_A_128,
    CHANNEL_A_64,
    CHANNEL_B,
    CHANNEL_A_128_AND_B,
    CHANNEL_A_64_AND_B
}
