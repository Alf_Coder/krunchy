package Experimental;

import com.diozero.api.DigitalOutputDevice;
import com.diozero.api.GpioEventTrigger;
import com.diozero.api.GpioPullUpDown;
import com.diozero.api.WaitableDigitalInputDevice;
import com.diozero.devices.Button;
import com.diozero.devices.LED;
import com.diozero.util.RuntimeIOException;
import com.diozero.util.SleepUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//===============================================================================
//  Hardware Experiments
//===============================================================================
public class FirstExperiments
{
    private static final Logger LOGGER = LoggerFactory.getLogger(FirstExperiments.class);

    // GPIOxx RaspberryPi naming (e.g. GPIO23 -> header pin 16 / GPIO_GEN4)
    private static final int BUTTON_NR_STOP_APP  = 26;

    private static final int BUTTON_NR_DOWN_SLOW =  5;
    private static final int BUTTON_NR_DOWN_FAST =  6;
    private static final int BUTTON_NR_UP_SLOW   = 13;
    private static final int BUTTON_NR_UP_FAST   =  4;

    private static final int LED_NR_DOWN_SLOW = 18;
    private static final int LED_NR_DOWN_FAST = 23;
    private static final int LED_NR_UP_SLOW   = 24;
    //private static final int LED_NR_UP_FAST   = 25;

    private static final int A4988_STEP_PIN   = 27;
    private static final int A4988_DIR_PIN    = 17;
    private static final int A4988_ENABLE_PIN = 22;

    private static final int HX711_DATA_OUT_PIN   = 20;
    private static final int HX711_CLOCK_IN_PIN   = 21;

    private static final DigitalOutputDevice m_pinEnable = new DigitalOutputDevice(A4988_ENABLE_PIN, false, false);
    private static final DigitalOutputDevice m_pinStep   = new DigitalOutputDevice(A4988_STEP_PIN,   true,  false);
    private static final DigitalOutputDevice m_pinDir    = new DigitalOutputDevice(A4988_DIR_PIN,    true,  false);

    private static final Button m_butStopApp  = new Button(BUTTON_NR_STOP_APP, GpioPullUpDown.PULL_DOWN);

    private static final Button m_butUpSlow   = new Button(BUTTON_NR_UP_SLOW, GpioPullUpDown.PULL_DOWN);
    private static final Button m_butUpFast   = new Button(BUTTON_NR_UP_FAST, GpioPullUpDown.PULL_DOWN);
    private static final Button m_butDownSlow = new Button(BUTTON_NR_DOWN_SLOW, GpioPullUpDown.PULL_DOWN);
    private static final Button m_butDownFast = new Button(BUTTON_NR_DOWN_FAST, GpioPullUpDown.PULL_DOWN);

    private static final LED    m_ledUpSlow   = new LED(LED_NR_UP_SLOW);
   // private static final LED    m_ledUpFast   = new LED(LED_NR_UP_FAST);
    private static final LED    m_ledDownSlow = new LED(LED_NR_DOWN_SLOW);
    private static final LED    m_ledDownFast = new LED(LED_NR_DOWN_FAST);

    //===============================================================================
    //  MAIN
    //===============================================================================
    public static void main(String[] args) {
        LOGGER.info("FirstExperiments main invoked");

        String sJavaLibPath = System.getProperty("java.library.path");
        System.out.println(sJavaLibPath);

 //       pulseDuration2();
//        pulseDuration();
        helloLED();
//        speedTest();
//        frequencyTest();
//        frequencySelector();
//        stepperTest();
//        rampTest();
//        stepperMoveByButtonClick();
//       testHX711();
//        LedChainTest();

        LOGGER.info("FirstExperiments main close hardware");
        m_ledUpSlow.close();
        //m_ledUpFast.close();
        m_ledDownFast.close();
        m_ledDownSlow.close();
        m_pinEnable.close();
        m_pinStep.close();
        m_pinDir.close();
        m_butStopApp.close();
        m_butUpSlow.close();
        m_butUpFast.close();
        m_butDownSlow.close();
        m_butDownFast.close();
        LOGGER.info("FirstExperiments main exit");

    }




    //--------------------------------------------------------------------------------
    //  helloLED
    //--------------------------------------------------------------------------------
    public static void helloLED()
    {
        final int BUTTON_NR_TRIGGER   = 19;
        boolean bStop = m_butStopApp.isPressed();

        try
        {
            WaitableDigitalInputDevice triggerPin = new WaitableDigitalInputDevice(BUTTON_NR_TRIGGER, GpioPullUpDown.PULL_DOWN, GpioEventTrigger.BOTH);

            LOGGER.info("Waiting for 2000ms for button release");
            boolean notified = triggerPin.waitForValue(false, 2000);
            LOGGER.info("Timed out? " + !notified);

            LOGGER.info("Waiting for 2000ms for button press");
            notified = triggerPin.waitForValue(true, 2000);
            LOGGER.info("Timed out? " + !notified);

            LOGGER.info("Waiting for 2000ms for button release");
            notified = triggerPin.waitForValue(false, 2000);
            LOGGER.info("Timed out? " + !notified);

            triggerPin.close();
        }
        catch (RuntimeIOException ioe)
        {
            LOGGER.error("{}", ioe);
        }
        catch (InterruptedException ex)
        {
            LOGGER.error("{}", ex);
        }


        try
        {
            Button triggerButton = new Button(BUTTON_NR_TRIGGER, GpioPullUpDown.PULL_DOWN);

            triggerButton.addListener(event -> LOGGER.debug("valueChanged({})", event));

            LOGGER.debug("Waiting for 10s - *** Press the button connected to input pin " + BUTTON_NR_TRIGGER + " ***");

            SleepUtil.sleepSeconds(10);

            LOGGER.debug("End of Waiting ***");
            triggerButton.close();
        }
        catch (RuntimeIOException ioe)
        {
            LOGGER.error("{}", ioe);
        }


        /*


        DigitalOutputDevice m_pinEnable = new DigitalOutputDevice(A4988_ENABLE_PIN, false, false);
        InputEventListener
                WaitableDigitalInputDevice

        try (WaitableDigitalInputDevice input = new WaitableDigitalInputDevice(inputPin, GpioPullUpDown.PULL_UP, GpioEventTrigger.BOTH)) {
            while (true) {
                Logger.info("Waiting for 2000ms for button press");
                boolean notified = input.waitForValue(false, 2000);
                Logger.info("Timed out? " + !notified);
                Logger.info("Waiting for 2000ms for button release");
                notified = input.waitForValue(true, 2000);
                Logger.info("Timed out? " + !notified);
            }
        } catch (RuntimeIOException ioe) {
            Logger.error(ioe, "Error: {}", ioe);
        } catch (InterruptedException e) {
            Logger.error(e, "Error: {}", e);
        }
    */

        while (!bStop)
        {
            boolean bUpSlowPressed   = m_butUpSlow.isPressed();
            boolean bUpFastPressed   = m_butUpFast.isPressed();
            boolean bDownSlowPressed = m_butDownSlow.isPressed();
            boolean bDownFastPressed = m_butDownFast.isPressed();

            m_ledUpSlow.setOn(bUpSlowPressed);
            //m_ledUpFast.setOn(bUpFastPressed);
            m_ledDownSlow.setOn(bDownSlowPressed);
            m_ledDownFast.setOn(bDownFastPressed);

            SleepUtil.sleepMillis(100);

            bStop = m_butStopApp.isPressed();
        }
    }









    //--------------------------------------------------------------------------------
    //  pulseDuration
    //--------------------------------------------------------------------------------
    public static void pulseDuration()
    {
        boolean bStop = m_butStopApp.isPressed();

        while (!bStop)
        {
            boolean bUpSlowPressed   = m_butUpSlow.isPressed();
            boolean bUpFastPressed   = m_butUpFast.isPressed();
            boolean bDownSlowPressed = m_butDownSlow.isPressed();
            boolean bDownFastPressed = m_butDownFast.isPressed();

            if (bUpFastPressed)
            {
//                m_ledUpFast.setOn(true);
//                m_ledUpFast.setOn(false);
//                m_ledUpFast.on();
//                m_ledUpFast.off();
            }

            if (bUpSlowPressed)
            {
 //               m_ledUpFast.setOn(false);
                m_ledUpSlow.on();
                m_ledUpSlow.off();
            }

            if (bDownSlowPressed)
            {
                m_ledDownSlow.on();
                bStop = m_butStopApp.isPressed();
                m_ledDownSlow.off();
            }

            if (bDownFastPressed)
            {
                m_ledDownFast.on();
                m_ledDownFast.on();
                bStop = m_butStopApp.isPressed();
                m_ledDownFast.off();
            }

            SleepUtil.sleepMicros(1);

            bStop = m_butStopApp.isPressed();
        }
    }


    //--------------------------------------------------------------------------------
    //  pulseDuration
    //--------------------------------------------------------------------------------
    public static void pulseDuration2()
    {
        boolean bStop = m_butStopApp.isPressed();

        while (!bStop)
        {
            boolean bUpSlowPressed   = m_butUpSlow.isPressed();
//            boolean bDownSlowPressed = m_butDownSlow.isPressed();
//            boolean bDownFastPressed = m_butDownFast.isPressed();

            if (bUpSlowPressed)
            {
                m_ledUpSlow.setOn(true);
                m_ledUpSlow.setOn(false);
                m_ledUpSlow.on();
                m_ledUpSlow.off();
             //   SleepUtil.sleepMicros(1);
            }

            while(m_butUpFast.isPressed())
            {
//                m_ledUpFast.setOn(true);
//                m_ledUpFast.setOn(false);
//                m_ledDownFast.on();
//                m_ledDownFast.off();
            }

            bStop = m_butStopApp.isPressed();
        }
    }



    //--------------------------------------------------------------------------------
    //  speedTest
    //--------------------------------------------------------------------------------
    private static void speedTest()
    {
        int iClockBurst = 10000;

        boolean bStop = m_butStopApp.isPressed();

        while (!bStop)
        {
            int iClocks = iClockBurst;

            while(iClocks > 0)
            {
//                m_ledUpFast.setOn(true);
//                m_ledUpFast.setOn(false);
//                m_ledDownFast.on();
//                m_ledDownFast.off();
                iClocks--;
            }

            bStop = m_butStopApp.isPressed();
        }
    }

    //--------------------------------------------------------------------------------
    //  frequencyTest
    //--------------------------------------------------------------------------------
    private static void frequencyTest()
    {
        int iFrequencyHz = 100;

        boolean bStop = m_butStopApp.isPressed();

        while (!bStop)
        {
            m_ledDownFast.on();
            SleepUtil.sleepMicros(100);
            m_ledDownFast.off();
            SleepUtil.sleepMicros(100);

            bStop = m_butStopApp.isPressed();
        }
    }

    //--------------------------------------------------------------------------------
    //  frequencySelector
    //--------------------------------------------------------------------------------
    private static void frequencySelector()
    {
        int iFrequencyHz = 100;

        boolean bStop = m_butStopApp.isPressed();

        while (!bStop)
        {
            iFrequencyHz = 100;
            if (m_butUpSlow.isPressed())
            {
                iFrequencyHz = 200;
            }
            if (m_butUpFast.isPressed())
            {
                iFrequencyHz = 500;
            }
            if (m_butDownSlow.isPressed())
            {
                iFrequencyHz = 10000;
            }

            int iPeriodMicro = 1000000/iFrequencyHz;
            int iPulseDuration = iPeriodMicro / 2;

            m_ledDownFast.on();
            SleepUtil.sleepMicros(iPulseDuration);
            m_ledDownFast.off();
            SleepUtil.sleepMicros(iPulseDuration);

            bStop = m_butStopApp.isPressed();
        }
    }


    //--------------------------------------------------------------------------------
    //  stepperTest
    //--------------------------------------------------------------------------------
    public static void stepperTest()
    {
        m_pinEnable.on(); // Enables the motor to move in a particular direction

        int iPulseDurationUs = 20000;
        int iTestLoops = 1600;

        m_pinDir.on(); // move clockwise ???

        while (iTestLoops > 0)
        {
            m_pinStep.on();
            SleepUtil.sleepMicros(iPulseDurationUs);
            m_pinStep.off();
            SleepUtil.sleepMicros(iPulseDurationUs);
            iTestLoops--;
        }

        SleepUtil.sleepSeconds(1);

        m_pinDir.off(); // move counterclockwise ???

        iTestLoops = 1600;
        while (iTestLoops > 0)
        {
            m_pinStep.on();
            SleepUtil.sleepMicros(iPulseDurationUs);
            m_pinStep.off();
            SleepUtil.sleepMicros(iPulseDurationUs);
            iTestLoops--;
        }

        m_pinEnable.off(); // Disable the motor power

    }



    //--------------------------------------------------------------------------------
    //  rampTest
    //--------------------------------------------------------------------------------
    public static void rampTest()
    {
        double dbStartFrequencyHz =  100.0;
        double dbMaxFrequencyHz   = 1400.0;
        double dbAccelerationHz2  = 5000.0;
        double dbDecelerationHz2  = 5000.0;


        m_pinEnable.on(); // Enables the motor to move in a particular direction
        m_pinDir.on(); // move clockwise ???

        //--------------------------------------------
        // acceleration phase
        double dbTimeToAccelerate  = (dbMaxFrequencyHz - dbStartFrequencyHz) / dbAccelerationHz2;
        int iStepsToAccelerate = (int)((dbMaxFrequencyHz - dbStartFrequencyHz) / 2 * dbTimeToAccelerate);
        int iPulseDurationUs = (int)(1000000.0 / dbStartFrequencyHz);
        int iDurationDecrementUs = 1;
        while (iStepsToAccelerate > 0)
        {
            m_pinStep.on();
            SleepUtil.sleepMicros(iPulseDurationUs);
            m_pinStep.off();
            SleepUtil.sleepMicros(iPulseDurationUs);
            iStepsToAccelerate--;
            iPulseDurationUs -= iDurationDecrementUs;
            LOGGER.info("iPulseDurationUs: {} / Frequency: {}", iPulseDurationUs, 1000000/iPulseDurationUs);

        }

        //--------------------------------------------
        // movement phase
        int iStepsForDistance = 1600; // 8 rotations
        iPulseDurationUs = (int)(1000000.0 / dbMaxFrequencyHz);
        while (iStepsForDistance > 0)
        {
            m_pinStep.on();
            SleepUtil.sleepMicros(iPulseDurationUs);
            m_pinStep.off();
            SleepUtil.sleepMicros(iPulseDurationUs);
            iStepsForDistance--;
        }

        //--------------------------------------------
        // deceleration phase
        double dbTimeToDecelerate  = (dbMaxFrequencyHz - dbStartFrequencyHz) / dbDecelerationHz2; // in seconds !
        int iStepsToDeccelerate = (int)((dbMaxFrequencyHz - dbStartFrequencyHz) / 2 * dbTimeToDecelerate);

        iPulseDurationUs = (int)(1000000.0 / dbMaxFrequencyHz);
        int iDurationIncrementUs = 1;
        while (iStepsToDeccelerate > 0)
        {
            m_pinStep.on();
            SleepUtil.sleepMicros(iPulseDurationUs);
            m_pinStep.off();
            SleepUtil.sleepMicros(iPulseDurationUs);
            iStepsToDeccelerate--;
            iPulseDurationUs += iDurationIncrementUs;
            LOGGER.info("iPulseDurationUs: {} / Frequency: {}", iPulseDurationUs, 1000000/iPulseDurationUs);
        }

        m_pinEnable.off(); // Disable the motor power
    }


    //--------------------------------------------------------------------------------
    //  stepperMoveByButtonClick
    //--------------------------------------------------------------------------------
    public static void stepperMoveByButtonClick()
    {
        boolean bStop = m_butStopApp.isPressed();
        while (! bStop)
        {
            stepperMoveSlow(m_butUpSlow, true);
            stepperMoveSlow(m_butDownSlow, false);
            stepperMoveFast(m_butUpFast, true);
            stepperMoveFast(m_butDownFast, false);

            bStop = m_butStopApp.isPressed();
        }
    }

    private static void stepperMoveSlow(Button p_SlowButton, boolean p_bClockwise)
    {
        int iSlowFrequencyHz = 20;
        int iPulseDurationUs = (int)(1000000.0 / iSlowFrequencyHz);

        boolean bButtonSlow = p_SlowButton.isPressed();

        if (bButtonSlow)
        {
            m_pinEnable.on();
            m_pinDir.setOn(p_bClockwise);
            while (bButtonSlow)
            {
                m_pinStep.on();
                SleepUtil.sleepMicros(iPulseDurationUs);
                m_pinStep.off();
                SleepUtil.sleepMicros(iPulseDurationUs);

                bButtonSlow =  p_SlowButton.isPressed();
            }
            m_pinEnable.off();
        }
    }

    private static void stepperMoveFast(Button p_FastButton, boolean p_bClockwise)
    {
        double dbStartFrequencyHz =  100.0;
        double dbMaxFrequencyHz   = 1800.0;
        double dbAccelerationHz2  = 5000.0;
        double dbDecelerationHz2  = 5000.0;

        int iPulseDurationUs = (int)(1000000.0 / dbMaxFrequencyHz);

        boolean bButtonFast = p_FastButton.isPressed();

        if (bButtonFast)
        {
            m_pinEnable.on();
            m_pinDir.setOn(p_bClockwise);
            while (bButtonFast)
            {
                m_pinStep.on();
                SleepUtil.sleepMicros(iPulseDurationUs);
                m_pinStep.off();
                SleepUtil.sleepMicros(iPulseDurationUs);

                bButtonFast = p_FastButton.isPressed();
            }
            m_pinEnable.off();

        }

    }



    //--------------------------------------------------------------------------------
    //  testHX711
    //--------------------------------------------------------------------------------
    private static void testHX711()
    {
        EconversionMode eThisTestMode = EconversionMode.CHANNEL_A_128;

        HX711 testComponent = new HX711(HX711_DATA_OUT_PIN, HX711_CLOCK_IN_PIN, eThisTestMode);

        //testComponent.setReadingFormat("LSB", "MSB");

        testComponent.setReferenceUnit(-47);

        testComponent.resetConverter();

        System.out.println(("dummy read: " + (int)testComponent.getUnits(5)));

        testComponent.tare(5);

        boolean bStop = m_butStopApp.isPressed();

        while (!bStop)
        {

            // These three lines are usefull to debug wether to use MSB or LSB in the reading formats
            // for the first parameter of "hx.set_reading_format("LSB", "MSB")".
            // Comment the two lines "val = hx.get_weight(5)" and "print val" and uncomment the three lines to see what it prints.
            // np_arr8_string = hx.get_np_arr8_string()
            // binary_string = hx.get_binary_string()
            // print binary_string + " " + np_arr8_string

            // Prints the weight. Comment if you're debbuging the MSB and LSB issue.

            double dbVal = testComponent.getUnits(1);

            System.out.println(("getUnits: " + (int)dbVal + " gram"));

            //System.out.println(dbVal);

            testComponent.resetConverter();

//            SleepUtil.sleepMillis(30);

            //except (KeyboardInterrupt, SystemExit):
            //cleanAndExit()

            bStop = m_butStopApp.isPressed();

        }
    }









}
