package Experimental;// package decl

import com.diozero.ws281xj.LedDriverInterface;
import com.diozero.ws281xj.PixelAnimations;
import com.diozero.ws281xj.PixelColour;
import com.diozero.ws281xj.rpiws281x.WS281x;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//===============================================================================
//  WS2812B RGB LED Experiments
//===============================================================================
public class LedChainTest
{
    private static final Logger LOGGER = LoggerFactory.getLogger(FirstExperiments.class);

    // GPIOxx RaspberryPi naming (e.g. GPIO23 -> header pin 16 / GPIO_GEN4)
    private static final int BUTTON_NR_STOP_APP  = 26;

    private static final int WS2812B_DATA_LINE_PIN = 18;

    private static LedDriverInterface m_LedDriver;

    //===============================================================================
    //  MAIN
    //===============================================================================
    public static void main(String[] args) {
        LOGGER.info("LedChainTest main invoked");

        String sJavaLibPath = System.getProperty("java.library.path");
        System.out.println(sJavaLibPath);

        int iBrightness = 64; // 0..255
        int iNrOfLeds   = 8;

        m_LedDriver = new WS281x(WS2812B_DATA_LINE_PIN, iBrightness, iNrOfLeds);

        m_LedDriver.getNumPixels();
        //checkAllGpioPins();
        colorRange();
        hueSatBrightTest();
        hueSatLuminTest();
        colorWipe(0x7F0000, "red");
        colorWipe(0x007F00, "green");
        colorWipe(0x00007F, "blue");
        rainbowAll();
        chasingColors(0x7F7F7F, "white");
        chasingColors(0x7F0000, "red");
        chasingColors(0x00007F, "blue");
        chasingRainbow();
        rainbowColors();
        rainbowCycle();


        LOGGER.info("LedChainTest main close hardware");

        LOGGER.info("LedChainTest main exit");

    }

    private static void checkAllGpioPins()
    {
        int iBrightness = 64; // 0..255
        int iNrOfLeds   = 1;

        for (int iGPIO = 1; iGPIO < 40; iGPIO++)
        {
            System.out.println("Trying GPIO " + iGPIO);
            try
            {
                LedDriverInterface dummy =  new WS281x(iGPIO, iBrightness, iNrOfLeds);
                System.out.println("Success GPIO " + iGPIO);
            }
            catch(Throwable t)
            {
                System.out.println("Failed GPIO " + iGPIO);
            }
        }
    }


    private static void colorRange()
    {
        System.out.println("    private static void colorRange()\n()");

        int iDelayMs = 5;
        int iNrOfLeds = _GetNrOfLeds();

        // Gradually add red
        System.out.println("Adding red   0..255");
        for (int iRedLevel = 0; iRedLevel < 256; iRedLevel++)
        {
            for (int iLed = 0; iLed < iNrOfLeds; iLed++)
            {
                m_LedDriver.setRedComponent(iLed, iRedLevel);
            }
            m_LedDriver.render();
            PixelAnimations.delay(iDelayMs);
        }
        m_LedDriver.allOff();

        // Gradually add green
        System.out.println("Adding green 0..255");
        for (int iGreenLevel = 0; iGreenLevel < 256; iGreenLevel++)
        {
            for (int iLed = 0; iLed < iNrOfLeds; iLed++)
            {
                m_LedDriver.setGreenComponent(iLed, iGreenLevel);
            }
            m_LedDriver.render();
            PixelAnimations.delay(iDelayMs);
        }
        m_LedDriver.allOff();

        // Gradually add blue
        System.out.println("Adding blue  0..255");
        for (int iBlueLevel = 0; iBlueLevel < 256; iBlueLevel++)
        {
            for (int iLed = 0; iLed < iNrOfLeds; iLed++)
            {
                m_LedDriver.setBlueComponent(iLed, iBlueLevel);
            }
            m_LedDriver.render();
            PixelAnimations.delay(iDelayMs);
        }
        m_LedDriver.allOff();

        // Gradually add white
        System.out.println("Adding white 0..255");

    }


    private static void rainbowColors()
    {
        System.out.println("rainbowColours()");

        // 24 bits [r7..r0][g7..g0][b7..b0]
        int[] iColorArray = new int[] {0x400000, 0x402000, 0x404000, 0x004000, 0x004040, 0x000040, 0x200020, 0x400020};

        int iDelayMs    = 50;
        int iNrOfLeds   = _GetNrOfLeds();
        int iNrOfColors = iColorArray.length;

        for (int iLoop = 0; iLoop < 250; iLoop++)
        {
            for (int iLed = 0; iLed < iNrOfLeds; iLed++)
            {
                int iColorIndex = (iLoop + iLed) % iNrOfColors;
                int iCurrentColor = iColorArray[iColorIndex];
                _SetPixelColor(iLed, iCurrentColor);
            }

            m_LedDriver.render();
            PixelAnimations.delay(iDelayMs);
        }
        m_LedDriver.allOff();
    }

    private static void hueSatBrightTest()
    {
        System.out.println("hueSatBrightTest()");

        int   iDelayMs    = 25;
        int   iNrOfLeds   = _GetNrOfLeds();
        float fBrightness = 0.5f;

        for (float fHue = 0.0f; fHue < 1.0f; fHue += 0.05f)
        {
            for (float fSaturation = 0.0f; fSaturation <= 1.0f; fSaturation += 0.05f)
            {
                for (int iLed = 0; iLed < iNrOfLeds; iLed++)
                {
                    m_LedDriver.setPixelColourHSB(iLed, fHue, fSaturation, fBrightness);
                }
                m_LedDriver.render();
                PixelAnimations.delay(iDelayMs);
            }
        }
        m_LedDriver.allOff();
    }

    private static void hueSatLuminTest()
    {
        System.out.println("hueSatLuminTest()");

        int   iDelayMs   = 25;
        int   iNrOfLeds  = _GetNrOfLeds();
        float fLuminance = 0.5f;
        float fHueIncrement = 360/20;

        for (float fHue = 0.0f; fHue < 360.0f; fHue += fHueIncrement)
        {
            for (float fSaturation = 0.0f; fSaturation <= 1.0f; fSaturation += 0.05f)
            {
                for (int iLed = 0; iLed < iNrOfLeds; iLed++)
                {
                    m_LedDriver.setPixelColourHSL(iLed, fHue, fSaturation, fLuminance);
                }
                m_LedDriver.render();
                PixelAnimations.delay(iDelayMs);
            }
        }
        m_LedDriver.allOff();
    }


    private static void chasingRainbow()
    {
        System.out.println("chasingRainbow()");

        int   iDelayMs   = 50;
        int   iNrOfLeds  = _GetCorrectedNrOfLeds(3);

        for(int iLoop = 0; iLoop < 256; ++iLoop)
        {
            for(int iLedOffset = 0; iLedOffset < 3; ++iLedOffset)
            {
                for (int iLedBase = 0; iLedBase < iNrOfLeds; iLedBase += 3)
                {
                    int iCurrentColor = PixelColour.wheel((iLedBase + iLoop) % 255);
                    _SetPixelColor(iLedBase + iLedOffset, iCurrentColor);
                }

                m_LedDriver.render();
                PixelAnimations.delay(iDelayMs);

                for (int iLedBase = 0; iLedBase < iNrOfLeds; iLedBase += 3)
                {
                    _SetPixelColor(iLedBase + iLedOffset, 0);
                }
            }
        }
        m_LedDriver.allOff();
    }


    private static void chasingColors(int p_iColor, String p_sColorName)
    {
        System.out.println("chasingColors - " + p_sColorName);

        int   iDelayMs   = 50;
        int   iNrOfLeds  = _GetCorrectedNrOfLeds(3);

        for(int iLoop = 0; iLoop < 30; ++iLoop)
        {
            for(int iLedOffset = 0; iLedOffset < 3; ++iLedOffset)
            {
                // turn the LED on
                for (int iLedBase = 0; iLedBase < iNrOfLeds; iLedBase += 3)
                {
                    _SetPixelColor(iLedBase + iLedOffset, p_iColor);
                }

                m_LedDriver.render();
                PixelAnimations.delay(iDelayMs);

                // turn the LED off
                for (int iLedBase = 0; iLedBase < iNrOfLeds; iLedBase += 3)
                {
                    _SetPixelColor(iLedBase + iLedOffset, 0);
                }
            }
        }
        m_LedDriver.allOff();
    }


    private static void rainbowCycle()
    {
        System.out.println("rainbowCycle");

        int   iDelayMs   = 15;
        int   iNrOfLeds  = _GetCorrectedNrOfLeds(3);

        for(int iLoop = 0; iLoop < 1280; iLoop++)
        {
            for (int iLed = 0; iLed < iNrOfLeds; iLed++)
            {
                int iCurrentColor = PixelColour.wheel((iLed * 256 / iNrOfLeds + iLoop & 255));
                _SetPixelColor(iLed, iCurrentColor);
            }
            m_LedDriver.render();
            PixelAnimations.delay(iDelayMs);
        }

        m_LedDriver.allOff();
    }


    private static void colorWipe(int p_iColor, String p_sColorName)
    {
        System.out.println("colorWipe - " + p_sColorName);

        int   iDelayMs   = 50;
        int   iNrOfLeds  = _GetCorrectedNrOfLeds(3);

        for (int iLed = 0; iLed < iNrOfLeds; iLed++)
        {
            _SetPixelColor(iLed, p_iColor);
            m_LedDriver.render();
            PixelAnimations.delay(iDelayMs);
        }
        m_LedDriver.allOff();
    }


    private static void rainbowAll()
    {
        System.out.println("rainbowAll");

        int   iDelayMs   = 15;
        int   iNrOfLeds  = _GetCorrectedNrOfLeds(3);

        for(int iLoop = 0; iLoop < 256; iLoop++)
        {
            for (int iLed = 0; iLed < iNrOfLeds; iLed++)
            {
                int iCurrentColor = PixelColour.wheel((iLed + iLoop & 255));
                _SetPixelColor(iLed, iCurrentColor);
            }

            m_LedDriver.render();
            PixelAnimations.delay(iDelayMs);
        }
        m_LedDriver.allOff();
    }


    private static int _GetNrOfLeds()
    {
        return m_LedDriver.getNumPixels();
    }

    private static int _GetCorrectedNrOfLeds(int p_iLengthOfSegment)
    {
        int iNrOfLeds = _GetNrOfLeds();
        if (iNrOfLeds % p_iLengthOfSegment != 0)
        {
            // correct the number of leds to avoid an error
            // i.e. 8 leds with a segment length of 3 does not work well (mod 3)
            // increase the number of leds to use all existing ones but let the
            // additional leds be dummies (waste of time)
            iNrOfLeds = iNrOfLeds + p_iLengthOfSegment - (iNrOfLeds % p_iLengthOfSegment);
        }
        return iNrOfLeds;
    }


    private static void _SetPixelColor(int p_iLedNr, int p_iColor)
    {
        // ommit commands for inexisting leds
        if (p_iLedNr < m_LedDriver.getNumPixels())
        {
            m_LedDriver.setPixelColour(p_iLedNr, p_iColor);
        }
    }

}
