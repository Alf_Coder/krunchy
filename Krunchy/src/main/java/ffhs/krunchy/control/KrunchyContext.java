/*
 * Copyright (c) 2018 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */
package ffhs.krunchy.control;

import com.diozero.api.DigitalInputDevice;
import com.diozero.api.GpioEventTrigger;
import com.diozero.api.GpioPullUpDown;
import com.diozero.devices.Button;
import com.diozero.devices.LED;
import ffhs.krunchy.control.statemachine.KrunchyState;
import ffhs.krunchy.control.statemachine.KrunchyStateStarting;
import ffhs.krunchy.control.statemachine.diagnostic.KrunchyStateDiag;
import ffhs.krunchy.control.statemachine.initialising.*;
import ffhs.krunchy.control.statemachine.running.*;
import ffhs.krunchy.force.KrunchyForceMeasurement;
import ffhs.krunchy.motion.KrunchyMotorDriver;
import ffhs.krunchy.motion.KrunchySpeedController;
import ffhs.krunchy.synchronisation.SynchObject;
import ffhs.krunchy.synchronisation.SynchObjectCompletion;
import ffhs.krunchy.vision.Illumination_WS2812;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  The context class of the main runtime environment of the Krunchy
 */
public class KrunchyContext
{
    private final Logger LOGGER = LoggerFactory.getLogger(KrunchyContext.class);

    // configuration components
    private String m_sProperties;

    // control components
    private KrunchyState m_CurrentState;
    private KrunchyState m_NextState;
    private KrunchyGuardManager m_GuardManager;

    // user interface components
    private Button m_StartButton;
    private LED    m_StartLed;

    private Button m_StopButton;
    private LED    m_StopLed;

    private DigitalInputDevice m_CoverSwitch;

    // motion components
    private KrunchyMotorDriver m_MotorDriver;

    // force measurement components
    private KrunchyForceMeasurement m_ForceMeasurement;

    // speed controlling componets
    private KrunchySpeedController m_SpeedController;

    // process illumination
    private Illumination_WS2812 m_IlluminationDriver;



    /**
     *  construnction of the context
     *
     *  - instantiates all components
     *  - start the statemachine
     *
     * @param p_sPropertiesFile  name of the configuration properties file
     */
    public KrunchyContext(String p_sPropertiesFile)
    {
        m_sProperties = p_sPropertiesFile;
        initInstance();
    }


    /**
     *  setup all needed for operation
     */
    private void initInstance()
    {
        // - read the configuration data
        KrunchyConfig.createInstance(m_sProperties);

        // - setup the hardware layer
        createHardwareComponents();

        setGuardmanager(new KrunchyGuardManager(this));

        // - create all state singletons
        createStates();

    }

    private void createHardwareComponents()
    {
        Integer iPinNumber = 0;

        // create the start button and led
        iPinNumber = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.Button.GPIO.Pin.Start"));
        m_StartButton = new Button(iPinNumber, GpioPullUpDown.PULL_DOWN);
        iPinNumber = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.Led.GPIO.Pin.Start"));
        m_StartLed = new LED(iPinNumber);

        // create the shutdown button and led
        iPinNumber = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.Button.GPIO.Pin.Stop"));
        m_StopButton = new Button(iPinNumber, GpioPullUpDown.PULL_DOWN);
        iPinNumber = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.Led.GPIO.Pin.Stop"));

        // LED(GpioDeviceFactoryInterface deviceFactory, int gpio, boolean activeHigh, boolean initialValue) {
        //        m_StopLed  = new LED(new GpioDeviceFactoryInterface() , iPinNumber, true, false);
        m_StopLed = new LED(iPinNumber, true);

        // create the cover switch
        iPinNumber = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.Switch.GPIO.Pin.Cover"));

        // DigitalInputDevice(int gpio, GpioPullUpDown pud, GpioEventTrigger trigger) throws RuntimeIOException
        m_CoverSwitch = new DigitalInputDevice(iPinNumber, GpioPullUpDown.PULL_DOWN, GpioEventTrigger.BOTH);

    }


    //-------------------------------------------------------------------------------------------------
    // cleanup what's dirty
    //-------------------------------------------------------------------------------------------------
    public void exitInstance()
    {
        // - switch off the motor power
        m_MotorDriver.switchOffMotorPower();
        m_MotorDriver.switchOffCoolingFan();
        m_MotorDriver.shutdown();

        // - shutdown the A/D reader threads
        m_ForceMeasurement.shutdown();

        // - switch off the button LEDs
        m_StartLed.off();
        m_StartLed.close();
        m_StopLed.off();
        m_StopLed.close();

        // - turn off the RGB illumination
        m_IlluminationDriver.shutdown();


    }


    //-------------------------------------------------------------------------------------------------
    // do a explicit creation which can aet the context in the state objects
    //-------------------------------------------------------------------------------------------------
    private void createStates()
    {
        setCurrentState(null);
        setNextState(null);

        KrunchyStateStarting.createInstance(this);

        // sub-statemachine initialising
        KrunchyStateInitParameters.createInstance(this);
        KrunchyStateInitStartingSubsys.createInstance(this);
        KrunchyStateInitError.createInstance(this);
        KrunchyStateInitCheckingSafety.createInstance(this);
        KrunchyStateInitHomingCruncherAxis.createInstance(this);
        KrunchyStateInitSettingTare.createInstance(this);

        // sub-statemachine diagnostic
        KrunchyStateDiag.createInstance(this);

        // sub-statemachine running
        KrunchyStateRunIdle.createInstance(this);
        KrunchyStateRunCracking.createInstance(this);
        KrunchyStateRunError.createInstance(this);
        KrunchyStateRunStopping.createInstance(this);
        KrunchyStateRunReleasing.createInstance(this);
        KrunchyStateRunSafetyStop.createInstance(this);

    }

    private void createActions()
    {
    }


    //-------------------------------------------------------------------------------------------------
    public void changeState(KrunchyState p_NextState)
    {
        if (p_NextState != null)
        {
            String sToState = p_NextState.toString();
            String sFromState = "<NONE-State>";

            if (getCurrentState() != null)
            {
                sFromState = getCurrentState().toString();

                // current state must be ended properly before it is forgotten
                getCurrentState().exit();
            }

            LOGGER.debug("State transition from {} to {}", sFromState, sToState);

            // set both to the nextstate for the RunStateMachine() function
            setCurrentState(p_NextState);
            setNextState(p_NextState);
        }
        else
        {
            String sFromState = "<NONE>";

            if (getCurrentState() != null)
            {
                sFromState = getCurrentState().toString();

                // current state must be ended properly before it is forgotten
                getCurrentState().exit();
            }
            LOGGER.error("Error: tried to change from {} to a <NONE-State>", sFromState);
        }
    }


    //-------------------------------------------------------------------------------------------------
    public void runStateMachine()
    {
        KrunchyState tempNextState = getNextState();

        while (getNextState() != null)
        {
            tempNextState = getNextState();
            setNextState(null);  // one time consumption

            tempNextState.enter();
            tempNextState.doIt();
        }

        LOGGER.debug("runStateMachine ended from {}", getCurrentState().toString());
    }

    public boolean waitingFor(SynchObject p_SyncObject)
    {
        boolean bSuccess = true;
        LOGGER.debug("waitingFor({})", p_SyncObject.getName());

        synchronized(p_SyncObject)
        {
            try
            {
                LOGGER.debug("waitingFor is going to wait");
                p_SyncObject.wait();

                if (SynchObjectCompletion.class == p_SyncObject.getClass())
                {
                    SynchObjectCompletion temp = (SynchObjectCompletion)p_SyncObject;
                    bSuccess = !temp.isError();
                }

                LOGGER.debug("waitingFor wait finished");
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        return bSuccess;
    }


    public void threadSleepMillis(int p_iMilliSeconds)
    {
        try
        {
            Thread.sleep(p_iMilliSeconds);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public void setSystemParameters()
    {
        LOGGER.error("setSystemParameters\t {}", "no more specific parameter have to be set");
    }


    public void startForceController()
    {
        // create the a/d converters
        LOGGER.error("startForceController\t {}", "");

        m_ForceMeasurement = new KrunchyForceMeasurement();
    }

    public void startMotorController()
    {
        // create the stepper motor driver board
        LOGGER.error("startMotorController\t {}", "");

        m_SpeedController  = new KrunchySpeedController(m_ForceMeasurement);
        m_MotorDriver      = new KrunchyMotorDriver(m_SpeedController);
    }

    public void startIlluminationController()
    {
        // create the RGB LEDs
        LOGGER.error("startIlluminationController\t {}", "with 2 RGB LED");

        m_IlluminationDriver = new Illumination_WS2812(m_SpeedController);
        m_IlluminationDriver.start();
    }


    public void initialiseCruncherAxis()
    {
        LOGGER.debug("\n");
        LOGGER.error("initialiseCruncherAxis\t {}", "");
        LOGGER.debug("======================");
        waitingFor(m_MotorDriver.initializeAxis());
    }

    public SynchObjectCompletion doCrackCycle()
    {
        LOGGER.debug("\n");
        LOGGER.error("doCrackCycle\t {}", "");
        LOGGER.debug("============");

        SynchObjectCompletion crunchCompleted = m_MotorDriver.crunchWithForceSpeed();
        return crunchCompleted;
    }


    public SynchObjectCompletion doReleaseAxis()
    {
        LOGGER.debug("\n");
        LOGGER.error("doReleaseAxis\t {}", "");
        LOGGER.debug("=============");

        SynchObjectCompletion crunchCompleted = m_MotorDriver.releaseWithForceSpeed();
        return crunchCompleted;
    }


    public void tareForceMeasurement()
    {
        int iForceOld = m_ForceMeasurement.getCurrentForce();
        m_ForceMeasurement.tare();
        int iForceNew = m_ForceMeasurement.getCurrentForce();
        LOGGER.error("tareForceMeasurement\t old: {} new {}", iForceOld, iForceNew);
    }



    public void doDiagnosticCycles()
    {
        setButtonLedForCoverOpen();
        m_MotorDriver.selfTest();

    }

    public void doManualMove()
    {

    }






    public boolean isCruncherBelowSensor()
    {
        return m_MotorDriver.isAxisBelowRefSensor();
    }






    public Button getStartButton()
    {
        return m_StartButton;
    }

    public Button getStopButton()
    {
        return m_StopButton;
    }

    public DigitalInputDevice getCoverSwitch()
    {
        return m_CoverSwitch;
    }

    public void setStopButtonActive()
    {
        m_StopButton.whenPressed(m_MotorDriver::stopMovement);

/*
    public static void test(int buttonPin, int ledPin)
    {
        try (Button button = new Button(buttonPin, GpioPullUpDown.PULL_UP); LED led = new LED(ledPin))
        {
            button.whenPressed(led::on);
            button.whenReleased(led::off);

            Logger.info("Waiting for 10s - *** Press the button connected to pin {} ***", Integer.valueOf(buttonPin));
            SleepUtil.sleepSeconds(10);
        }
        catch (RuntimeIOException e)
        {
            Logger.error(e, "Error: {}", e);
        }
    }

    public static void test(int inputPin)
    {
        try (WaitableDigitalInputDevice input = new WaitableDigitalInputDevice(inputPin, GpioPullUpDown.PULL_UP, GpioEventTrigger.BOTH)) {
            while (true)
            {
                Logger.info("Waiting for 2000ms for button press");
                boolean notified = input.waitForValue(false, 2000);
                Logger.info("Timed out? " + !notified);
                Logger.info("Waiting for 2000ms for button release");
                notified = input.waitForValue(true, 2000);
                Logger.info("Timed out? " + !notified);
            }
        }
        catch (RuntimeIOException ioe)
        {
            Logger.error(ioe, "Error: {}", ioe);
        }
        catch (InterruptedException e)
        {
            Logger.error(e, "Error: {}", e);
        }
    }
*/

    }

    public void setButtonLedOff()
    {
        m_StartLed.off();
        m_StopLed.off();
    }

    public void setButtonLedForCoverOpen()
    {
        m_StartLed.blink(0.15f, 0.15f, -1, true);
        threadSleepMillis(150);
        m_StopLed.blink(0.15f, 0.15f, -1, true);
    }

    public void setButtonLedForStart()
    {
        m_StartLed.off();
        m_StartLed.blink(0.5f, 0.5f, -1, true);
        m_StopLed.off();
    }

    public void setButtonLedStoppableRun()
    {
        m_StartLed.on();
        m_StopLed.blink(0.5f, 0.5f, -1, true);
    }



    //======================================================
    // Getter / Setter
    //======================================================

    public KrunchyGuardManager getGuardManager()
    {
        return m_GuardManager;
    }

    public void setGuardmanager(KrunchyGuardManager p_Guardmanager)
    {
        m_GuardManager = p_Guardmanager;
    }

    public KrunchyState getCurrentState()
    {
        return m_CurrentState;
    }

    public void setCurrentState(KrunchyState p_CurrentState)
    {
        m_CurrentState = p_CurrentState;
    }

    public KrunchyState getNextState()
    {
        return m_NextState;
    }

    public void setNextState(KrunchyState p_NextState)
    {
        m_NextState = p_NextState;
    }

}

