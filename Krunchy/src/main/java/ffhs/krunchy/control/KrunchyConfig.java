/*
 * Copyright (c) 2019 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.control;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class KrunchyConfig
{
    private final Logger LOGGER = LoggerFactory.getLogger(KrunchyContext.class);

    private static KrunchyConfig s_Instance = null;

    private Properties m_Properties;


    /**
     * create an instance of this configuration class if it is not instantiated yet
     *
     * @param p_Context the underlying context to be stored in the super class
     */
    public static void createInstance(String p_sProperties)
    {
        if (s_Instance == null)
        {
            s_Instance = new KrunchyConfig(p_sProperties);
        }
    }


    /**
     * return the one and only instance of this configuration class
     */
    public static KrunchyConfig getInstance()
    {
        return s_Instance;
    }


    /**
     * return the value for the given key if it exist
     * @param  p_sKey for the requested value
     * @return String
     */
    public String getValue(String p_sKey)
    {
        String sValue = m_Properties.getProperty(p_sKey);

        if (sValue == null)
        {
            sValue = "property " + p_sKey + "not found";
        }

        return  sValue;
    }


    /**
     * initialize the singleton configuration object
     *
     * @param p_sProperties the name of the properties file
     */
    private KrunchyConfig(String p_sProperties)
    {
        // should be available from now on
        m_Properties = new Properties();
        InputStream propertiesReader = null;
        try
        {
            propertiesReader = new FileInputStream(p_sProperties);
            m_Properties.load(propertiesReader);

            String sVersion = m_Properties.getProperty("SoftwareVersion");
            LOGGER.info("Krunchy Controll-Software Version: {}", sVersion);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            if (propertiesReader != null)
            {
                try
                {
                    propertiesReader.close();
                }
                catch (IOException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
    }



}
