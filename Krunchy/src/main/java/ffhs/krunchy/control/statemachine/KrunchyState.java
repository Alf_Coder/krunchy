/*
 * Copyright (c) 2018 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */
package ffhs.krunchy.control.statemachine;

import ffhs.krunchy.control.KrunchyContext;
import ffhs.krunchy.control.KrunchyGuardManager;

/**
 * Acts as the super class of all state classes in the krunchy statemachines
 * collects all commons for the states
 */
public abstract class KrunchyState
{
    protected static KrunchyContext s_Context;
    protected String m_sStateName = "undefined name";
	public KrunchyContext m_KrunchyContext;

    public KrunchyState(KrunchyContext p_Context)
    {
        s_Context = p_Context;
    }

    @Override
    public String toString()
    {
        return m_sStateName;
    }

    public KrunchyContext getContext()
    {
        return s_Context;
    }

    protected KrunchyGuardManager getGuardManager()
    {
        return getContext().getGuardManager();
    }

    protected void changeState(KrunchyState p_NextState)
    {
        getContext().changeState(p_NextState);
    }

    public abstract void enter();

    public abstract void doIt();

    public abstract void exit();


}