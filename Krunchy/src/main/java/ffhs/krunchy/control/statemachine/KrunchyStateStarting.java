/*
 * Copyright (c) 2018 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */
package ffhs.krunchy.control.statemachine;

import ffhs.krunchy.control.KrunchyContext;
import ffhs.krunchy.control.statemachine.diagnostic.KrunchyStateDiag;
import ffhs.krunchy.control.statemachine.initialising.KrunchyStateInitParameters;

/**
 * Created by Andy on 25.11.2018.
 * @author andreas.frommel
 * @version 1.0
 * @updated 30-Nov-2018 05:58:23
 */
public class KrunchyStateStarting extends KrunchyState
{

    private static KrunchyStateStarting s_Instance = null;

    public void finalize() throws Throwable
    {
        super.finalize();
    }

    /**
     * initialize the state object
     *
     * @param p_Context
     */
    private KrunchyStateStarting(KrunchyContext p_Context)
    {
        super(p_Context);
        m_sStateName = "KrunchyStateStarting";
    }

    /**
     * create an instance of this state class if it is not instantiated yet
     *
     * @param p_Context the underlying context to be stored in the super class
     */
    public static void createInstance(KrunchyContext p_Context)
    {
        if (s_Instance == null)
        {
            s_Instance = new KrunchyStateStarting(p_Context);
        }
    }

    /**
     * return the one and only instance of this state class
     */
    public static KrunchyStateStarting getInstance()
    {
        return s_Instance;
    }

    @Override
    public void doIt()
    {
        boolean bStartButtonPressedAtStartup = getGuardManager().isStartButtonPressed();

        if (bStartButtonPressedAtStartup)
        {
            changeState(KrunchyStateDiag.getInstance());
        }
        else
        {
            changeState(KrunchyStateInitParameters.getInstance());
        }
    }

    @Override
    public void enter()
    {
        // nothing to do on enter so far
    }

    @Override
    public void exit()
    {
        // nothing to do on exit so far
    }
}