/*
 * Copyright (c) 2018 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.control.statemachine.running;


import ffhs.krunchy.control.KrunchyContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class KrunchyStateRunIdle extends KrunchyStateRun
{
    private final Logger LOGGER = LoggerFactory.getLogger(KrunchyContext.class);

    private static KrunchyStateRunIdle s_Instance = null;

    private KrunchyStateRunIdle(KrunchyContext p_Context)
    {
        super(p_Context);
        m_sStateName = "KrunchyStateRunIdle";
    }

    /**
     * create an instance of this state class if it is not instantiated yet
     * @param p_Context  the underlying context to be stored in the super class
     */
public static void createInstance(KrunchyContext p_Context)
    {
        if (s_Instance == null)
        {
            s_Instance = new KrunchyStateRunIdle(p_Context);
        }
    }

    /**
     * return the one and only instance of this state class
     */
    public static KrunchyStateRunIdle getInstance()
    {
        return s_Instance;
    }

    @Override
    public void doIt()
    {
        boolean bCoverClosed = getContext().getGuardManager().isCoverClosed();
        boolean bStartButtonPressed = getContext().getGuardManager().isStartButtonPressed();

        if (bCoverClosed)
        {
            boolean bCruncherReleased = getContext().getGuardManager().isCruncherReleased();

            if (bCruncherReleased)
            {
                if (bStartButtonPressed)
                {
                    getContext().changeState(KrunchyStateRunCracking.getInstance());
                }
                else
                {
                    // nothing to do so far
                    getContext().setButtonLedForStart();

                    boolean bStayInLoop = true;
                    while(bStayInLoop)
                    {
                        getContext().threadSleepMillis(250);

                        bCoverClosed = getContext().getGuardManager().isCoverClosed();
                        bStartButtonPressed = getContext().getGuardManager().isStartButtonPressed();
                        bStayInLoop = bCoverClosed && !bStartButtonPressed;
                    }

                    getContext().setButtonLedOff();

                    if (!bCoverClosed)
                    {
                        // cover open has priority
                        getContext().changeState(KrunchyStateRunSafetyStop.getInstance());
                    }
                    else
                    {
                        // if cover still closed the start button was pressed
                        getContext().changeState(KrunchyStateRunCracking.getInstance());
                    }
                }
            }
            else
            {
                getContext().changeState(KrunchyStateRunReleasing.getInstance());
            }
        }
        else
        {
            getContext().changeState(KrunchyStateRunSafetyStop.getInstance());
        }
    }


    @Override
    public void enter()
    {
        // nothing to do on enter so far
    }

    @Override
    public void exit()
    {
        // nothing to do on exit so far
    }

}
