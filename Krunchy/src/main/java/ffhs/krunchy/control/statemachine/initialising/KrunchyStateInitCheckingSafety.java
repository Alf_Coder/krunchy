/*
 * Copyright (c) 2018 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */
package ffhs.krunchy.control.statemachine.initialising;

import ffhs.krunchy.control.KrunchyContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  State of the Krunchy initialisation statemacine
 *  Checks whether to continue or remain dependent on the safety key-switch and
 *  the safety cover
 *
 */
public class KrunchyStateInitCheckingSafety extends KrunchyStateInit
{
    private final Logger LOGGER = LoggerFactory.getLogger(KrunchyContext.class);

    private static KrunchyStateInitCheckingSafety s_Instance = null;

    private KrunchyStateInitCheckingSafety(KrunchyContext p_Context)
    {
        super(p_Context);
        m_sStateName = "KrunchyStateInitCheckingSafety";
    }

    /**
     * create the one and only instance of this state class if it is not instantiated yet
     *
     * @param p_Context the underlying context to be stored in the super class
     */
    public static void createInstance(KrunchyContext p_Context)
    {
        if (s_Instance == null)
        {
            s_Instance = new KrunchyStateInitCheckingSafety(p_Context);
        }
    }

    /**
     * return the one and only instance of this state class
     */
    public static KrunchyStateInitCheckingSafety getInstance()
    {
        return s_Instance;
    }

    @Override
    public void doIt()
    {
        // TODO: use worker thread and async handling
        // getContext().waitFor(enumKeySwitchOn);

        // no cover switch installed, use the keyswitch instead
        boolean bCoverClosed = getGuardManager().isCoverClosed();

        if (bCoverClosed)
        {
            getContext().setButtonLedOff();
            changeState(KrunchyStateInitHomingCruncherAxis.getInstance());
        }
        else
        {
            getContext().setButtonLedForCoverOpen();

            boolean bStayInLoop = true;
            while(bStayInLoop)
            {
                getContext().threadSleepMillis(250);
                bStayInLoop = !getGuardManager().isCoverClosed();
            }

            getContext().setButtonLedOff();
            changeState(KrunchyStateInitCheckingSafety.getInstance());
        }
    }


    @Override
    public void enter()
    {
        // nothing to do on enter so far
    }

    @Override
    public void exit()
    {
        // nothing to do on exit so far
    }

}
