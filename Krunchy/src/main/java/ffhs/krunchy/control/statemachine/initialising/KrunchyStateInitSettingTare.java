/*
 * Copyright (c) 2018 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */
package ffhs.krunchy.control.statemachine.initialising;


import ffhs.krunchy.control.KrunchyContext;
import ffhs.krunchy.control.statemachine.running.KrunchyStateRunIdle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  State of the Krunchy initialisation statemacine
 *
 *
 */
public class KrunchyStateInitSettingTare extends KrunchyStateInit
{
    private final Logger LOGGER = LoggerFactory.getLogger(KrunchyContext.class);

    private static KrunchyStateInitSettingTare s_Instance = null;

    private KrunchyStateInitSettingTare(KrunchyContext p_Context)
    {
        super(p_Context);
        m_sStateName = "KrunchyStateInitSettingTare";
    }

    /**
     * create an instance of this state class if it is not instantiated yet
     *
     * @param p_Context the underlying context to be stored in the super class
     */
    public static void createInstance(KrunchyContext p_Context)
    {
        if (s_Instance == null)
        {
            s_Instance = new KrunchyStateInitSettingTare(p_Context);
        }
    }

    /**
     * return the one and only instance of this state class
     */
    public static KrunchyStateInitSettingTare getInstance()
    {
        return s_Instance;
    }

    @Override
    public void doIt()
    {
        getContext().tareForceMeasurement();

        if (getContext().getGuardManager().isErrorPending())
        {
            getContext().changeState(KrunchyStateInitError.getInstance());
        }
        else
        {
            getContext().changeState(KrunchyStateRunIdle.getInstance());
        }
    }


    @Override
    public void enter()
    {
        // nothing to do on enter so far
    }

    @Override
    public void exit()
    {
        // nothing to do on exit so far
    }

}
