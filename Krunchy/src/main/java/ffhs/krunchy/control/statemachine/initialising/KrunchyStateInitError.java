/*
 * Copyright (c) 2018 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */
package ffhs.krunchy.control.statemachine.initialising;

import com.diozero.util.SleepUtil;
import ffhs.krunchy.control.KrunchyContext;
import ffhs.krunchy.control.statemachine.KrunchyStateStarting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  State of the Krunchy initialisation statemacine
 *  end-up here if a init task could not be completed
 *
 */
public class KrunchyStateInitError extends KrunchyStateInit
{
    private final Logger LOGGER = LoggerFactory.getLogger(KrunchyContext.class);

    private static KrunchyStateInitError s_Instance = null;

    private KrunchyStateInitError(KrunchyContext p_Context)
    {
        super(p_Context);
        m_sStateName = "KrunchyStateInitError";
    }

    /**
     * create an instance of this state class if it is not instantiated yet
     *
     * @param p_Context the underlying context to be stored in the super class
     */
    public static void createInstance(KrunchyContext p_Context)
    {
        if (s_Instance == null)
        {
            s_Instance = new KrunchyStateInitError(p_Context);
        }
    }

    /**
     * return the one and only instance of this state class
     */
    public static KrunchyStateInitError getInstance()
    {
        return s_Instance;
    }

    @Override
    public void doIt()
    {
        // define what to do in case of an error e.g.
        // - do extensive logging
        // - blink a certain code
        // - wait for a certain button code to restart the application

        boolean bStartButtonPressed = getGuardManager().isStartButtonPressed();
        boolean bStopButtonPressed = getGuardManager().isStopButtonPressed();

        if (bStartButtonPressed && bStopButtonPressed)
        {
            changeState(KrunchyStateStarting.getInstance());
        }
        else
        {
            SleepUtil.sleepSeconds(1);
            changeState(KrunchyStateInitError.getInstance());
        }
    }


    @Override
    public void enter()
    {
        // nothing to do on enter so far
    }

    @Override
    public void exit()
    {
        // nothing to do on exit so far
    }

}
