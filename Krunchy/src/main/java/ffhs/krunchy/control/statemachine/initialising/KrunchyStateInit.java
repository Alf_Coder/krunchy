/*
 * Copyright (c) 2018 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */
package ffhs.krunchy.control.statemachine.initialising;

import ffhs.krunchy.control.KrunchyContext;
import ffhs.krunchy.control.statemachine.KrunchyState;

/**
 * Acts as the super class of all state classes in the sub-statemachine initialising
 * collects all commons for those states
 */
public abstract class KrunchyStateInit extends KrunchyState
{
    public KrunchyStateInit(KrunchyContext p_Context)
    {
        super(p_Context);
    }
}
