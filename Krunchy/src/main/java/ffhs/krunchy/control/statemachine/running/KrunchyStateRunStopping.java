/*
 * Copyright (c) 2018 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */
package ffhs.krunchy.control.statemachine.running;

import ffhs.krunchy.control.KrunchyContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class KrunchyStateRunStopping extends KrunchyStateRun
{
    private final Logger LOGGER = LoggerFactory.getLogger(KrunchyContext.class);

    private static KrunchyStateRunStopping s_Instance = null;

    private KrunchyStateRunStopping(KrunchyContext p_Context)
    {
        super(p_Context);
        m_sStateName = "KrunchyStateRunStopping";
    }

    /**
     * create an instance of this state class if it is not instantiated yet
     *
     * @param p_Context the underlying context to be stored in the super class
     */
    public static void createInstance(KrunchyContext p_Context)
    {
        if (s_Instance == null)
        {
            s_Instance = new KrunchyStateRunStopping(p_Context);
        }
    }

    /**
     * return the one and only instance of this state class
     */
    public static KrunchyStateRunStopping getInstance()
    {
        return s_Instance;
    }


    @Override
    public void doIt()
    {
        boolean bStartButtonPressedAtStartup = getContext().getGuardManager().isStartButtonPressed();

        if (bStartButtonPressedAtStartup)
        {
            //getContext().changeState(KrunchyStateDiag.getInstance());
        }
        else
        {
            //getContext().changeState(KrunchyStateInit.getInstance());
        }
    }


    @Override
    public void enter()
    {
        // nothing to do on enter so far
    }

    @Override
    public void exit()
    {
        // nothing to do on exit so far
    }

}
