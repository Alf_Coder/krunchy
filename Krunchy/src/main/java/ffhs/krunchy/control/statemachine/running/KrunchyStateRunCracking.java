/*
 * Copyright (c) 2018 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.control.statemachine.running;


import ffhs.krunchy.control.KrunchyContext;
import ffhs.krunchy.synchronisation.SynchObjectCompletion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class KrunchyStateRunCracking extends KrunchyStateRun
{
    private final Logger LOGGER = LoggerFactory.getLogger(KrunchyContext.class);

    private static KrunchyStateRunCracking s_Instance = null;

    private KrunchyStateRunCracking(KrunchyContext p_Context)
    {
        super(p_Context);
        m_sStateName = "KrunchyStateRunCracking";
    }

    /**
     * create an instance of this state class if it is not instantiated yet
     *
     * @param p_Context the underlying context to be stored in the super class
     */
    public static void createInstance(KrunchyContext p_Context)
    {
        if (s_Instance == null)
        {
            s_Instance = new KrunchyStateRunCracking(p_Context);
        }
    }

    /**
     * return the one and only instance of this state class
     */
    public static KrunchyStateRunCracking getInstance()
    {
        return s_Instance;
    }


    @Override
    public void doIt()
    {
        getContext().setButtonLedStoppableRun();;

        getContext().tareForceMeasurement();

        getContext().setStopButtonActive();

        SynchObjectCompletion crunchCompleted = getContext().doCrackCycle();

        if (getContext().waitingFor(crunchCompleted))
        {
            LOGGER.error("doCrackCycle finished");
            getContext().changeState(KrunchyStateRunReleasing.getInstance());
        }
        else
        {
            boolean bTestingSignalled = crunchCompleted.IsSignalled();
            assert !bTestingSignalled : "synch objects not signalled...";

            // distinguish between different error cases
            String sErrorType = crunchCompleted.getErrorType();
            LOGGER.error("doCrackCycle finished with error:\t {}", sErrorType);

            if (sErrorType.equals("Stopped by User"))
            {
                // user pressed shutdown button, it's ok to continue as if it is finished
                getContext().changeState(KrunchyStateRunReleasing.getInstance());
            }
            else if (sErrorType.equals("Max force reached"))
            {
                // maximum force reached, let it stay and inspect
                getContext().changeState(KrunchyStateRunError.getInstance());
            }
            else if (sErrorType.equals("Max position reached"))
            {
                // maximum position reached, let it stay and inspect
                getContext().changeState(KrunchyStateRunError.getInstance());
            }
            else
            {
                assert false : "error type not yet handled: " + sErrorType;
            }

        }

    }


    @Override
    public void enter()
    {
        // nothing to do on enter so far
    }

    @Override
    public void exit()
    {
        // nothing to do on exit so far
    }

}
