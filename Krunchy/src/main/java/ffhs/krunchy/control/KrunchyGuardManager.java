/*
 * Copyright (c) 2018 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.control;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Andy on 25.11.2018.
 * @author andreas.frommel
 * @version 1.0
 * @updated 30-Nov-2018 05:58:22
 */
public class KrunchyGuardManager
{
    private final Logger LOGGER = LoggerFactory.getLogger(KrunchyContext.class);

    private KrunchyContext m_Context;
	public KrunchyContext m_KrunchyContext;

    public KrunchyGuardManager(KrunchyContext p_Context)
    {
        m_Context = p_Context;
    }


    public boolean isStartButtonPressed()
    {
        boolean bStartButtonPressed = m_Context.getStartButton().isActive();
        LOGGER.error("isStartButtonPressed\t {}", bStartButtonPressed);
        return bStartButtonPressed;
    }

    public boolean isStopButtonPressed()
    {
        boolean bStopButtonPressed = m_Context.getStopButton().isActive();
        LOGGER.error("isStopButtonPressed\t {}", bStopButtonPressed);
        return bStopButtonPressed;
    }


    public boolean isKeySwitchOn()
    {
        // the key switch is used as the cover switch
        m_Context.getCoverSwitch();

        LOGGER.error("isKeySwitchOn\t {}", "TODO: still a stub");

        return true;
    }

    public boolean isCoverClosed()
    {
        boolean bCoverIsClosed = m_Context.getCoverSwitch().isActive();
        LOGGER.error("isCoverClosed\t {}", bCoverIsClosed);
        return bCoverIsClosed;
    }

    public boolean isErrorPending()
    {
        // hardware error detection seems to be difficult with the installed hardware

        // check stepper driver
        // TODO: how to check the correct functionality automatically in this state
        // could be ok if communication shows promising results

        // check a/d converter if enabled in properties
        // TODO: how to check the correct functionality automatically in this state

        // check rgb led
        // TODO: they cannot be tested by software alone

        // check illuminated buttons
        // TODO: the leds cannot be tested and the buttons could be pressed by user

        LOGGER.error("isErrorPending\t {}", "always OK");

        return false;
    }

    public boolean isCruncherReleased()
    {
        boolean bCruncherIsUnsafe = m_Context.isCruncherBelowSensor();

        LOGGER.error("isCruncherReleased\t {}", !bCruncherIsUnsafe);

        return !bCruncherIsUnsafe;
    }
}
