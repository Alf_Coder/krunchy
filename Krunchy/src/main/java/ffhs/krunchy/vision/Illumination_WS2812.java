/*
 * Copyright (c) 2019 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.vision;

import com.diozero.ws281xj.LedDriverInterface;
import com.diozero.ws281xj.rpiws281x.WS281x;
import ffhs.krunchy.control.KrunchyConfig;
import ffhs.krunchy.motion.KrunchySpeedController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Illumination_WS2812  extends Thread
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Illumination_WS2812.class);

    private static LedDriverInterface m_LedDriver;

    private KrunchySpeedController  m_SpeedController;

    private int m_iPinNumber;
    private int m_iNrOfLeds;
    private int m_iBrightness;

    private int m_iMinSpeed;
    private int m_iMaxSpeed;
    private int m_iMaxForce;

    protected volatile boolean m_bKeepRunning = true;
    protected volatile boolean m_bCrackDetected = false;

    public Illumination_WS2812(KrunchySpeedController p_SpeedController)
    {
        super("WS2812 Illumination Controller 1");
        m_SpeedController = p_SpeedController;

        LOGGER.debug("Illumination_WS2812 ctor enter");

        readConfiguration();
        createHardware();
        activateHardware();

        startUpTest();

        LOGGER.info("StepperDriverL6470 ctor exit");
    }


    @Override
    public void finalize()
    {
        // try to shutdown what has been started

        LOGGER.error("{} object gets deleted", getName());
    }


    private void readConfiguration()
    {
        LOGGER.debug("StepperDriverL6470 readConfiguration enter");

        // hardware related values
        m_iPinNumber  = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.LedRGB.GPIO.Pin.DataOut"));
        m_iNrOfLeds   = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.LedRGB.NumberOfLeds"));
        m_iBrightness = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.LedRGB.Brightness"));
        m_iMinSpeed   = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Speed.Min"));
        m_iMaxSpeed   = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Speed.Max"));
        m_iMaxForce   = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Force.Max"));

        LOGGER.debug("StepperDriverL6470 readConfiguration exit");
    }

    private void createHardware()
    {
        m_LedDriver = new WS281x(m_iPinNumber, m_iBrightness, m_iNrOfLeds);
    }


    private void activateHardware()
    {
        m_LedDriver.allOff();
        threadSleepMillis(250);
    }



    /**
     * in case of an ordinary shutdown of the Krunchy this will shutdown the thread
     */
    public void shutdown()
    {
        LOGGER.info("stopping thread {}", getName());
        m_bKeepRunning = false;

    }






    /**
     *  the running core of the thread
     */
    @Override
    public void run()
    {
        LOGGER.info("{} started", getName());

        // thread run preparation
        int iRedComponent   = 0;
        int iGreenComponent = 0;
        int iBlueComponent  = 0;

        //int iSpeedRange = m_iMaxSpeed - m_iMinSpeed;
        int iForceStep  = m_iMaxForce / 256; // how many grams for one brightness increment


        // the tread loop
        while(m_bKeepRunning)
        {
            // TODO: check for division by zero possibilities

            // get current speed
            int iCurrentSpeed = m_SpeedController.getCurrentColorSpeed();
            int iCurrentForce = m_SpeedController.getLastForceMeasured();

            //------------------------------------------------------------
            // calculate current speed color
            int iCurrentSpeedColor = 0;
            if (iCurrentSpeed > 0)
            {
                // up movement; use blue with trend to cyan as it goes faster
                iRedComponent   = 0;
                iGreenComponent = (iCurrentSpeed * 255) / m_iMaxSpeed; // max speed = cyan
                iBlueComponent  = 255;
                iCurrentSpeedColor = (iRedComponent << 16) + (iGreenComponent << 8) + iBlueComponent;
            }
            else
            {
                // down movement; use yellow with trend to red as it goes slower
                iRedComponent   = 255;
                iGreenComponent = (255 * -iCurrentSpeed) / m_iMaxSpeed; // max speed = yellow
                iBlueComponent  = 0;
                iCurrentSpeedColor = (iRedComponent << 16) + (iGreenComponent << 8) + iBlueComponent;
            }
            LOGGER.debug("SpeedColor {}", String.format("\t%06X", iCurrentSpeedColor));
            LOGGER.debug("SpeedColor Red: {} Green: {} Blue: {}", String.format("\t%2X", iRedComponent), String.format("\t%2X", iGreenComponent), String.format("\t%2X", iBlueComponent));

            //------------------------------------------------------------
            // calculate speed blinker rate

            // TODO: blink speed at min speed


            //------------------------------------------------------------
            // calculate current force color
            iRedComponent   = (iCurrentForce / iForceStep);     // max force = max red
            iGreenComponent = 255 - (iCurrentForce / iForceStep); // zero force = max green
            iBlueComponent  = 0;
            int iCurrentForceColor = (iRedComponent << 16) + (iGreenComponent << 8) + iBlueComponent;

            LOGGER.debug("ForceColor {}", String.format("\t%06X", iCurrentForceColor));
            LOGGER.debug("ForceColor Red: {} Green: {} Blue: {}", String.format("\t%2X", iRedComponent), String.format("\t%2X", iGreenComponent), String.format("\t%2X", iBlueComponent));


            // Crack detection flasher TODO: implement it
            if (m_bCrackDetected)
            {
                // do some flashing in case a crack is detected
            }

            //------------------------------------------------------------
            // pass the force color to the LED
            m_LedDriver.setPixelColour(0, iCurrentForceColor);

            // pass the speed color to the LED
            m_LedDriver.setPixelColour(1, iCurrentSpeedColor);

            m_LedDriver.render();
            threadSleepMillis(125);


        }


        // thread is being stopped, close the controller
        LOGGER.info("{} running loop ended", getName());
        m_LedDriver.allOff();
        m_LedDriver.close();
    }











    public void startUpTest()
    {
        LOGGER.debug("startUpTest\t {}", "two LED");


        m_LedDriver.setPixelColour(0, 0xFF0000);
        m_LedDriver.render();
        threadSleepMillis(250);

        m_LedDriver.setPixelColour(1, 0x0000FF);
        m_LedDriver.render();
        threadSleepMillis(250);

        m_LedDriver.setPixelColour(0, 0x00FF00);
        m_LedDriver.render();
        threadSleepMillis(250);

        m_LedDriver.setPixelColour(1, 0xFF0000);
        m_LedDriver.render();
        threadSleepMillis(250);

        m_LedDriver.setPixelColour(0, 0x0000FF);
        m_LedDriver.render();
        threadSleepMillis(250);

        m_LedDriver.setPixelColour(1, 0x00FF00);
        m_LedDriver.render();
        threadSleepMillis(250);

        m_LedDriver.setPixelColour(0, 0x7F7F7F);
        m_LedDriver.render();
        threadSleepMillis(250);

        m_LedDriver.setPixelColour(1, 0x7F7F7F);
        m_LedDriver.render();
        threadSleepMillis(250);

        m_LedDriver.setPixelColour(0, 0x404040);
        m_LedDriver.render();
        threadSleepMillis(250);

        m_LedDriver.setPixelColour(1, 0x404040);
        m_LedDriver.render();
        threadSleepMillis(250);

        m_LedDriver.setPixelColour(0, 0x202020);
        m_LedDriver.render();
        threadSleepMillis(250);

        m_LedDriver.setPixelColour(1, 0x202020);
        m_LedDriver.render();
        threadSleepMillis(250);

        m_LedDriver.setPixelColour(0, 0x007F00); // green
        m_LedDriver.setPixelColour(1, 0x7F007F); // red & blue = purple
        m_LedDriver.render();
    }















    public void threadSleepMillis(int p_iMilliSeconds)
    {
        try
        {
            Thread.sleep(p_iMilliSeconds);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

}
