/*
 * Copyright (c) 2019 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.force;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *   superclass of the loadcell readers
 *   provide basic functionality of the reader threads
 */
public abstract class LoadcellReader extends Thread
{
    protected final Logger LOGGER = LoggerFactory.getLogger(LoadcellReader.class);

    protected volatile int m_iCurrentValue = 0;
    protected volatile boolean m_bKeepRunning = true;

    // a simple moving average is used to reduce the noise of the readings
    protected int[]   m_iValueStorage;
    protected long    m_lSumOfValues = 0;
    protected int     m_iNrOfValues = 0;
    protected int     m_iStorageLOcation = 0;


    /**
     * in case of an ordinary shutdown of the Krunchy this will shutdown the thread
     */
    public void stopThread()
    {
        LOGGER.info("stopping thread {}", getName());
        m_bKeepRunning = false;
    }


    /**
     *  set the latest averaged value for the outside of this thread
     *     only this thread should write into this as the single writer
     * @param p_iNewValue the latest value from the moving average
     */
    protected void setCurrentValue(int p_iNewValue)
    {
        m_iCurrentValue = p_iNewValue;

        //LOGGER.debug("setCurrentValue {}", String.format("\t%10X", m_iCurrentValue));
    }


    /**
     *  return the latest averaged conversion value
     *    this is a shared resource but with a single writer it is safe with a volatile member
     *
     * @return the conversion value
     */
    public int getCurrentValue()
    {
        return m_iCurrentValue;
    }



    /**
     *  this initialises the simple moving average algorithm with an initial value
     * @param p_iInitialValue  the value to fill the storage
     */
    protected void initMovingAverage(int p_iInitialValue)
    {
        LOGGER.info("initMovingAverage({})", p_iInitialValue);

        m_iValueStorage    = new int[m_iNrOfValues];
        m_iStorageLOcation = 0;
        m_lSumOfValues     = 0;

        // fill the storage with the initial value
        for (int iIndex = 0; iIndex < m_iNrOfValues; iIndex++)
        {
            m_iValueStorage[iIndex] = p_iInitialValue;
        }
        m_lSumOfValues = m_iNrOfValues * p_iInitialValue;
    }


    /**
     *  storage management of the moving average output of the converter
     *
     * @param p_iNewValue the next value to add to the storage
     * @return the latest average value including the new value
     */
    protected int nextAverageValue(int p_iNewValue)
    {
        // remove the oldest value from the sum
        m_lSumOfValues = m_lSumOfValues - m_iValueStorage[m_iStorageLOcation];

        // store the newest value at the location of the oldest value
        m_iValueStorage[m_iStorageLOcation] = p_iNewValue;

        // add the newest value to the sum
        m_lSumOfValues = m_lSumOfValues + p_iNewValue;

        // shift the storage location pointer
        m_iStorageLOcation = (m_iStorageLOcation + 1) % m_iNrOfValues;

        return (int) m_lSumOfValues / m_iNrOfValues;
    }

}
