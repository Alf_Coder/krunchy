/*
 * Copyright (c) 2019 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.force;

import com.diozero.api.DigitalOutputDevice;
import com.diozero.api.GpioEventTrigger;
import com.diozero.api.GpioPullUpDown;
import com.diozero.api.WaitableDigitalInputDevice;
import com.diozero.util.SleepUtil;

/**
 *  the class LoadcellReaderHX711 defines the functionality of the reading thread for the loadcell
 *  by controlling a HX711 A/D converter chip
 *  the resulting value is provided to the other threads
 *
 */
public class LoadcellReaderHX711 extends LoadcellReader
{
    private static final int HX711_RESOLUTION_NR_OF_BITS = 24;
    private static final int HX711_MIN_VALUE             = -8388608;  // 24 bits limit with 2's complement
    private static final int HX711_MAX_VALUE             =  8288607;  // i.e. FF800000..007FFFFFF

    private WaitableDigitalInputDevice m_pinHx711Data;  // the output pin of the converter is a input pin on the Raspi
    private DigitalOutputDevice m_pinHx711Clock;        // the input pin of the converter is a output pin on the Raspi


    /**
     *  construction of the reader thread
     *
     * @param p_sThreadName            the explicit name for better debug information
     * @param p_iClockPin              the GPIO pin to clock the data out
     * @param p_iDataPin               the GPIO pin to read the data bits
     * @param p_iMovingAverageSize     the number of values to be averaged
     */
    public LoadcellReaderHX711(String p_sThreadName, int p_iClockPin, int p_iDataPin, int p_iMovingAverageSize)
    {
        setName(p_sThreadName);

        m_pinHx711Data  = new WaitableDigitalInputDevice(p_iDataPin, GpioPullUpDown.PULL_DOWN, GpioEventTrigger.RISING);
        m_pinHx711Clock = new DigitalOutputDevice(p_iClockPin, true, false);

        m_iNrOfValues = p_iMovingAverageSize;
    }


    /**
     *  the running core of the thread
     */
    @Override
    public void run()
    {
        LOGGER.debug("started");

        initMovingAverage(0); // could be done with a real value from sensor

        // thread started, do some dummy reads since the HX711 shows a strange behavior after
        // startup: some FFFFFF readings must be skipped
        for (int iLoop = 0 ; iLoop < 10; iLoop++)
        {
            int iValue = readRawBits();

            LOGGER.debug("dummy read: {} ", String.format("\t%10X", iValue));

            if (isValidData(iValue))
            {
                int iNextValue = nextAverageValue(iValue);
                LOGGER.debug("fill moving average: {} ", String.format("\t%10X", iNextValue));
            }
        }

        // the tread loop
        while(m_bKeepRunning)
        {
            try
            {
                // wait for a conversion to be finished and ready to get read
                m_pinHx711Data.waitForInactive();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }

            // then read the data
            int iNewValue = readRawBits();

            // filter out odd values
            if (isValidData(iNewValue))
            {
                LOGGER.debug("store value {}", iNewValue);
                int iNextValue = nextAverageValue(iNewValue);
                setCurrentValue(iNextValue);
            }
        }

        // thread is being stopped, shutdown the converter
        LOGGER.info("{} running loop ended", getName());
        powerDown();
    }

    /**
     * check if the given data is a valid reading from the HX711
     * sometimes the converter provides odd data like all '0' or all '1'
     *
     * @param p_iData  the data to be checked
     * @return  true if the data meets the requirements
     */
    private boolean isValidData(int p_iData)
    {
        // check all bits to zeros which is hard to get,
        // so most probably a bad read
        boolean bAllZeros = p_iData == 0x00000000;

        // check the lower 22 bits to be all ones which is hard to get,
        // so most probably a bad read -> NOT(data) AND 3FFFFF
        boolean bTooManyOnes = (~p_iData & 0x003FFFFF) == 0;

        boolean bDataIsValid = (p_iData > HX711_MIN_VALUE) && (p_iData < HX711_MAX_VALUE) && !bAllZeros && !bTooManyOnes;
        return bDataIsValid;
    }


    /**
     * Return true if the converter is ready for data transfer
     *   ref. HX711 datasheet:
     *   When output data is not ready for retrieval, digital output pin DOUT is high.
     *   Serial clock input PD_SCK should be low (keep it power-up).
     *   When DOUT goes to low, it indicates data is ready for retrieval.
     *
     * @return boolean
     */
    private boolean isReady()
    {
        return !m_pinHx711Data.isActive();
    }


    /**
     * Set the HX711 into power safe mode
     *   ref. HX711 datasheet:
     *   Pin PD_SCK input is used to power down the HX711.
     *   When PD_SCK Input is low, chip is in normal working mode.
     *   When PD_SCK pin changes from low to high and stays at high for longer than 60μs,
     *   HX711 enters power down mode.
     *   When PD_SCK returns to low, chip will reset and enter normal operation mode.
     */
    private void powerDown()
    {
        LOGGER.info("HX711 converter power down");
        m_pinHx711Clock.off();
        m_pinHx711Clock.on();
        SleepUtil.sleepMicros(100); // longer than 60
    }


    /**
     * Wake-up the HX711 from power safe mode
     *   ref. HX711 datasheet:
     *   When PD_SCK returns to low, chip will reset and enter normal operation mode.
     */
    private void powerUp()
    {
        m_pinHx711Clock.off();
    }


    /**
     * Resetting the HX711 is performed by cycle the power mode
     */
    public void resetConverter()
    {
        powerDown();
        powerUp();
    }


    /**
     * Read the current conversion result from the HX711 and return it as a
     * int number: 4 bytes i.e. signed 32 bits long
     *
     * @return int the raw data read from the converter
     */
    private int readRawBits()
    {

        int iRawBits = 0;

        // waiting for the converter to signal it has data ready for transmission
        while (!isReady()) ; // how to avoid an endless loop in case of defect?!

        // fill the bits into the int var, MSBit comes in first
        for (int iBitNumber = HX711_RESOLUTION_NR_OF_BITS - 1; iBitNumber >= 0; iBitNumber--)
        {
            // get the n-th bit at the data output. use the same command as an additional delay
            // of some more nano seconds due to the signal conditioning propagation delay
            for (int iDelayLoop = 0 ; iDelayLoop < 10; iDelayLoop++)
            {
                m_pinHx711Clock.on();
            }

            // spend the pulse duration time to shift the bits before adding the new bit as
            // least significant bit on pos 0
            iRawBits = iRawBits << 1;

            // by now the data output of the converter should have the bit presented, read it
            if (m_pinHx711Data.isActive())
            {
                iRawBits = iRawBits + 1; // add a '1' bit as the current LSB
            }

            for (int iDelayLoop = 0 ; iDelayLoop < 4; iDelayLoop++)
            {
                m_pinHx711Clock.off();
            }
        }

        // the current read cycle must also configure the mode for the next conversion
        // add one clock pulse without read, the data pin should be 1
        m_pinHx711Clock.on();

        // perform number conversions during the additional clock pulse
        // the HX711 returns 2s complement numbers i.e. fill the remaining bits for sign correctness
        int iSignBit = 1 << (HX711_RESOLUTION_NR_OF_BITS - 1); // with 24bits this would result in 00000000 10000000 00000000 00000000

        boolean bIsNegativeNumber = (iRawBits & iSignBit) > 0;
        if (bIsNegativeNumber)
        {
            // the captured value is negative, fill the leading bits with 1's
            int iNegativeSignMask = -2 << (HX711_RESOLUTION_NR_OF_BITS - 1);
            // -2 is e.g. 11111111 11111111 11111111 11111110
            // shift to   11111111 00000000 00000000 00000000

            iRawBits = iRawBits | iNegativeSignMask;
            // e.g. from  00000000 1xxxxxxx xxxxxxxx xxxxxxxx
            //      to    11111111 1xxxxxxx xxxxxxxx xxxxxxxx
        }

        // now the final clock pulse ends
        m_pinHx711Clock.off();

         LOGGER.debug("readRawBits {}", String.format("\t%10X", iRawBits));
        return iRawBits;
    }

}
