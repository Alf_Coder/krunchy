/*
 * Copyright (c) 2019 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.force;

import ffhs.krunchy.control.KrunchyConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  the class KrunchyForceMeasurement manages the measurement of the force applied to the two
 *  loadcells and provides the the force in grams to the other threads
 */
public class KrunchyForceMeasurement
{
    private final Logger LOGGER = LoggerFactory.getLogger(KrunchyForceMeasurement.class);

    private LoadcellReader[]      m_Converter    = {null, null};
    private int[]                 m_iTare        = {0,    0};
    private double[]              m_dbGramFactor = {1.0,  1.0};

    public KrunchyForceMeasurement()
    {
        boolean bConverter1Enabled = Boolean.parseBoolean(KrunchyConfig.getInstance().getValue("Krunchy.HX711.Controller1.Enabled"));
        boolean bConverter2Enabled = Boolean.parseBoolean(KrunchyConfig.getInstance().getValue("Krunchy.HX711.Controller2.Enabled"));
        int     iMovingAverageSize = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.HX711.Controller.AverageSize"));

        // get the calibration values of the cells
        double dbFactor = Double.parseDouble(KrunchyConfig.getInstance().getValue("Krunchy.HX711.Controller1.ScaleFactor"));
        if ((int)dbFactor != 0)
        {
            // must be <> 0 to avoid division by zero
            m_dbGramFactor[0] = dbFactor;
        }
        dbFactor = Double.parseDouble(KrunchyConfig.getInstance().getValue("Krunchy.HX711.Controller2.ScaleFactor"));
        if ((int)dbFactor != 0)
        {
            m_dbGramFactor[1] = dbFactor;
        }

        // prepare the first A/D channel
        if (bConverter1Enabled)
        {
            // only start a thread if the converter is used
            int iClockPin = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.HX711.Controller1.GPIO.Pin.Clock"));
            int iDataPin  = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.HX711.Controller1.GPIO.Pin.Data"));
            m_Converter[0] = new LoadcellReaderHX711("Force Converter 1", iClockPin, iDataPin, iMovingAverageSize);
        }

        // prepare the second A/D channel
        if (bConverter2Enabled)
        {
            // only start a thread if the converter is used
            int iClockPin = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.HX711.Controller2.GPIO.Pin.Clock"));
            int iDataPin  = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.HX711.Controller2.GPIO.Pin.Data"));
            m_Converter[1] = new LoadcellReaderHX711("Force Converter 2", iClockPin, iDataPin, iMovingAverageSize);
        }

        // start all prepared threads
        for (int iThread = 0; iThread < m_Converter.length; iThread++)
        {
            if (m_Converter[iThread] != null)
            {
                m_Converter[iThread].start();
            }
        }
    }


    /**
     *   shutdown the running threads
     */
    public void shutdown()
    {
        for (int iThreadNr = 0; iThreadNr < m_Converter.length; iThreadNr++)
        {
            if (m_Converter[iThreadNr] != null)
            {
                if (m_Converter[iThreadNr].isAlive())
                {
                    m_Converter[iThreadNr].stopThread();
                }
            }
        }
    }


    /**
     *  store the current force as a zero-offset for future values
     */
    public void tare()
    {
        for (int iThread = 0; iThread < m_Converter.length; iThread++)
        {
            int iTaraValue = getCurrentValue(iThread);
            m_iTare[iThread] = iTaraValue;
            LOGGER.debug("tare value: {}", iTaraValue);
        }
    }


    /**
     *  Return the current force applied to the weightcells
     *  corrected by the tare values and the gram-scale factors
     * @return
     */
    public int getCurrentForce()
    {
        int iForce1 = getCurrentForce(0);
        int iForce2 = getCurrentForce(1);

        if (iForce1 < 0)
        {
            // filter illegal values
            iForce1 = 0;
        }
        if (iForce2 < 0)
        {
            // filter illegal values
            iForce2 = 0;
        }

        return iForce1 + iForce2;
    }


    /**
     *  Return the current force applied to the loadcells with the given number
     *  corrected by the tare value and the gram-scale factor
     *  if the converter is not active, use the other one
     *  if both are not active return zero
     *
     * @param p_iChannelNumber  the number of the channel, zero based
     * @return the current force of the channel
     */
    private int getCurrentForce(int p_iChannelNumber)
    {
        double dbValue = 0.0;

        int iChannelNumber = p_iChannelNumber;
        if (m_Converter[iChannelNumber] == null)
        {
            // the converter is not running, use the other
            iChannelNumber = (iChannelNumber + 1) ^ 2; // use modulo 2 to switch to the other
        }
        int iValueRead  = getCurrentValue(iChannelNumber);
        int iValueTare  = m_iTare[iChannelNumber];
        double dbFactor = m_dbGramFactor[iChannelNumber];

        dbValue = (iValueRead - iValueTare) / dbFactor;

        LOGGER.debug("getCurrentForce({}): {} = ({} - {}) / {}", p_iChannelNumber, dbValue, iValueRead, iValueTare, dbFactor);
        return (int) dbValue;
    }

    /**
     *  Return the current raw value from the loadcell with the given number
     *  return zero if the converter is not active
     *
     * @return the current reading of the channel
     */
    private int getCurrentValue(int p_iChannelNumber)
    {
        int iValue = 0;
        if (m_Converter[p_iChannelNumber] != null)
        {
            iValue = m_Converter[p_iChannelNumber].getCurrentValue();
        }
        return iValue;
    }

}
