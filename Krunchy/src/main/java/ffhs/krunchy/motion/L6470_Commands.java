/*
 * Copyright (c) 2019 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.motion;

public class L6470_Commands
{
    public static final byte L6470_CMD_NOP          = (byte) 0x00;

    public static final byte L6470_CMD_GET_PARAM    = (byte) 0x20; // + register 01..1B
    public static final byte L6470_CMD_SET_PARAM    = (byte) 0x00; // + register 01..1B

    public static final byte L6470_CMD_GETSTATUS    = (byte) 0xD0;

    public static final byte L6470_CMD_RESET_DEVICE = (byte) 0xC0;
    public static final byte L6470_CMD_RESET_POS    = (byte) 0xD8;

    public static final byte L6470_CMD_GOTO         = (byte) 0x60;
    public static final byte L6470_CMD_RUN          = (byte) 0x50;
    public static final byte L6470_CMD_MOVE         = (byte) 0x40;
    public static final byte L6470_CMD_GOTO_DIR     = (byte) 0x68;
    public static final byte L6470_CMD_GO_UNTIL     = (byte) 0x82;
    public static final byte L6470_CMD_GO_HOME      = (byte) 0x70;
    public static final byte L6470_CMD_GO_MARK      = (byte) 0x78;

    public static final byte L6470_CMD_STEP_CLOCK   = (byte) 0x58;
    public static final byte L6470_CMD_RELEASE_SW   = (byte) 0x92;
    public static final byte L6470_CMD_SOFT_STOP    = (byte) 0xB0;
    public static final byte L6470_CMD_HARD_STOP    = (byte) 0xB8;
    public static final byte L6470_CMD_SOFT_HIZ     = (byte) 0xA0;
    public static final byte L6470_CMD_HARD_HIZ     = (byte) 0xA8;
}
