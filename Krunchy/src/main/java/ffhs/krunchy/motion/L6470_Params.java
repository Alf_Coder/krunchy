/*
 * Copyright (c) 2019 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */
package ffhs.krunchy.motion;


public class L6470_Params
{
    public static final byte L6470_PARAM_ABS_POS    = 0x01;
    public static final byte L6470_PARAM_EL_POS     = 0x02;
    public static final byte L6470_PARAM_MARK       = 0x03;
    public static final byte L6470_PARAM_SPEED      = 0x04;
    public static final byte L6470_PARAM_ACC        = 0x05;
    public static final byte L6470_PARAM_DEC        = 0x06;
    public static final byte L6470_PARAM_MAX_SPEED  = 0x07;
    public static final byte L6470_PARAM_MIN_SPEED  = 0x08;
    public static final byte L6470_PARAM_KVAL_HOLD  = 0x09;
    public static final byte L6470_PARAM_KVAL_RUN   = 0x0A;
    public static final byte L6470_PARAM_KVAL_ACC   = 0x0B;
    public static final byte L6470_PARAM_KVAL_DEC   = 0x0C;
    public static final byte L6470_PARAM_INT_SPD    = 0x0D;
    public static final byte L6470_PARAM_ST_SLP     = 0x0E;
    public static final byte L6470_PARAM_FN_SLP_ACC = 0x0F;
    public static final byte L6470_PARAM_FN_SLP_DEC = 0x10;
    public static final byte L6470_PARAM_K_THERM    = 0x11;
    public static final byte L6470_PARAM_ADC_OUT    = 0x12;
    public static final byte L6470_PARAM_OCD_TH     = 0x13;
    public static final byte L6470_PARAM_STALL_TH   = 0x14;
    public static final byte L6470_PARAM_FS_SPD     = 0x15;
    public static final byte L6470_PARAM_STEP_MODE  = 0x16;
    public static final byte L6470_PARAM_ALARM_EN   = 0x17;
    public static final byte L6470_PARAM_CONFIG     = 0x18;
    public static final byte L6470_PARAM_STATUS     = 0x19;
}
