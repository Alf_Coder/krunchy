/*
 * Copyright (c) 2019 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.motion;

import ffhs.krunchy.control.KrunchyConfig;
import ffhs.krunchy.force.KrunchyForceMeasurement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class KrunchySpeedController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(KrunchySpeedController.class);

    private KrunchyForceMeasurement m_ForceMeasurement;

    private int m_iMinSpeed;
    private int m_iMaxSpeed;
    private int m_iMaxForce;
    private int m_iForceToReachMinSpeed;

    private double m_dbForceEquSlope;

    private int m_iLastForceMeasured;
    private int m_iLastSpeedReturned;
    private boolean m_bUpMovement = true;

    public KrunchySpeedController(KrunchyForceMeasurement p_ForceMeasurement)
    {
        m_ForceMeasurement = p_ForceMeasurement;

        m_iMinSpeed        = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Speed.Min"));
        m_iMaxSpeed        = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Speed.Max"));
        m_iMaxForce        = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Force.Max"));
        m_iForceToReachMinSpeed = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Force.Speed.Min"));

        double dbDeltaSpeed = m_iMinSpeed - m_iMaxSpeed;
        double dbDeltaForce = m_iForceToReachMinSpeed - 0;
        m_dbForceEquSlope   = dbDeltaSpeed / dbDeltaForce;

        m_iLastSpeedReturned = m_iMaxSpeed;
    }

    public boolean isMaxForceReached()
    {
        m_iLastForceMeasured = m_ForceMeasurement.getCurrentForce();

        boolean bMaxForce = m_iLastForceMeasured > m_iMaxForce;
        return bMaxForce;
    }


    public int getCurrentUpSpeed()
    {
        m_iLastForceMeasured = m_ForceMeasurement.getCurrentForce();

        m_bUpMovement = true;
        // lookup the speed for this force
        m_iLastSpeedReturned = calcMaxSpeed(m_iLastForceMeasured);
        LOGGER.debug("getCurrentUpSpeed() force: {}  speed {}", m_iLastForceMeasured, m_iLastSpeedReturned);

        return m_iLastSpeedReturned;  // in steps/second
    }

    public int getCurrentDownSpeed()
    {
        m_iLastForceMeasured = m_ForceMeasurement.getCurrentForce();

        m_bUpMovement = false;
        // lookup the speed for this force
        m_iLastSpeedReturned = calcMaxSpeed(m_iLastForceMeasured);
        LOGGER.debug("getCurrentDownSpeed() force: {}  speed {}", m_iLastForceMeasured, m_iLastSpeedReturned);

        return m_iLastSpeedReturned;  // in steps/second
    }

    public int getCurrentColorSpeed()
    {
        // for the color definition the down movement is negative
        int iColorSpeed = -m_iLastSpeedReturned;

        if (m_bUpMovement)
        {
            iColorSpeed = m_iLastSpeedReturned;
        }

        return iColorSpeed;
    }

    private int calcMaxSpeed(int p_iCurrentForce)
    {
        int iForce = p_iCurrentForce;
        // use linear equation to calculate the allowed speed
        // y = ax + b  ->  speed = slope * force + maxspeed

        // precondition
        if (iForce < 0)
        {
            iForce = 0;
        }

        // calculation
        double dbSpeed = m_dbForceEquSlope * iForce + m_iMaxSpeed;

        // postcondition
        if (dbSpeed < m_iMinSpeed)
        {
            dbSpeed = m_iMinSpeed;
        }
        if (dbSpeed > m_iMaxSpeed)
        {
            dbSpeed = m_iMaxSpeed;
        }

        return (int) dbSpeed;
    }


    public int getLastForceMeasured()
    {
        return m_iLastForceMeasured;
    }

    public int getCurrentForceMeasured()
    {
        m_iLastForceMeasured = m_ForceMeasurement.getCurrentForce();
        return getLastForceMeasured();
    }

}
