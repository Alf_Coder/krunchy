/*
 * Copyright (c) 2019 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.motion;

import ffhs.krunchy.synchronisation.SynchObject;
import ffhs.krunchy.synchronisation.SynchObjectCompletion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KrunchyMotorDriver
{
    private static final Logger LOGGER = LoggerFactory.getLogger(KrunchyMotorDriver.class);

    //private StepperBoardL6470      m_StepperDriver = null;
    private StepperDriverL6470      m_StepperDriver       = null;
    private KrunchySpeedController  m_SpeedController = null;


    public KrunchyMotorDriver(KrunchySpeedController p_SpeedController)
    {
        m_SpeedController = p_SpeedController;
        activateStepperBoard();
    }

    private void activateStepperBoard()
    {
        m_StepperDriver = new StepperDriverL6470(m_SpeedController);


        m_StepperDriver.start();
    }

    public int getCurrentSpeed()
    {
        return 0;
    }

    public int getCurrentPosition()
    {
        return 0;
    }


    public SynchObject initializeAxis()
    {
        return m_StepperDriver.initializeAxis();
        //return new SynchObject("bla");
    }

    public void moveToSafePosition()
    {

    }

    public SynchObjectCompletion crunchWithForceSpeed()
    {
        return m_StepperDriver.crunchWithForceSpeed();
    }

    public SynchObjectCompletion releaseWithForceSpeed()
    {
        return m_StepperDriver.releaseWithForceSpeed();
    }


    public void stopMovement()
    {
        m_StepperDriver.stopTask();
    }

    public void shutdown()
    {
        m_StepperDriver.stopThread();
    }


    public void switchOffMotorPower()
    {

    }

    public void switchOffCoolingFan()
    {

    }

    public boolean isAxisBelowRefSensor()
    {
        return m_StepperDriver.isAxisBelowRefSensor();
    }

    public void getStatusRegister()
    {
        m_StepperDriver.getStatusRegister();
    }


    public void selfTest()
    {
        m_StepperDriver.switchCoolingFanOn();
        m_StepperDriver.switchMotorPowerOn();
        m_StepperDriver.selfTest();
    }

}
