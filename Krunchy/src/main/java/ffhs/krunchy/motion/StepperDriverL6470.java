/*
 * Copyright (c) 2019 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.motion;

import com.diozero.api.*;
import com.diozero.util.RuntimeIOException;
import ffhs.krunchy.control.KrunchyConfig;
import ffhs.krunchy.synchronisation.SynchObjectCommand;
import ffhs.krunchy.synchronisation.SynchObjectCompletion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static ffhs.krunchy.motion.L6470_Commands.*;
import static ffhs.krunchy.motion.L6470_Params.*;


public class StepperDriverL6470 extends Thread
{
    private static final Logger LOGGER = LoggerFactory.getLogger(StepperDriverL6470.class);

    private static final int DIRECTION_UP   = 0;
    private static final int DIRECTION_DOWN = 1;

    // thread synchronization
    private volatile boolean      m_bKeepRunning     = true;
    private volatile boolean      m_bStopCurrentTask = false;

    private SynchObjectCommand    m_CommandReceived  = new SynchObjectCommand("MotorCommand");
    private SynchObjectCompletion m_CommandCompleted = new SynchObjectCompletion("CommandCompleted");

    // hardware components
    private SpiDevice               m_SpiChannel;
    private KrunchySpeedController  m_SpeedController;

    private int m_iPinNrBusySync;
    private int m_iPinNrReset;
    private int m_iPinNrMotorRelay;
    private int m_iPinNrCoolingFan;

    private WaitableDigitalInputDevice m_pinBusySynch;

    private DigitalOutputDevice m_pinBoardReset;
    private DigitalOutputDevice m_pinMotorPowerRelay;
    private DigitalOutputDevice m_pinCoolingFan;

    // parameters to remeber
    private double m_dbTick;

    private int m_iCurrentHold;
    private int m_iCurrentAcc;
    private int m_iCurrentDec;
    private int m_iCurrentRun;

    private int m_iBackEmfStartSlope;
    private int m_iBackEmfFinalSlopeAcc;
    private int m_iBackEmfFinalSlopeDec;

    private boolean m_bVoltageCompensation;

    private int m_iThermalWindingCompensation;

    private int m_iOvercurrentTriggerThreshold;
    private int m_iStallcurrentTriggerThreshold;

    private long m_lMicrostepping;

    private int m_iMinSpeed;
    private int m_iMaxSpeed;
    private boolean m_bMinSpeedOptimization;
    private int m_iAcceleration;
    private int m_iDeceleration;
    private int m_iFullStepFrequency;

    private boolean m_bLoggingNeeded;
    private long m_lLastStatus;

    private int m_iCrackForceDropMin;
    private int m_iCrackForceDropPercent;
    private int m_iHighestForceMeasured;


    public StepperDriverL6470(KrunchySpeedController p_SpeedController)
    {
        super("Stepper Driver 1");
        m_SpeedController = p_SpeedController;

        LOGGER.info("StepperDriverL6470 ctor enter");

        readConfiguration();
        createHardware();
        activateHardware();

        //selfTest(); // todo removeMe

        LOGGER.info("StepperDriverL6470 ctor exit");
    }


    @Override
    public void finalize()
    {
        // try to shutdown what has been started
        LOGGER.error("L6470 Spi Stepper Driver object gets deleted");

        activateBoardReset();
        threadSleepMillis(250);

        switchMotorPowerOff();
        threadSleepMillis(500);

        switchCoolingFanOff();

        m_SpiChannel.close();

    }


    private void readConfiguration()
    {
        LOGGER.debug("StepperDriverL6470 readConfiguration enter");

        // hardware related values
        m_iPinNrBusySync    = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.GPIO.Pin.BusySync"));
        m_iPinNrReset       = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.GPIO.Pin.BoardReset"));
        m_iPinNrMotorRelay  = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.GPIO.Pin.MotorPowerRelay"));
        m_iPinNrCoolingFan  = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.GPIO.Pin.CoolingFan"));

        m_dbTick            = Double.parseDouble(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Ticker"));

        // read some L6470 parameters
        m_iCurrentHold = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Current.Hold"));
        m_iCurrentAcc  = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Current.Acc"));
        m_iCurrentDec  = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Current.Dec"));
        m_iCurrentRun  = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Current.Run"));

        m_bVoltageCompensation =  Boolean.parseBoolean(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Compensation.Voltage"));

        m_iBackEmfStartSlope    = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.BackEMF.StartSlope"));
        m_iBackEmfFinalSlopeAcc = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.BackEMF.FinalSlopeAcc"));
        m_iBackEmfFinalSlopeDec = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.BackEMF.FinalSlopeDec"));

        m_iThermalWindingCompensation = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Compensation.Thermal"));

        m_iOvercurrentTriggerThreshold = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Threshold.Overcurrent"));
        m_iStallcurrentTriggerThreshold = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Threshold.Stallcurrent"));

        m_lMicrostepping   = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Microstepping"));

        m_iMinSpeed        = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Speed.Min"));
        m_iMaxSpeed        = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Speed.Max"));
        m_iAcceleration    = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Ramp.Acc")); // in step/s^2
        m_iDeceleration    = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Ramp.Dec")); // in step/s^2
        m_bMinSpeedOptimization = Boolean.parseBoolean(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Speed.Min.Optimized"));
        m_iFullStepFrequency =  Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.dSpin.Frequency.Fullstep"));

        //  process parameter
        m_iCrackForceDropMin     = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.Process.Force.Crack.Drop.Min"));
        m_iCrackForceDropPercent = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.Process.Force.Crack.Drop.Percent"));

        LOGGER.debug("StepperDriverL6470 readConfiguration exit");
    }

    private void createHardware()
    {
        LOGGER.debug("StepperDriverL6470 createHardware enter");

        // create the SPI channel for the driver board
        int iSpiController = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.SPI.Controller"));
        int iSpiChipSelect = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.SPI.ChipSelect"));
        int iSpiFrequency  = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.SPI.Frequency"));
        int iSpiMode       = Integer.parseInt(KrunchyConfig.getInstance().getValue("Krunchy.L6470.SPI.Mode"));
        SpiClockMode eSpiMode  = SpiClockMode.values()[iSpiMode];
        boolean bLsbFirst = Boolean.parseBoolean(KrunchyConfig.getInstance().getValue("Krunchy.L6470.SPI.LsbFirst"));

        try
        {
            m_SpiChannel = new SpiDevice(iSpiController, iSpiChipSelect, iSpiFrequency, eSpiMode, bLsbFirst);
        }
        catch(RuntimeIOException exception)
        {
            LOGGER.error("SpiDevice not created:\n{}", exception.getMessage());
        }

        // create the digital IO of the driver board
        m_pinBusySynch         = new WaitableDigitalInputDevice(m_iPinNrBusySync, GpioPullUpDown.PULL_UP, GpioEventTrigger.BOTH);
        m_pinBoardReset        = new DigitalOutputDevice(m_iPinNrReset, false, true);
        m_pinMotorPowerRelay   = new DigitalOutputDevice(m_iPinNrMotorRelay, true, false);
        m_pinCoolingFan        = new DigitalOutputDevice(m_iPinNrCoolingFan, true, false);

        LOGGER.debug("StepperDriverL6470 createHardware exit");
    }


    private void activateHardware()
    {
        LOGGER.debug("StepperDriverL6470 activateHardware enter");

        switchCoolingFanOn();
        threadSleepMillis(500);

        // L6470 needs motor power ON, even for communication
        switchMotorPowerOn();
        threadSleepMillis(500);

        activateBoardReset(); // activate the board from hard reset to refresh everything
        threadSleepMillis(500);
        releaseBoardReset(); // release the board from hard reset, start with default parameters
        threadSleepMillis(500);
        getConfigRegister();

        // now the registers are all reset and needs to be re-written
        // but it seems that sometimes the h-bridges are already active and prevent writing some params
        cmdHardHiZ();   // to allow configuration settings which need the bridgs in off state

        enableSwitchMode(true); // do not hard shutdown on switch event

        getConfigRegister();
        // set the motor current
        setMotorCurrentHold(m_iCurrentHold);
        setMotorCurrentAcc(m_iCurrentAcc);
        setMotorCurrentDec(m_iCurrentDec);
        setMotorCurrentRun(m_iCurrentRun);
        double dbKval = 24 * (getMotorCurrentRun()/256);
        LOGGER.debug("Motor Current Run (KVAL):\t{}", dbKval);

        // set supply voltage compensation
        enableVoltageCompensation(m_bVoltageCompensation);

        // set back emf compensation factors
        setStartSlope(m_iBackEmfStartSlope);
        setFinalSlopeAcc(m_iBackEmfFinalSlopeAcc);
        setFinalSlopeDec(m_iBackEmfFinalSlopeDec);

        setThermalWindingCompensation(m_iThermalWindingCompensation);

        setOverCurrentThreshold(m_iOvercurrentTriggerThreshold);
        setStallCurrentThreshold(m_iStallcurrentTriggerThreshold);

        setStepMode(m_lMicrostepping);

        setMinSpeed(m_bMinSpeedOptimization, m_iMinSpeed);
        setMaxSpeed(m_iMaxSpeed);

        setAcceleration(m_iAcceleration); // in step/s^2
        setDeceleration(m_iDeceleration); // in step/s^2

        setFullStepFrequency(m_iFullStepFrequency);

        cmdSoftStop(); // this should bring the h-bridges back and apply the holding current

        LOGGER.debug("StepperDriverL6470 activateHardware exit");
    }


    public void selfTest()
    {
        LOGGER.info("SpiStepperTest start test movements");

        getConfigRegister();
        testGetStatus();

        cmdRun(700, DIRECTION_DOWN);
        checkStatus();
        threadSleepMillis(1000);
        checkStatus();
        cmdRun(800,DIRECTION_DOWN);
        threadSleepMillis(3000);
        cmdRun(900, DIRECTION_UP);
        threadSleepMillis(1000);
        cmdRun(700, DIRECTION_UP);
        threadSleepMillis(1000);
        cmdRun(600, DIRECTION_UP);
        threadSleepMillis(2000);
        cmdRun(500, DIRECTION_UP);
        threadSleepMillis(1000);
        cmdRun(400, DIRECTION_UP);
        threadSleepMillis(1000);
        cmdRun(300, DIRECTION_UP);
        threadSleepMillis(1000);
        cmdRun(350, DIRECTION_UP);
        threadSleepMillis(50);
        cmdRun(400, DIRECTION_UP);
        threadSleepMillis(50);
        cmdRun(450, DIRECTION_UP);
        threadSleepMillis(50);
        cmdRun(500, DIRECTION_UP);
        threadSleepMillis(50);
        cmdRun(350, DIRECTION_UP);
        threadSleepMillis(250);
        cmdSoftStop();
        checkStatus();

        cmdGoUntil(400, DIRECTION_DOWN);

        threadSleepMillis(15000);

        checkStatus();
        LOGGER.info("testMovements movements finished");
    }




    /**
     *  the running core of the thread
     */
    @Override
    public void run()
    {
        LOGGER.debug("started");

        // the thread loop
        while(m_bKeepRunning)
        {

            LOGGER.info("waiting for command");

            // wait for a command call
            doWaitForCommand();

            // update the status of the board
            cmdGetStatus();

            // handle the command
            doHandleCommand();

            threadSleepMillis(100);
        }


        // thread is being stopped, shutdown the converter
        LOGGER.info("{} running loop ended", getName());
        switchMotorPowerOff();
        switchCoolingFanOff();
    }


    private void doHandleCommand()
    {
        String sCommand = m_CommandReceived.getCommand();
        LOGGER.info("command received: {}", sCommand);

        if (sCommand.equals("StopThread"))
        {
            // do nothing but drop out of the loop
            LOGGER.info("do nothing");

        }
        else if (sCommand.equals("InitializeAxis"))
        {
            LOGGER.info("do initialize");
            doInitializeAxis();
            notifyCommandCompletion();
        }
        else if (sCommand.equals("CrunchWithForceSpeed"))
        {
            LOGGER.info("do crunch with force dependent speed");
            m_iHighestForceMeasured = 0;
            doCrunchWithForceSpeed();
            notifyCommandCompletion();
        }
        else if(sCommand.equals("ReleaseWithForceSpeed"))
        {
            LOGGER.info("do release with force dependent speed");
            doReleaseWithForceSpeed();
            notifyCommandCompletion();
        }
        // else if(sCommand.equals("bla"))
        //  other commands to be added...


    }



    /**
     *  tell the other thread which is waiting for a command completion that it is finished
     */
    private void notifyCommandCompletion()
    {
        LOGGER.info("notify sync object {}", m_CommandCompleted.getName());

        synchronized(m_CommandCompleted)
        {
            m_CommandCompleted.setSignalled();
            m_CommandCompleted.notify();
        }
    }


    /**
     *  wait until the next command comes in
     */
    public void doWaitForCommand()
    {
        synchronized(m_CommandReceived)
        {
            if (m_CommandReceived.isPending())
            {
                // if this motor thread starts waiting after the command
                LOGGER.info("new command already received: {}", m_CommandReceived.getCommand());
            }
            else
            {
                // not yet received, wait for it
                try
                {
                    m_CommandReceived.wait();
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
            m_CommandReceived.resetPending();
        }
    }


    /**
     *  at the end of the program execution the thread should be stopped as a shutdown cleanup
     */
    public void stopThread()
    {
        LOGGER.info("stopping thread {}", getName());
        m_bKeepRunning = false;

        synchronized(m_CommandReceived)
        {
            m_CommandReceived.setPending();
            m_CommandReceived.setCommand("StopThread");
            m_CommandReceived.notify();
        }
    }

    /**
     *  as an intervention from the outside, the current task can be stopped at the next occasion when
     *  a flag can be checked
     *  only sets the shutdown flag, no synch object returned or modified
     */
    public void stopTask()
    {
        LOGGER.info("stopping task {}", getName());
        m_bStopCurrentTask = true;
    }


    /**
     *  as an intervention from the outside, the current task can be stopped at the next occasion when
     *  a flag can be checked
     *  only sets the shutdown flag, no synch object returned or modified
     */
    public void coverOpen()
    {
        LOGGER.info("cover open stops task {}", getName());
        m_bStopCurrentTask = true;
        activateBoardReset(); // activate the board from hard reset to refresh everything
    }

    /**
     *  prepares the completion synch objext before notification to tell the outside about the negative
     *  result of the task
     *
     * @param p_sErrorType  the type of error that stopped the task
     */
    private void setCompletionError(String p_sErrorType)
    {
        if (m_CommandCompleted != null)
        {
            m_CommandCompleted.setError(p_sErrorType);
        }
    }

    /**
     *
     * @return
     */
    public SynchObjectCompletion initializeAxis()
    {
        LOGGER.info("enter initializeAxis");

        synchronized(m_CommandReceived)
        {
            m_CommandReceived.setPending();
            m_CommandReceived.setCommand("InitializeAxis");
            m_CommandReceived.notify();
        }
        m_bStopCurrentTask = false;
        m_CommandCompleted = new SynchObjectCompletion("InitializeAxis");
        return m_CommandCompleted;
    }

    /**
     *
     * @return
     */
    public SynchObjectCompletion crunchWithForceSpeed()
    {
        LOGGER.info("enter crunchWithForceSpeed");

        synchronized(m_CommandReceived)
        {
            m_CommandReceived.setPending();
            m_CommandReceived.setCommand("CrunchWithForceSpeed");
            m_CommandReceived.notify();
        }
        m_bStopCurrentTask = false;
        m_CommandCompleted = new SynchObjectCompletion("CrunchWithForceSpeed");
        return m_CommandCompleted;
    }

    /**
     *
     * @return
     */
    public SynchObjectCompletion releaseWithForceSpeed()
    {
        LOGGER.info("enter releaseWithForceSpeed");

        synchronized(m_CommandReceived)
        {
            m_CommandReceived.setPending();
            m_CommandReceived.setCommand("ReleaseWithForceSpeed");
            m_CommandReceived.notify();
        }
        m_bStopCurrentTask = false;
        m_CommandCompleted = new SynchObjectCompletion("ReleaseWithForceSpeed");
        return m_CommandCompleted;
    }


    /**
     *  wait until the busy flag indicates that the stepper board has finished the task
     */
    private void doWaitForNotBusy(int p_iTimeoutMillis)
    {
        // the busy pin output is low active, the flag in the status register is high active...
        // so, not busy means a 0 at the
        isBusySynchPinActive();

        try
        {
            // wait until the board has finished a command and set the busy flag to back 0
            LOGGER.debug("start waiting for busy pin inactive");
            m_pinBusySynch.waitForInactive(p_iTimeoutMillis);
            LOGGER.debug("end waiting for busy pin inactive");
            getStatusRegister();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        isBusySynchPinActive();

        // what to do in case of a timeout? return bool would be something
    }


    /**
     *  perform an initialisation of the stepper axis
     *  dependent on the state of the reference sensor it will move up or down to find
     *  the edge and then slowly to the switching point
     */
    private void doInitializeAxis()
    {
        LOGGER.debug("start initializing");

        if (isReferenceSensorActive())
        {
            LOGGER.debug("search the ref sensor upwards");
            cmdRun(100, DIRECTION_UP);
            threadSleepMillis(300);

            int iLoopCount = 40; // 40 * 250ms = 10sec
            while(iLoopCount > 0 && isAxisBelowRefSensor())
            {
                cmdRun(600, DIRECTION_UP);
                threadSleepMillis(250);
            }
            cmdSoftStop();
        }

        // inactive means the flag is inside of the sensor
        // move down until the flag gets out
        isReferenceSensorActive();
        isBusySynchPinActive();

        LOGGER.debug("search the ref sensor downwards");
        LOGGER.debug("cmdGoUntil({}, {})", m_iMaxSpeed, DIRECTION_DOWN);
//        setMinSpeed(false, m_iMaxSpeed);
        cmdGoUntil(m_iMaxSpeed, DIRECTION_DOWN);
        doWaitForNotBusy(10000);
  //      setMinSpeed(false, m_iMinSpeed);

        isBusySynchPinActive();

        // flag is now just below sensor send home command
        LOGGER.debug("home the axis upwards");
        LOGGER.debug("cmdReleaseSW({})", DIRECTION_UP);
        cmdReleaseSW(DIRECTION_UP);
        doWaitForNotBusy(10000);

        isBusySynchPinActive();

        getConfigRegister();

    }


    /**
     *  move the stepper axis down with max speed until the force starts to increase
     *  the speed will be reduced to allow more torque for even higher force
     *  this should end at the maximum force or until it has detected a crack
     */
    public void doCrunchWithForceSpeed()
    {
        boolean bContinue = true;

        int iCrackForce = 0;
        LOGGER.debug("reset local values iHighestForceMeasured: {} iCrackForce: {}", m_iHighestForceMeasured, iCrackForce);


        // checking task continuation
        while(bContinue)
        {
            isBusySynchPinActive();

            int iSpeed = m_SpeedController.getCurrentDownSpeed();
            int iForce = m_SpeedController.getLastForceMeasured();

            LOGGER.debug("if (iForce({}) > m_iHighestForceMeasured({}))", iForce,  m_iHighestForceMeasured);
            if (iForce > m_iHighestForceMeasured)
            {
                m_iHighestForceMeasured = iForce;
                double dbForceDrop = (double)m_iHighestForceMeasured / 100 * m_iCrackForceDropPercent; // n percent of the max value

                int iForceDrop = (int) dbForceDrop;
                if (iForceDrop < m_iCrackForceDropMin)
                {
                    iForceDrop = m_iCrackForceDropMin;
                }
                iCrackForce = m_iHighestForceMeasured - iForceDrop;

                LOGGER.debug("new crack force = {} ({} - {})", iCrackForce, m_iHighestForceMeasured, iForceDrop);

            }
            LOGGER.debug("cmdRun({}, {})", iSpeed, DIRECTION_DOWN);

            cmdRun(iSpeed, DIRECTION_DOWN);
            threadSleepMillis(250);

            getStatusRegister();

            // needs to filter out the error cases, the busy flag and the motor status are not reliable
            // because they tend to change during issuing new commands
            // -> ref.manual: This command keeps the BUSY flag low until the target speed is reached.

            // the first byte should be 7E the second after filtering is 10 (only DIR and not HiZ)
            int iStatus = getStatusRegister() & 0b1111111100010001 ;
            int iExpect =                       0b0111111000010000;

            boolean bMaxForceReached = m_SpeedController.isMaxForceReached();

            boolean bMaxPositionReached = false;
            // TODO add position checking

            iForce = m_SpeedController.getLastForceMeasured();
            boolean bCrackDetected = iForce < iCrackForce;
            LOGGER.debug("boolean bCrackDetected =", iForce,  iCrackForce);

            if (m_bStopCurrentTask)
            {
                LOGGER.debug("===================");
                LOGGER.debug("| Stopped by User |");
                LOGGER.debug("===================");
                bContinue = false;
                setCompletionError("Stopped by User");
            }
            else if (iStatus != iExpect)
            {
                LOGGER.debug("Status: {} Expected: {}", String.format("0x%04X", iStatus), String.format("0x%04X", iExpect));
                cmdGetStatus();
                bContinue = false;
            }

            else if (bCrackDetected)
            {
                LOGGER.debug("========================================");
                LOGGER.debug("| Crack detected Lo {} vs. Hi {} |", iForce, m_iHighestForceMeasured);
                LOGGER.debug("========================================");
                bContinue = false;
            }
            else if (bMaxForceReached)
            {
                LOGGER.debug("=====================");
                LOGGER.debug("| Max Force reached |");
                LOGGER.debug("=====================");
                bContinue = false;
                setCompletionError("Max force reached");
            }
            else if (false)
            {
                // max crunch position reached is also a shutdown reason

            }
            else
            {
                LOGGER.debug("Status {} -> continue crunching with force {}", String.format("0x%04X", iStatus), m_SpeedController.getLastForceMeasured());
            }
        }
        cmdSoftStop();
        cmdGetStatus();

    }

    /**
     *  move the stepper axis up with reduced speed dependent on the force applied to the
     *  loadcells.
     */
    private void doReleaseWithForceSpeed()
    {
        boolean bContinue = true;

        // checking task continuation
        while(bContinue)
        {
            isBusySynchPinActive();

            int iSpeed = m_SpeedController.getCurrentUpSpeed();
            LOGGER.debug("cmdRun({}, {})", iSpeed, DIRECTION_UP);

            cmdRun(iSpeed, DIRECTION_UP);
            threadSleepMillis(250);

            bContinue = isAxisBelowRefSensor();
        }
        cmdSoftStop();
        cmdGetStatus();


        // at the end home the axis properly
        doInitializeAxis();

    }



    // ==========================================================================================
    // dSpin Parameter Configuration Section
    // ==========================================================================================

    private void setAlarmCondition(byte p_byAlarmFlagSource)
    {
        byte[] commandByte = {L6470_CMD_SET_PARAM + L6470_Params.L6470_PARAM_ALARM_EN};
        byte[] dataByte    = {p_byAlarmFlagSource};
        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);
    }



    private void enableSwitchMode(boolean p_bEnableState)
    {
        // inefficient version of the config register setting, since the current
        // state must be read before write in order to modify only one flag

        // read the current configuration bytes
        byte[] commandByte  = {L6470_Commands.L6470_CMD_GET_PARAM + L6470_Params.L6470_PARAM_CONFIG}; // 0x20 + 0x18 = 0x38
        byte[] oneByteRead  = {L6470_Commands.L6470_CMD_NOP};
        byte[] configBytes = new byte[2];

        m_SpiChannel.write(commandByte);
        configBytes[0] = m_SpiChannel.writeAndRead(oneByteRead)[0];
        configBytes[1] = m_SpiChannel.writeAndRead(oneByteRead)[0];

        String sCommand = String.format("0x%02X 0x%02X 0x%02X", commandByte[0], configBytes[0], configBytes[1]);
        LOGGER.info("Config register read {}", sCommand);

        // write the new configuration bytes in single byte arrays
        commandByte[0] = L6470_CMD_SET_PARAM + L6470_Params.L6470_PARAM_CONFIG;
        byte[] configByte1 = {configBytes[0]};
        byte[] configByte2 = {configBytes[1]};

        if (p_bEnableState)
        {
            LOGGER.info("enableSwitchMode(1)");
            configBytes[1] = (byte) (configBytes[1] | 0b00010000);
            configByte2[0] = (byte) (configByte2[0] | 0b00010000);
        }
        else
        {
            LOGGER.info("enableSwitchMode(0)");
            configBytes[1] = (byte) (configBytes[1] & 0b11101111);
            configByte2[0] = (byte) (configByte2[0] & 0b11101111);
        }

        setConfigRegister(configBytes);

        sCommand = String.format("0x%02X 0x%02X 0x%02X", commandByte[0], configByte1[0], configByte2[0]);
        LOGGER.info("Config register write {}", sCommand);

//        m_StepperBoard.write(commandByte);
//        m_StepperBoard.write(configByte1);
//        m_StepperBoard.write(configByte2);
    }


    public void getConfigRegister()
    {
        // h18 CONFIG IC configuration
        // 2 bytes, reset at 2E88 (2E08)
        // Internal oscillator,
        // 2MHz OSCOUT clock,
        // supply voltage compensation disabled,
        // overcurrent shutdown enabled,          --> 2E88 vs 2E08
        // slew-rate = 290 V/µs
        // PWM frequency = 15.6kH
        byte[] commandByte  = {L6470_Commands.L6470_CMD_GET_PARAM + L6470_Params.L6470_PARAM_CONFIG};
        byte[] oneByteRead  = {L6470_Commands.L6470_CMD_NOP};
        byte[] configReturn = new byte[2];

        m_SpiChannel.write(commandByte);
        configReturn[0] = m_SpiChannel.writeAndRead(oneByteRead)[0];
        configReturn[1] = m_SpiChannel.writeAndRead(oneByteRead)[0];

        String sCommand = String.format("0x%02X",commandByte[0]);

        int iBytesReceived = configReturn.length;
        String sResult = "";
        for (int iByte = 0; iByte < iBytesReceived; iByte++)
        {
            sResult = sResult + String.format(" 0x%02X",configReturn[iByte]);
        }

        LOGGER.info("SpiDevice getConfigRegister ({}) returned Status ({} bytes):{}",sCommand ,iBytesReceived, sResult);

    }










    private void setConfigRegister(byte[] p_byParameter)
    {
        // write the new configuration bytes in single byte arrays
        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_Params.L6470_PARAM_CONFIG};
        byte[] configByte1 = {p_byParameter[0]};
        byte[] configByte2 = {p_byParameter[1]};

        String sCommand = String.format("0x%02X 0x%02X 0x%02X", commandByte[0], configByte1[0], configByte2[0]);
        LOGGER.info("setConfigRegister({})", sCommand);

        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(configByte1);
        m_SpiChannel.write(configByte2);
    }


    public void checkStatus()
    {
        long lCurrentStatus = getStatusRegister();
        long lStateDifference = lCurrentStatus^m_lLastStatus;

        if (lStateDifference != 0)
        {
            // log the differences of the state
            LOGGER.info("Status: -------------------------");
            if ((lStateDifference & 0b0000000000000001) > 0)
            {
                long lHiZ = lCurrentStatus & 0b0000000000000001;
                LOGGER.info("High-Z:     \t{}", lHiZ);
            }

            if ((lStateDifference & 0b0000000000000010) > 0)
            {
                long lBusy = (lCurrentStatus & 0b0000000000000010) >> 1;
                LOGGER.info("Busy:      \t{}", lBusy);
            }

            if ((lStateDifference & 0b0000000000000100) > 0)
            {
                long lSwitch = (lCurrentStatus & 0b0000000000000100) >> 2;
                LOGGER.info("SwitchState: \t{}", lSwitch);
            }

            if ((lStateDifference & 0b0000000000001000) > 0)
            {
                long lSwitch = (lCurrentStatus & 0b0000000000001000) >> 3;
                LOGGER.info("SwitchEvent: \t{}", lSwitch);
            }

            if ((lStateDifference & 0b0000000000010000) > 0)
            {
                long lDir = (lCurrentStatus & 0b0000000000010000) >> 4;
                LOGGER.info("Direction: \t{}", lDir);
            }

            if ((lStateDifference & 0b0000000001100000) > 0)
            {
                long lMotorState = (lCurrentStatus & 0b0000000001100000) >> 5;
                LOGGER.info("MotorState: \t{}", lMotorState);
            }

            if ((lStateDifference & 0b0000000010000000) > 0)
            {
                long lNotPerform = (lCurrentStatus & 0b0000000010000000) >> 7;
                LOGGER.info("NotPerform: \t{}", lNotPerform);
            }

            if ((lStateDifference & 0b0000000100000000) > 0)
            {
                long lWrongCmd = (lCurrentStatus & 0b0000000100000000) >> 8;
                LOGGER.info("WrongCmd: \t{}", lWrongCmd);
            }

            if ((lStateDifference & 0b0000001000000000) > 0)
            {
                long lUvLock = (lCurrentStatus & 0b0000001000000000) >> 9;
                LOGGER.info("UnderVoltage: \t{}", lUvLock);
            }

            if ((lStateDifference & 0b0000010000000000) > 0)
            {
                long lWarnLevel = (lCurrentStatus & 0b0000010000000000) >> 10;
                LOGGER.info("WarnLevel: \t{}", lWarnLevel);
            }

            if ((lStateDifference & 0b0000100000000000) > 0)
            {
                long lStopLevel = (lCurrentStatus & 0b0000100000000000) >> 11;
                LOGGER.info("StopLevel: \t{}", lStopLevel);
            }

            if ((lStateDifference & 0b0001000000000000) > 0)
            {
                long lOverCurrent = (lCurrentStatus & 0b0001000000000000) >> 12;
                LOGGER.info("OverCurrent: \t{}", lOverCurrent);
            }

            if ((lStateDifference & 0b0010000000000000) > 0)
            {
                long lStepLoss = (lCurrentStatus & 0b0010000000000000) >> 13;
                LOGGER.info("StepLoss A: \t{}", lStepLoss);
            }

            if ((lStateDifference & 0b0100000000000000) > 0)
            {
                long lStepLoss = (lCurrentStatus & 0b0100000000000000) >> 14;
                LOGGER.info("StepLoss B: \t{}", lStepLoss);
            }

            if ((lStateDifference & 0b1000000000000000) > 0)
            {
                long lStepClockMode = (lCurrentStatus & 0b1000000000000000) >> 15;
                LOGGER.info("StepClockMode: \t{}", lStepClockMode);
            }

        }

        // keep the state for the next check
        m_lLastStatus = lCurrentStatus;
    }


    private void setMotorCurrentHold(long p_lCurrentMilliAmp)
    {
        // calculate the KVal for this current and set the register accordingly
        //TODO
        // calculate the KVal value out of the given current by using the motor
        // specs and the supply voltage
        long lKValNominal = 25;

        long lKvalHold = 0x37;

        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_Params.L6470_PARAM_KVAL_HOLD};
        byte[] dataByte  = {(byte) lKvalHold};

        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);

    }

    private void setMotorCurrentAcc(long p_lCurrentMilliAmp)
    {
        // calculate the KVal for this current and set the register accordingly
        //TODO: calculate the KVal value out of the given current by using the motor specs and the supply voltage
        long lKValNominal = 25;

        long lKvalAcceleration = 0x37;

        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_Params.L6470_PARAM_KVAL_ACC};
        byte[] dataByte  = {(byte) lKvalAcceleration};

        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);
    }


    private void setMotorCurrentDec(long p_lCurrentMilliAmp)
    {
        // calculate the KVal for this current and set the register accordingly
        //TODO: calculate the KVal value out of the given current by using the motor specs and the supply voltage
        long lKValNominal = 25;

        long lKvalDeceleration = 0x37;

        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_Params.L6470_PARAM_KVAL_DEC};
        byte[] dataByte  = {(byte) lKvalDeceleration};

        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);
    }

    private void setMotorCurrentRun(long p_lCurrentMilliAmp)
    {
        // calculate the KVal for this current and set the register accordingly
        //TODO: calculate the KVal value out of the given current by using the motor specs and the supply voltage
        long lKValNominal = 25;

        long lKvalRun = 0x37;

        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_Params.L6470_PARAM_KVAL_RUN};
        byte[] dataByte  = {(byte) lKvalRun};

        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);
    }

    private int getMotorCurrentRun()
    {
        byte[] commandByte  = {L6470_Commands.L6470_CMD_GET_PARAM + L6470_Params.L6470_PARAM_KVAL_RUN};
        byte[] oneByteRead  = {L6470_Commands.L6470_CMD_NOP};
        byte[] dataByte     = new byte[1];

        m_SpiChannel.write(commandByte);
        dataByte[0] = m_SpiChannel.writeAndRead(oneByteRead)[0];

        LOGGER.info("SpiDevice getMotorCurrentRun returned:{}", String.format(" 0x%02X",dataByte[0]));

        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);
        return dataByte[0];
    }


    private void enableVoltageCompensation(boolean p_bEnableState)
    {
        // inefficient version of the config register setting, since the current
        // state must be read before write in order to modify only one flag

        byte[] commandByte  = {L6470_Commands.L6470_CMD_GET_PARAM + L6470_Params.L6470_PARAM_CONFIG};
        byte[] oneByteRead  = {L6470_Commands.L6470_CMD_NOP};
        byte[] configReturn = new byte[2];

        m_SpiChannel.write(commandByte);
        configReturn[0] = m_SpiChannel.writeAndRead(oneByteRead)[0];
        configReturn[1] = m_SpiChannel.writeAndRead(oneByteRead)[0];

        commandByte[0] = L6470_CMD_SET_PARAM + L6470_Params.L6470_PARAM_CONFIG;
        byte[] configByte1 = {configReturn[0]};
        byte[] configByte2 = {configReturn[1]};

        if (p_bEnableState)
        {

        }
        else
        {

        }

        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(configByte1);
        m_SpiChannel.write(configByte2);

    }

    private void setStartSlope(long p_lStartSlope)
    {
        // When ST_SLP, FN_SLP_ACC and FN_SLP_DEC parameters are
        // set to zero no BEMF compensation is performed.
        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_Params.L6470_PARAM_ST_SLP};
        byte[] dataByte  = {(byte)p_lStartSlope};
        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);
    }

    private void setFinalSlopeAcc(long p_lFinalSlope)
    {
        // When ST_SLP, FN_SLP_ACC and FN_SLP_DEC parameters are
        // set to zero no BEMF compensation is performed.
        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_Params.L6470_PARAM_FN_SLP_ACC};
        byte[] dataByte  = {(byte)p_lFinalSlope};
        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);
    }

    private void setFinalSlopeDec(long p_lFinalSlope)
    {
        // When ST_SLP, FN_SLP_ACC and FN_SLP_DEC parameters are
        // set to zero no BEMF compensation is performed.
        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_Params.L6470_PARAM_FN_SLP_DEC};
        byte[] dataByte  = {(byte)p_lFinalSlope};
        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);
    }

    private void setThermalWindingCompensation(long p_lCoefficient)
    {
        byte[] commandByte  = {L6470_CMD_SET_PARAM & L6470_Params.L6470_PARAM_K_THERM};
        byte[] dataByte  = {(byte)p_lCoefficient};
        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);
    }

    private void setOverCurrentThreshold(long p_lCurrentMilliAmp)
    {
        // set the OCD_TH register to the value corresponding to the nearest value of the
        // 4-bit raster. The available range is from 375 mA to 6 A, steps of 375 mA
        // i.e.  375 -->  0
        //       750 -->  1
        //      6000 --> 15
        long lCurrentStep = 375;

        //logOCD_Thresh(p_lCurrentMilliAmp);

        // use integer-truncation for rounding by adding half step value
        long lValue = (p_lCurrentMilliAmp + (lCurrentStep/2)) / lCurrentStep;

        byte[] commandByte  = {L6470_CMD_SET_PARAM | L6470_Params.L6470_PARAM_OCD_TH};
        byte[] dataByte  = {(byte)lValue};
        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);
    }


    private void setStallCurrentThreshold(long p_lCurrentMilliAmp)
    {
        // set the STALL_TH register to the value corresponding to the nearest value of the
        // 7-bit raster. The available range is from 31.25 mA to 4 A with a resolution of 31.25 mA.
        // i.e.  31.25 -->   0
        //       62.50 -->   1
        //     4000.00 --> 127
        double dbCurrentStep = 31.25;

        //logStall_Thresh(p_lCurrentMilliAmp);

        long lValue = (int) Math.round((double)p_lCurrentMilliAmp / dbCurrentStep);

        byte[] commandByte  = {L6470_CMD_SET_PARAM | L6470_Params.L6470_PARAM_STALL_TH};
        byte[] dataByte  = {(byte)lValue};
        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);
    }


    // ======================================================================
    //  Step Mode handling
    // ======================================================================
    private void setStepMode(long p_lStepMode)
    {
        logStepMode(p_lStepMode);

        // m_lMicrostepping = 1  Full  step
        //                    2  Half  step
        //                    4  1/4   microstep
        //                    8  1/8   microstep
        //                   16  1/16  microstep
        //                   32  1/32  microstep
        //                   64  1/64  microstep
        //                  128  1/128 microstep
        m_lMicrostepping = 1 << p_lStepMode;

        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_Params.L6470_PARAM_STEP_MODE};
        byte[] dataByte  = {(byte) p_lStepMode};

        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);
    }

    private void logStepMode(long p_lStepMode)
    {
        // -----------------------------------------------------
        // |    7    |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
        // -----------------------------------------------------
        // | SYNC_EN |     SYNC_SEL    |  0  |    STEP_SEL     |
        // -----------------------------------------------------

        // when SYNC_EN bit is set low BUSY/SYNC output is forced low during commands execution
        // when SYNC_EN bit is set high, BUSY/SYNC output provides a clock signal according to
        // SYNC_SEL parameter.

        // The SYNC_SEL parameter selects the divider of the step frequency. this depends on
        // the selected STEP_SEL mode.

        // The STEP_SEL parameter selects one of eight possible stepping modes:
        // 0 Full  step
        // 1 Half  step
        // 2 1/4   microstep
        // 3 1/8   microstep
        // 4 1/16  microstep
        // 5 1/32  microstep
        // 6 1/64  microstep
        // 7 1/128 microstep
        if (m_bLoggingNeeded)
        {
            long lSyncEnabled = ((p_lStepMode & 0b10000000) >> 7);
            long lSyncSelect  = ((p_lStepMode & 0b01110000) >> 4);
            long lStepSelect  = ((p_lStepMode & 0b00000111) >> 0);

            LOGGER.info("StepMode({}) SYNC_EN:{} SYNC_SEL:{} STEP_SEL:{}", p_lStepMode, lSyncEnabled, lSyncSelect, lStepSelect);
        }
    }


    private void setMinSpeed(boolean p_bOptimizationEnabled, int p_iMinSpeed)
    {
//        byte[] commandBytes  = new byte[3];
//        commandBytes[0]  = L6470_CMD_SET_PARAM + L6470_Params.L6470_PARAM_MIN_SPEED;
//        m_SpiChannel.write(new byte[]{commandBytes[0]});
//        m_SpiChannel.write(new byte[]{commandBytes[1]});
//        m_SpiChannel.write(new byte[]{commandBytes[2]});


        // the L6470 is using min speed 0 when optimization is enabled, set only bit 12:
        int iStepsPerTick = 0b0001000000000000;

        if (!p_bOptimizationEnabled)
        {
            iStepsPerTick = calcMinSpeed(p_iMinSpeed);
        }

        cmdSetParam(L6470_PARAM_MIN_SPEED, iStepsPerTick, 13);


/*
        byte[] commandBytes  = new byte[3];
        commandBytes[0] = L6470_CMD_SET_PARAM + L6470_PARAM_MIN_SPEED;

        if (p_bOptimizationEnabled)
        {
            // L6470 is using speed 0 when optimization is enabled
            commandBytes[1]  = (byte) 0b0001000000000000;
            commandBytes[2]  = (byte) 0b0000000000000000;
        }
        else
        {
            long lStepsPerTick = calcMinSpeed(p_lMinSpeed);
            commandBytes[1]  = (byte) ((lStepsPerTick & 0b0000111100000000) >>  8);
            commandBytes[2]  = (byte) ((lStepsPerTick & 0b0000000011111111) >>  0);
        }

//        m_SpiChannel.write(new byte[]{commandBytes[0]});
//        m_SpiChannel.write(new byte[]{commandBytes[1]});
//        m_SpiChannel.write(new byte[]{commandBytes[2]});

  */
    }


    private int calcMinSpeed(int p_iMinSpeed)
    {
        int iStepsPerSecond = p_iMinSpeed;
        int iStepsPerTick   = (int)((double)iStepsPerSecond * m_dbTick / Math.pow(2, -24));
        // can be reduced to (double)lStepsPerSecond * 4.194304

        // parameter correction:
        // min speed is positive and not larger than 0x0FFF
        if (iStepsPerTick > 0x0FFF)
        {
            iStepsPerTick = 0x0FFF;
        }
        else if (iStepsPerTick < 0)
        {
            iStepsPerTick = 0;
        }

        return iStepsPerTick;
    }

    private void setMaxSpeed(int p_iMaxSpeed)
    {
//        byte[] commandBytes  = new byte[3];
//        commandBytes[0]  = L6470_CMD_SET_PARAM + L6470_PARAM_MAX_SPEED;

        int iStepsPerTick = calcMaxSpeed(p_iMaxSpeed);

//        commandBytes[1]  = (byte) ((iStepsPerTick & 0b0000001100000000) >>  8);
//        commandBytes[2]  = (byte) ((iStepsPerTick & 0b0000000011111111) >>  0);

//        m_SpiChannel.write(new byte[]{commandBytes[0]});
//        m_SpiChannel.write(new byte[]{commandBytes[1]});
//        m_SpiChannel.write(new byte[]{commandBytes[2]});

        cmdSetParam(L6470_PARAM_MAX_SPEED, iStepsPerTick, 10);
    }


    private int calcMaxSpeed(int p_iMaxSpeed)
    {
        int iStepsPerSecond = p_iMaxSpeed;
        int iStepsPerTick   = (int)((double)iStepsPerSecond * m_dbTick / Math.pow(2, -18));
        // can be reduced to (double)lStepsPerSecond * 0.065536

        // parameter correction:
        // max speed is positive and not larger than 0x03FF (10 bits)
        if (iStepsPerTick > 0x03FF)
        {
            iStepsPerTick = 0x03FF;
        }
        else if (iStepsPerTick < 0)
        {
            iStepsPerTick = 0;
        }

        return iStepsPerTick;
    }


    private void setAcceleration(long p_lSteps)
    {
        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_KVAL_ACC};
        byte[] dataByte  = {getAccDecSpeed(p_lSteps)};

        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);
    }


    private void setDeceleration(long p_lSteps)
    {
        byte[] commandByte  = {L6470_CMD_SET_PARAM + L6470_PARAM_KVAL_DEC};
        byte[] dataByte  = {getAccDecSpeed(p_lSteps)};

        m_SpiChannel.write(commandByte);
        m_SpiChannel.write(dataByte);
    }

    private byte getAccDecSpeed(long p_lSteps)
    {
        double dbStepPerTickSquare = ((double)p_lSteps * m_dbTick * m_dbTick)/Math.pow(2, -40);
        int i = (int)Math.round(dbStepPerTickSquare);
        LOGGER.info("Acc/Dec steps({}) = {} round {}", p_lSteps, dbStepPerTickSquare, i);
        return (byte) (int)Math.round(dbStepPerTickSquare);
    }



    private void setFullStepFrequency(int p_iFrequency)
    {
        int iStepsPerTick   = (int)((double)p_iFrequency * m_dbTick / Math.pow(2, -18) - 0.5);
        cmdSetParam(L6470_PARAM_FS_SPD, iStepsPerTick, 10);
    }

    private void testSetMaxSpeed()
    {
        byte[] commandBytes  = new byte[4];
        commandBytes[0]  = L6470_CMD_SET_PARAM;
    }

    public void testGetStatus()
    {
        // GetStatus
        // Command Structure:
        // from Host:   1 byte  0xD0
        // to host:     2 bytes MSB:LSB
        // The GetStatus command returns the Status register value.
        // GetStatus command resets the STATUS register warning flags. The command forces the
        // system to exit from any error state. The GetStatus command DO NOT reset HiZ flag.

        // Returns the current contents of the STATUS register on the L6470 chip.
        // This is a good communications sanity check because on boot, the value will be 0x2E88.
        byte[] commandByte  = {L6470_Commands.L6470_CMD_GETSTATUS};
        byte[] oneByteRead  = {L6470_Commands.L6470_CMD_NOP};
        byte[] statusReturn = new byte[2];

        m_SpiChannel.write(commandByte);
        statusReturn[0] = m_SpiChannel.writeAndRead(oneByteRead)[0];
        statusReturn[1] = m_SpiChannel.writeAndRead(oneByteRead)[0];

        String sCommand = String.format("0x%02X",commandByte[0]);

        int iBytesReceived = statusReturn.length;
        String sResult = "";
        for (int iByte = 0; iByte < iBytesReceived; iByte++)
        {
            sResult = sResult + String.format(" 0x%02X",statusReturn[iByte]);
        }

        LOGGER.info("SpiDevice getStatus ({}) returned Status ({} bytes):{}",sCommand ,iBytesReceived, sResult);

    }









    // ==========================================================================================
    // dSpin Command Section
    // ==========================================================================================


    /**
     *  dSpin command SetParam (PARAM, VALUE)
     *  The SetParam command sets the PARAM register value equal to VALUE; PARAM is the
     *  respective register address. The command should be followed by the new register
     *  VALUE (most significant byte first).
     *  The number of bytes composing the VALUE argument depends on the length of the
     *  target register.
     *  Some registers cannot be written, any attempt to write one of those registers
     *  causes the command to be ignored and the WRONG_CMD flag to rise at the end of
     *  command byte as like as unknown command code is sent.
     *  Some registers can only be written in particular conditions, any attempt to
     *  write one of those registers when the conditions are not satisfied causes the
     *  command to be ignored and the NOTPERF_CMD flag to rise at the end of last
     *  argument byte.
     *  Any attempt to set an inexistent register (wrong address value) causes the
     *  command to be ignored and WRONG_CMD flag to rise at the end of command byte
     *  as like as unknown command code is sent.
     *
     * @param p_iParam  the register adress of the parameter
     * @param p_iValue  the new value of the parameter
     * @param p_iRelevantBits different parameters hav different number of relevant bits to be set
     */
    private void cmdSetParam(int p_iParam, int p_iValue, int p_iRelevantBits)
    {
        byte[] commandByte = {(byte)(L6470_CMD_SET_PARAM | (byte) p_iParam)};

        int iNrOfValueBytes = (p_iRelevantBits / 8) + 1;
        byte[] valueBytes  = new byte[iNrOfValueBytes];

        switch (p_iRelevantBits)
        {
        case 8:
        {
            valueBytes[0] = (byte)((p_iValue & 0b0000000011111111) >>  0);
            break;
        }
        case 10:
        {
            valueBytes[0] = (byte)((p_iValue & 0b0000001100000000) >>  8);
            valueBytes[1] = (byte)((p_iValue & 0b0000000011111111) >>  0);
            break;
        }
        case 13:
        {
            valueBytes[0] = (byte)((p_iValue & 0b0001111100000000) >>  8);
            valueBytes[1] = (byte)((p_iValue & 0b0000000011111111) >>  0);
            break;
        }

        default:
        {
            LOGGER.error("cmdSetParam: not handled number of bits ({}) for param {}", p_iRelevantBits, String.format("0x%02X", p_iParam));
        }
        }

        // send all bytes one after the other
        String sValues = "";
        m_SpiChannel.write(commandByte);
        for (int iByteNr = 0; iByteNr < iNrOfValueBytes; iByteNr++)
        {
            m_SpiChannel.write(new byte[]{valueBytes[iByteNr]});
            sValues = sValues + String.format(" 0x%02X", valueBytes[iByteNr]);
        }
        LOGGER.info("setParam to register {} with {} bytes:{}",String.format("0x%02X", p_iParam) ,iNrOfValueBytes, sValues);
    }


    private void cmdHardHiZ()
    {
        // send a reset command byte to the only SPI slave
        byte[] commandByte  = {L6470_Commands.L6470_CMD_HARD_HIZ};
        m_SpiChannel.write(commandByte);
    }


    private int cmdGetStatus()
    {
        // GetStatus
        // Command Structure:
        // from Host:   1 byte  0xD0
        // to host:     2 bytes MSB:LSB
        // The GetStatus command returns the Status register value.
        // GetStatus command resets the STATUS register warning flags. The command forces the
        // system to exit from any error state. The GetStatus command DO NOT reset HiZ flag.

        // Returns the current contents of the STATUS register on the L6470 chip.
        // This is a good communications sanity check because on boot, the value will be 0x2E88.
        byte[] commandByte    = {L6470_Commands.L6470_CMD_GETSTATUS};
        byte[] oneByteRead    = {L6470_Commands.L6470_CMD_NOP};
        byte[] statusRegister = new byte[2];

        m_SpiChannel.write(commandByte);
        statusRegister[0] = m_SpiChannel.writeAndRead(oneByteRead)[0];
        statusRegister[1] = m_SpiChannel.writeAndRead(oneByteRead)[0];

        //---------------
        String sCommand = String.format("0x%02X",commandByte[0]);
        int iBytesReceived = statusRegister.length;
        String sResult = "";
        for (int iByte = 0; iByte < iBytesReceived; iByte++)
        {
            sResult = sResult + String.format(" 0x%02X",statusRegister[iByte]);
        }
        LOGGER.info("SpiDevice getStatus ({}) returned Status ({} bytes):{}",sCommand ,iBytesReceived, sResult);
        //---------------

        int iStatusRegister = (statusRegister[0] << 8) | statusRegister[1];

        return iStatusRegister;
    }


    private void cmdRun(int p_iSpeedStepPerSec, int p_iDirection)
    {
        int iSpeedStepsPerTick = calcRunSpeed(p_iSpeedStepPerSec);

        byte[] commandBytes  = new byte[4];
        commandBytes[0]  = (byte) (L6470_Commands.L6470_CMD_RUN + p_iDirection);
        commandBytes[1]  = (byte) ((iSpeedStepsPerTick & 0b000011110000000000000000) >> 16);
        commandBytes[2]  = (byte) ((iSpeedStepsPerTick & 0b000000001111111100000000) >> 8);
        commandBytes[3]  = (byte)  (iSpeedStepsPerTick & 0b000000000000000011111111);

        String sCommand = String.format("0x%02X 0x%02X 0x%02X 0x%02X",commandBytes[0], commandBytes[1], commandBytes[2], commandBytes[3]);
        LOGGER.info("--> SpiDevice cmdRun({} {}) issued as {} {}", p_iDirection, iSpeedStepsPerTick, String.format("0x%08X", iSpeedStepsPerTick), sCommand);

        m_SpiChannel.write(new byte[]{commandBytes[0]});
        m_SpiChannel.write(new byte[]{commandBytes[1]});
        m_SpiChannel.write(new byte[]{commandBytes[2]});
        m_SpiChannel.write(new byte[]{commandBytes[3]});
    }



    private void cmdGoUntil(int p_iSpeedStepPerSec, int p_iDirection)
    {
//        int lSpeedStepsPerTick = (int) calcMaxSpeed(p_iSpeedStepPerSec);
        int lSpeedStepsPerTick = calcRunSpeed(p_iSpeedStepPerSec);
        cmdGetStatus();
        byte[] commandBytes  = new byte[4];
        commandBytes[0]  = (byte) (L6470_Commands.L6470_CMD_GO_UNTIL + p_iDirection);
        commandBytes[1]  = (byte) ((lSpeedStepsPerTick & 0b000011110000000000000000) >> 16);
        commandBytes[2]  = (byte) ((lSpeedStepsPerTick & 0b000000001111111100000000) >> 8);
        commandBytes[3]  = (byte)  (lSpeedStepsPerTick & 0b000000000000000011111111);

        m_SpiChannel.write(new byte[]{commandBytes[0]});
        m_SpiChannel.write(new byte[]{commandBytes[1]});
        m_SpiChannel.write(new byte[]{commandBytes[2]});
        m_SpiChannel.write(new byte[]{commandBytes[3]});

        getStatusRegister();

    }


    private void cmdReleaseSW(int p_iDirection)
    {
        // the ACT bit is kept on 0 -> set pos reg to zero at sensor edge
        byte[] commandBytes  = new byte[1];
        commandBytes[0]  = (byte) (L6470_Commands.L6470_CMD_RELEASE_SW + p_iDirection);

        m_SpiChannel.write(new byte[]{commandBytes[0]});
    }


    private void cmdSoftStop()
    {
        byte[] commandBytes  = new byte[1];

        commandBytes[0]  = L6470_Commands.L6470_CMD_SOFT_STOP;
        m_SpiChannel.write(new byte[]{commandBytes[0]});

    }







    private int calcRunSpeed(int p_iSpeedStepPerSec)
    {
        // When issuing RUN command, the 20-bit speed is [(steps/s)*(tick)]/(2^-28)
        // where tick is 250ns (datasheet value).
        // Multiply desired steps/s by 67.106 to get an appropriate value for this register
        // This is a 20-bit value, so we need to make sure the value is at or below 0xFFFFF.
        int iStepsPerSecond = p_iSpeedStepPerSec;
        int iStepsPerTick   = (int)((double)iStepsPerSecond * m_dbTick / Math.pow(2, -28));
        // can be reduced to (double)lStepsPerSecond * 67.106

        // parameter correction:
        // cmdRun speed is positive and not larger than 0x000FFFFF
        if (iStepsPerTick > 0x000FFFFF)
        {
            iStepsPerTick = 0x000FFFFF;
        }
        else if (iStepsPerTick < 0)
        {
            iStepsPerTick = 0;
        }

        return iStepsPerTick;
    }



    public int getStatusRegister()
    {
        byte[] commandByte    = {L6470_Commands.L6470_CMD_GET_PARAM + L6470_Params.L6470_PARAM_STATUS};
        byte[] oneByteRead    = {L6470_Commands.L6470_CMD_NOP};
        byte[] statusRegister = new byte[2];

        m_SpiChannel.write(commandByte);
        statusRegister[0] = m_SpiChannel.writeAndRead(oneByteRead)[0];
        statusRegister[1] = m_SpiChannel.writeAndRead(oneByteRead)[0];

        int iStatusRegister = (statusRegister[0] << 8) | statusRegister[1];

        return iStatusRegister;
    }


    // ==========================================================================================
    //  Utility section
    // ==========================================================================================
    private void threadSleepMillis(int p_iMilliSeconds)
    {
        try
        {
            Thread.sleep(p_iMilliSeconds);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }


    public void switchMotorPowerOn()
    {
        m_pinMotorPowerRelay.on();
    }
    public void switchMotorPowerOff()
    {
        m_pinMotorPowerRelay.off();
    }

    public void switchCoolingFanOn()
    {
        m_pinCoolingFan.on();
    }
    public void switchCoolingFanOff()
    {
        m_pinCoolingFan.off();
    }

    public void activateBoardReset()
    {
        m_pinBoardReset.on();
    }
    public void releaseBoardReset()
    {
        m_pinBoardReset.off();
    }

    private boolean isBusySynchPinActive()
    {
        boolean bBusySynchPinActive = m_pinBusySynch.isActive();
        if(bBusySynchPinActive)
        {
            LOGGER.debug("BusySynchPin is active");
        }
        else
        {
            LOGGER.debug("BusySynchPin is inactive");
        }
        return bBusySynchPinActive;
    }

    private boolean isReferenceSensorActive()
    {
        long lCurrentStatus = getStatusRegister();

        boolean bReferenceSensorActive = (lCurrentStatus & 0b0000000000000100) > 0;
        LOGGER.debug("Reference Sensor Active: \t{}", bReferenceSensorActive);

        return bReferenceSensorActive;
    }

    public boolean isAxisBelowRefSensor()
    {
        return isReferenceSensorActive();
    }


}
