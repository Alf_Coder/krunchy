/*
 * Copyright (c) 2018 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */
package ffhs.krunchy;


import ffhs.krunchy.control.KrunchyContext;
import ffhs.krunchy.control.statemachine.KrunchyStateStarting;

import java.io.*;
import java.util.Enumeration;
import java.util.Properties;

public class KrunchyLauncher
{
    private static final String DEFAULT_PROPERTIES_FILE = "default.properties";  // in class path: .../src/main/resources/
    private static final String SPECIFIC_PROPERTIES_FILE = "krunchy.properties"; // incl. path => .../resources/... path somewhere? ???

    public static void main(String[] args)
    {
        // read the specific Krunchy properties or create them with default values
        if (!_prepareProperties())
        {
            // no properties makes no sense -> early exit
            System.exit(1);
        }

        KrunchyContext context = new KrunchyContext(SPECIFIC_PROPERTIES_FILE);

        context.changeState(KrunchyStateStarting.getInstance());
        context.runStateMachine();

        // prepare the system for shut down i.e. power off etc
        context.exitInstance();
    }


    /**
     * Check and create system properties
     */
    private static boolean _prepareProperties()
    {
        boolean bPropertiesOK = false;

        if (!_isSpecificPropertyFileExist())
        {
            _createSpecificProperties();
        }

        // now the specific properties should exist, check consistency
        bPropertiesOK = _synchronizeProperties();

        return bPropertiesOK;
    }


    /**
     * create a new (empty) specific properties file in the filesystem
     */
    private static void _createSpecificProperties()
    {
        Properties krunchyProperties = new Properties();
        OutputStream propertiesWriter = null;

        try
        {
            propertiesWriter = new FileOutputStream(SPECIFIC_PROPERTIES_FILE);
            krunchyProperties.store(propertiesWriter, null);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            if (propertiesWriter != null)
            {
                try
                {
                    propertiesWriter.close();
                }
                catch (IOException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
    }


    /**
     * add new properties to the specific properties and remove the deleted ones
     */
    private static boolean _synchronizeProperties()
    {
        boolean bPropertiesOK = false;

        if (!_isDefaultPropertyFileExist())
        {
            // something terrible happened in the deployment, early exit
            return bPropertiesOK;
        }

        Properties krunchyProperties = new Properties();
        Properties defaultProperties = new Properties();
        InputStream  defaultPropertiesReader = null;
        InputStream  krunchyPropertiesReader = null;
        OutputStream krunchyPropertiesWriter = null;

        try
        {
            krunchyPropertiesReader = new FileInputStream(SPECIFIC_PROPERTIES_FILE);
            krunchyProperties.load(krunchyPropertiesReader);

            krunchyPropertiesWriter = new FileOutputStream(SPECIFIC_PROPERTIES_FILE);
            krunchyPropertiesReader = new FileInputStream(SPECIFIC_PROPERTIES_FILE);

            // get all property names from the default properties file
            defaultPropertiesReader = KrunchyLauncher.class.getClassLoader().getResourceAsStream(DEFAULT_PROPERTIES_FILE);
            defaultProperties.load(defaultPropertiesReader);

            // check for new property keys in the default file
            Enumeration<?> eDefaultPropertyNames = defaultProperties.propertyNames();
            while (eDefaultPropertyNames.hasMoreElements())
            {
                String sPropertyKey = (String) eDefaultPropertyNames.nextElement();
                if (krunchyProperties.containsKey(sPropertyKey))
                {
                    // nothing to do if it is already there
                    String sPropertyValue = krunchyProperties.getProperty(sPropertyKey);
                    //System.out.println("Key: " + sPropertyKey + " already exists with Value: " + sPropertyValue);
                }
                else
                {
                    // need to add the new property with its default value
                    String sNewValue = defaultProperties.getProperty(sPropertyKey);
                    krunchyProperties.setProperty(sPropertyKey, sNewValue);
                    //System.out.println("Key: " + sPropertyKey + " added with Value: " + sNewValue);
                }
            }

            // check for old property keys in the specific file
            Enumeration<?> eSpecificPropertyNames = krunchyProperties.propertyNames();
            while (eSpecificPropertyNames.hasMoreElements())
            {
                String sPropertyKey = (String) eSpecificPropertyNames.nextElement();
                if (defaultProperties.containsKey(sPropertyKey))
                {
                    // nothing to do if it is still there
                    //System.out.println("Key: " + sPropertyKey + " still exists in default");
                }
                else
                {
                    // need to delete the obsolete property
                    String sOldValue = krunchyProperties.getProperty(sPropertyKey);
                    krunchyProperties.remove(sPropertyKey);
                    //System.out.println("Key: " + sPropertyKey + " removed with Value: " + sOldValue);
                }
            }

            // save updated specific properties file
            krunchyProperties.store(krunchyPropertiesWriter, null);
            bPropertiesOK = true; // reached here means all OK
        }
        catch (IOException io)
        {
            io.printStackTrace();
        }
        finally
        {
            if (defaultPropertiesReader != null)
            {
                try
                {
                    defaultPropertiesReader.close();
                }
                catch (IOException ex)
                {
                    ex.printStackTrace();
                }
            }

            if (krunchyPropertiesReader != null)
            {
                try
                {
                    krunchyPropertiesReader.close();
                }
                catch (IOException ex)
                {
                    ex.printStackTrace();
                }
            }

            if (krunchyPropertiesWriter != null)
            {
                try
                {
                    krunchyPropertiesWriter.close();
                }
                catch (IOException ex)
                {
                    ex.printStackTrace();
                }
            }
        }

        return bPropertiesOK;
    }


    /**
     * check if the default property file exist in the class path
     * it should be placed there by the deployment process
     *
     * @return true if it exists
     */
    private static boolean _isDefaultPropertyFileExist()
    {
        InputStream defaultPropertiesReader = KrunchyLauncher.class.getClassLoader().getResourceAsStream(DEFAULT_PROPERTIES_FILE);

        boolean bPropertiesFileExist = defaultPropertiesReader != null;
        if (!bPropertiesFileExist)
        {
            // should not happen since deployed in the class tree
            System.out.println("Properties File does not exist: " + DEFAULT_PROPERTIES_FILE);
        }
        else
        {
            try
            {
                defaultPropertiesReader.close();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }

        return bPropertiesFileExist;
    }


    /**
     * check if the specific property file for this Krunchy exist in the filesystem
     *
     * @return true if it exists
     */
    private static boolean _isSpecificPropertyFileExist()
    {
        boolean bPropertiesFileExist = false;
        InputStream specificPropertiesReader = null;
        try
        {
            specificPropertiesReader = new FileInputStream(SPECIFIC_PROPERTIES_FILE);
            bPropertiesFileExist = (specificPropertiesReader != null);
        }
        catch (FileNotFoundException ex)
        {
            // can happen on a very first run of the Krunchy software
            System.out.println("Properties File does not exist: " + SPECIFIC_PROPERTIES_FILE);
        }
        finally
        {
            if (specificPropertiesReader != null)
            {
                try
                {
                    specificPropertiesReader.close();
                }
                catch (IOException ex)
                {
                    ex.printStackTrace();
                }
            }
        }

        return bPropertiesFileExist;
    }

}







