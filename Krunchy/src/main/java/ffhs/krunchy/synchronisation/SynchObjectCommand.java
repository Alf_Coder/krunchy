/*
 * Copyright (c) 2019 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.synchronisation;

public class SynchObjectCommand extends SynchObject
{
    protected String m_sCommand; // TODO: make it an enum

    public SynchObjectCommand(String p_sName)
    {
        super(p_sName);
    }

    public void setCommand(String p_sCommand)
    {
        m_sCommand = p_sCommand;
    }

    public String getCommand()
    {
        return m_sCommand;
    }

}
