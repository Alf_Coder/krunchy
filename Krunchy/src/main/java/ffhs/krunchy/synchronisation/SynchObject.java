/*
 * Copyright (c) 2019 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.synchronisation;

public class SynchObject
{
    protected String  m_sName      = "noname";
    protected boolean m_bPending   = false;
    protected boolean m_bSignalled = false;

    public SynchObject(String p_sName)
    {
        m_sName = p_sName;
    }

    public String getName()
    {
        return m_sName;
    }

    public void setPending()
    {
        m_bPending = true;
    }
    public void resetPending()
    {
        m_bPending = false;
    }
    public boolean isPending()
    {
        return m_bPending;
    }

    public void setSignalled()
    {
        m_bSignalled = true;
    }
    public void resetSignalled()
    {
        m_bSignalled = false;
    }
    public boolean IsSignalled()
    {
        return m_bSignalled;
    }

}
