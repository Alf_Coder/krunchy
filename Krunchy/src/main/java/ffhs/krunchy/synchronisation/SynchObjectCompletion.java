/*
 * Copyright (c) 2019 andreas.frommel / mirko.helbling @ffhs.ch
 * Licensed under the Educational Community License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at  http://opensource.org/licenses/ecl2
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ffhs.krunchy.synchronisation;

public class SynchObjectCompletion extends SynchObject
{
    protected boolean m_bError = false;
    protected String  m_sErrorType; // TODO: make it an enum

    public SynchObjectCompletion(String p_sName)
    {
        super(p_sName);
    }

    public void setError(String p_sError)
    {
        m_sErrorType = p_sError;
        m_bError = true;
    }

    public boolean isError()
    {
        return m_bError;
    }

    public String getErrorType()
    {
        return m_sErrorType;
    }

}
